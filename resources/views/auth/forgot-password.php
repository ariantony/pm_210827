<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="PT. Klola Indonesia">
	<meta name="keywords" content="HR, Payroll, Human Resources, People Management, Human Capital, Talent, Talent Management, SDM">
	<meta name="description" content="Software HR & Payroll terbaik bebasis cloud">
	<link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.ico">
	<title>Klola Indonesia - HR & Payroll System</title>
	<!-- CSS -->
	<link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="../node_modules/toastr/build/toastr.min.css" rel="stylesheet">
	<link href="../node_modules/select2/dist/css/select2.min.css" rel="stylesheet">
	<link href="../node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
	<link href="../node_modules/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
	<link href="../node_modules/smartwizard/dist/css/smart_wizard_theme_arrows.min.css" rel="stylesheet">
	<link href="../node_modules/morris.js/morris.css" rel="stylesheet">
	<link href="../node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="../assets/css/animate.css" rel="stylesheet">
	<link href="../assets/css/styles.min.css" rel="stylesheet">


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="pace-done ">

<div class="auth-box" >

    <div class="form-box m-b-25 m-l-10 m-r-10">
      <div class="main-logo mb-5"></div>
      <form class="bg-white form-padding form-horizontal floating-labels" id="loginform" action="dashboard/sdm.html">
        <div class="form-group m-b-30 text-center">
          <div class="col-12">
            <h4 class="font-normal">Lupa Password</h4>
            <p class="text-muted">Masukkan Email anda, intruksi reset password akan dikirim melalu email. </p>
          </div>
        </div>
        <div class="form-group m-b-20">
          <div class="col-12">
            <input class="form-control" name="email" type="text" required="" placeholder="Email Address">
          </div>
        </div>
        <div class="form-group text-center">
          <div class="col-12">
            <button class="btn bg-gradient-success btn-md p-t-10 p-b-10 btn-block font-bold text-uppercase" type="submit">Kirim</button>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-12 text-center">
          <p><a href="index.php" class="m-l-5 font-12 text-muted"><i class="ti-arrow-left"></i> Kembali ke Halaman Login </a></p>
          </div>
        </div>
      </form>
      <div class="text-muted text-center mt-3">
				&copy; 2019 Klola Indonesia. Made with <i class="fa fa-heart text-danger"></i> in Indonesia.
      </div>
    </div>
</div>

    <!-- jQuery -->
    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="../node_modules/smartwizard/dist/js/jquery.smartWizard.min.js"></script>
    <script src="../node_modules/chart.js/dist/Chart.min.js"></script>
    <script src="../node_modules/datatables.net/js/jquery.dataTables.js"></script>
    <script src="../node_modules/raphael/raphael.min.js"></script>
    <script src="../node_modules/morris.js/morris.min.js"></script>
    <script src="../node_modules/select2/dist/js/select2.min.js"></script>
    <script src="../node_modules/icheck/icheck.min.js"></script>
    <script src="../node_modules/screenfull/dist/screenfull.js"></script>
    <script src="../node_modules/toastr/toastr.js"></script>
    <script src="../node_modules/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../node_modules/breakpoints-js/dist/breakpoints.min.js"></script>
    <script src="../node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="../node_modules/flot-charts/jquery.flot.js"></script>
    <script src="../node_modules/flot-charts/jquery.flot.resize.js"></script>
    <script src="../assets/js/main.js"></script>
</body>

</html>


