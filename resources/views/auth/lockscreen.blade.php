@extends('layouts.auth')

@section('content')
	<div class="auth-box" >
		<div class="form-box m-b-25 m-l-10 m-r-10">
			<div class="kl-login-avatar text-center"><img src="/images/user2.png" alt="user-img" width="90" class="rounded-circle"></div>
			<form class="bg-white form-padding form-horizontal " id="loginform" action="{{ route('login') }}">
				<div class="form-group mt-2">
					<div class="col-12 text-center">
						<h4 class="font-normal">Aris Sudaryanto</h4>
					</div>
				</div>
				<div class="form-group m-b-20">
					<div class="col-12">
						<label for="input1"> </label>
						<input class="form-control" type="password" id="password" required="" placeholder="Masukan password">
					</div>
				</div>
				<div class="form-group text-center m-t-20">
					<div class="col-12">
						<button class="btn bg-gradient-success btn-md p-t-10 p-b-10 btn-block text-uppercase font-bold " type="submit">Unlock</button>
					</div>
				</div>
				<div class="form-group m-b-0">
					<div class="col-sm-12 text-center">
						<p><a href="index.php" class="text-muted m-l-5">Keluar Aplikasi? </a></p>
					</div>
				</div>
			</form>
			<div class="text-muted text-center mt-3">
				&copy; 2019 Klola Indonesia. Made with <i class="fa fa-heart text-danger"></i> in Indonesia.
			</div>
		</div>
	</div>
@endsection
