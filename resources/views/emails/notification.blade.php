
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
   </head>
   <body style="background-color:#f9f9f9">
      <table style="margin: 30px auto 0;" cellpadding="0" cellspacing="0" width="550">
         <tr>
            <td valign="top">
              <h2 style="margin-bottom:0px">PMS - HARVEST GROUP</h2>
            </td>
         </tr>
      </table>
      <table style="margin: 20px auto;padding: 20px 30px" cellpadding="0" cellspacing="0" width="550" bgcolor="#ffffff">
           <tr>
              <td align="center" valign="top">
                  <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                       <td>
                          <center>
                          <br>
                          <h3>{{ $title }}</h3>
                          </center>
                       </td>
                    </tr>
                  </table>
                  <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                       <td style="margin: 20px; text-align: left;" align="left">
                          <br>
                          <div style="font-size:12px;color: #333333">
                            {{ $name }},
                            {{ $content }}
                            <br>
                          </div>
                          <br>
                       </td>
                    </tr>
                  </table>


                 <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                       <td>
                          <span style="font-size:12px;color: #333333">
                           Terima-Kasih. <br>
                          </span>
                          <br>
                         <br>
                       </td>
                    </tr>
                 </table>
              </td>
           </tr>
      </table>

       <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="550" >
           <tr>
              <td valign="top" >
                <center>
                     <br>
                     <span style="font-size:12px;color: #757575">
                        Powered By <a href="http://klola.id"  style="font-size:12px;color: #757575">Klola Indonesia</a>. All rights reserved.
                       <br>
                       <br>
                        Pondok Indah Office Tower 2, 17th Fl. <br>
                        Jl. Sultan Iskandar Muda Kav.V-TA Pondok Indah, RT.4/RW.3, Pd. Pinang, Kec. Kby. Lama, , Kota Jakarta Selatan - 12310 <br>
                        Telp.: +62 21 75922910 | Email: info@klola.id
                       <br>
                       <br>
                       <br>
                     </span>
                </center>
              </td>
          </tr>
       </table>
   </body>
</html>