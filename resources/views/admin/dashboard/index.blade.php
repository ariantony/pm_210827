@extends('layouts.app')

@section('content')

<div class="row gap-20 justify-content-end">
    <div class="col-sm-3">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text text-uppercase font-weight-bold" style="padding:0.3rem 1rem !important;">@lang('pm.periode')</span>
            </div>
            <input type="text" name="periode" value="{{ $year }}" class="form-control"  id="yearpicker" required>
        </div>
    </div>
</div>


<div class="row gap-20">

    <div class="col-sm-4">
        <div class="card p-20">
            <div class="layer w-100 mB-10">
                <div class="float-right">
                    <a class="text-semibold" href="{{ route('performance.monitoring',$year) }}">@lang('global.app_detail')</a>
                </div>
                <h6 class="lh-1">Monitoring Assessment</h6>
            </div>
            <div class="layer w-100">
                <div class="peers ai-sb fxw-nw">
                    <div class="peer peer-greed">
                         <i class="ti-user fa-2x"></i><span class="fa-2x"> {{ count($assessment) }}</span> 
                    </div>
                    <div class="peer">
                        <span class="font-11">Pegawai Belum melakukan Penilaian KPI/Kompetensi</span>
                    </div>
                </div>
            </div>
        </div>


        <div class="card p-20">
           
            <div class="layer w-100 mB-10">
                <div class="float-right">
                    <a class="text-semibold" href="{{ route('performance.monitoring.kpi',$year) }}">@lang('global.app_detail')</a>
                </div>
                <h6 class="lh-1">Monitoring Input KPI</h6>
            </div>
            <div class="layer w-100">
                <div class="peers ai-sb fxw-nw">
                    <div class="peer peer-greed">
                        <i class="ti-user fa-2x"></i><span class="fa-2x"> {{ count($performance) }}</span> 
                    </div>
                
                    <div class="peer">
                        <span class="font-11">Pegawai belum melakukan Input KPI</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body p-20">
                <h5 class="header-title"><i class="ti-throphy font-bold mr-2 icon-lg"></i> Status Input Performance</h5>
                <hr>
                <div style="min-height: 250px;">
                    <canvas id="chart2" height="240"></canvas>
                </div>
                <div id="chart2-legend" class="chart-legend mt-2" style="padding:0 !important"></div>
            </div>
        </div>
    </div>

    <div class="col-sm-8">

        <div class="row gap-20">
            <div class='col-md-12 mt-1'>
                <div class="card p-20">
                    <div class="layer mt-2 w-100">
                        <div style="min-height: 250px;">
                            <canvas id="chart1" height="240"></canvas>
                        </div>
                        <div id="chart1-legend" class="chart-legend"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


@endsection

<?php 
    $chart1_value_msi= [];
    $chart1_label_msi= [];
    foreach ($stats as $val){
        $chart1_label_msi[] = $val['name'];
        $chart1_value_msi[] = round($val['score'],2);
    }
    $chart1Label = '"'.implode('","', $chart1_label_msi).'"';
    $chart1Value = implode(',', $chart1_value_msi);


?>

@section('js')
    <script>
        $(document).ready(function() {

            $('#yearpicker').datepicker({
                container:'#page-content',
                autoclose: true,
                format: " yyyy", // Notice the Extra space at the beginning
                viewMode: "years",
                minViewMode: "years",
                orientation: 'bottom'
            }).on('changeDate', function(e) {
                var date = encodeURIComponent(this.value.trim());
                var url = "/admin/dashboard/"+date;
                window.open(url, '_self');
            });

            // Chart 1
            var Chart1 = new Chart(document.getElementById("chart1").getContext("2d"), {
                type: 'horizontalBar',
                data: {
                    labels: [
                        {!! $chart1Label !!},
                    ],
                    datasets: [
                        {
                            backgroundColor: "rgba(210, 77, 87,1)",
                            hoverBackgroundColor: "rgba(44, 130, 190,0.8)",
                            data: [{{ $chart1Value }}]
                        }
                    ]

                },
                options: {
                    responsive: true,
                    tooltips: {
                        mode: "index",
                        intersect: true
                    },
                    legend: {
                        display: false
                    },
                    scales: {
                        xAxes: [
                            {
                                display: true,
                                scaleLabel: {
                                    display: true
                                },
                                gridLines: {
                                    display: false
                                },
                                ticks: {
                                    fontSize: 9
                                },
                                barThickness: 10
                            }
                        ],
                        yAxes: [
                            {
                                display: true,
                                ticks: {
                                    beginAtZero: true,
                                    stepSize: 20,
                                    stepValue: 20,
                                    fontSize: 9
                                },
                                gridLines: {
                                    display: false
                                }
                            }
                        ]
                    },
                    title: {
                        display: true,
                        text: '{{ __("pm.chart1_title") }}'
                    }
                }
            });
            document.getElementById("chart1-legend").innerHTML = Chart1.generateLegend();

            // Chart 2
            var waiting = "{{ $performanceStatus[0]->waiting}}";
            var done    = "{{ $performanceStatus[0]->done}}";
            var process = "{{ $performanceStatus[0]->process}}";
            var draft   = "{{ $performanceStatus[0]->draft}}";

            var Chart2  = new Chart(document.getElementById("chart2"), {
                type: 'doughnut',
                data: {
                labels: ["Waiting to Approve", "On Process", "Done", "Draft"],
                datasets: [
                    {
                    backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#f45235"],
                    data: [waiting, process, done, draft]
                    }
                ]
                },
                options: {
                    legend: {
                        display: false,
                    },
                    title: {
                        display: true,
                        text: ''
                    }
                }
            });
            document.getElementById("chart2-legend").innerHTML = Chart2.generateLegend();


        
        });
    </script>
@endsection