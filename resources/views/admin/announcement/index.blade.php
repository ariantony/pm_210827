@extends('layouts.app')

@section('page-header')
    @lang('app.announcement')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('app.announcement')</li>
    </ol>
@endsection

@section('content')

<div class="row mB-40">
    <div class="col-sm-12">
        <div class="card p-20 bd">
            <div class="m-b-20">
                <a href="{{ route('admin.announcement.create') }}" class="btn btn-info btn-sm">
                    {{ trans('global.app_create') }}
                </a>
            </div>
            <div class="table-responsive">
                <table id="dataTables" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{ __('app.title') }}</th>
                            <th>{{ __('app.status') }}</th>
                            <th>{{ __('app.updated') }}</th>
                            <th>{{ __('app.action') }}</th>
                        </tr>
                    </thead>
                
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin.announcement.datatables') }}',
            columns: [
                {data: 'title', name: 'title'},
                {data: 'status', name: 'status',  orderable: false, searchable: false},
                {data: 'updated_at', name: 'updated_at', searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@stop