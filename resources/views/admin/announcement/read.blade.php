@extends('layouts.app')

@section('page-header')
    Announcement 
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Announcement</li>
    </ol>
@endsection

@section('content')

<div class="row mB-40">
    <div class="col-sm-12">
        <div class="p-20 ">
            
            <div class="row justify-content-center">
				<div  class="col-9">
                    <form action="{{ route('announcement.search') }}" method="post">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="text" placeholder="Cari Pengumuman Berdasarkan Judul..." name="q" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-info" type="submit">Search</button>
                            </span>
                        </div>
                    </form>
				</div>
			</div>
		</div>
	
			<div class="blog row">
                @foreach ($announcement as $item)
                    <div class="blog-item col-sm-4">
                        <div class="card p-20">
                            <div class="blog-content">
                                <div class="blog-title media-block">
                                    <div class="media-body">
                                        <a href="#" class="btn-link">
                                            <h6>{{ $item->title }}</h6>
                                        </a>
                                    </div>
                                </div>
                                <div class="blog-body">
                                    <?php echo substr($item->content,0, 160) ?>
                                </div>
                            </div>
                            <div class="blog-footer">
                                <div class="media-left">
                                    <span class="text-muted">
                                    {{ \Carbon\Carbon::parse($item->created_at)->diffForHumans() }}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{ $announcement->links() }}
			</div>

    </div>
</div>
@endsection

@section('js')
<script>
	$(document).ready(function() {
		$('.blog').masonry({
			// options...
			itemSelector: '.blog-item',
			columnWidth: 200
		});
	})
</script>
@stop