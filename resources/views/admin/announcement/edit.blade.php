@extends('layouts.app')

@section('page-header')
    @lang('app.announcement')
@stop

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.announcement.index') }}">@lang('app.announcement')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('global.app_edit')</li>
    </ol>
@endsection


@section('content')
<div class="row mB-40">

	<div class="col-sm-12">
        <div class="card p-30 bd">
            <h6><a class="float-left" href="{{ route('admin.announcement.index') }}"><i class="ti-arrow-left mR-10"></i></a></h6>
            <hr class="mB-30">
            
            {!! Form::model($announcement, [
                    'route' => ['admin.announcement.update', $announcement->id],
					'method' => 'put', 
					'files' => true
				])
			!!}
                
                <div class="form-group row mt-3">
                    <label class="col-sm-2 col-form-label text-right">@lang('app.title') <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('title'))
                            <p class="help-block">
                                {{ $errors->first('title') }}
                            </p>
                        @endif
                    </div>
                </div>  

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label text-right">@lang('app.content')</label>
                    <div class="col-sm-6">
                        {!! Form::textarea('content', old('content'), ['class' => 'form-control description', 'rows' => 4, 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('content'))
                            <p class="help-block">
                                {{ $errors->first('content') }}
                            </p>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label text-right">@lang('app.expired')</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ti-calendar"></i></span>
                            </div>
                            {!! Form::text('expired', date('d/m/Y',strtotime( $announcement->expired))  , ['class' => 'form-control  datepicker', 'placeholder' => '', 'required' => '']) !!}
                        </div>
                        <p class="help-block"></p>
                        @if($errors->has('expired'))
                            <p class="help-block">
                                {{ $errors->first('expired') }}
                            </p>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label text-right">@lang('app.status') <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="checkbox" name="status" class="switch switch-info" id="status" value="{{ $announcement->status or '0'}}">
                        <p class="help-block"></p>
                        @if($errors->has('status'))
                            <p class="help-block">
                                {{ $errors->first('status') }}
                            </p>
                        @endif
                    </div>
                </div>
                
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label text-right"></label>
                    <div class="col-sm-8">
                        <a href="{{ route('admin.announcement.index') }}" class="btn btn-light">{{ trans('global.app_cancel') }}</a>
                        {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
                    </div>
                </div>
				
			{!! Form::close() !!}
		</div>  
	</div>
</div>
	
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.11/tinymce.min.js"></script>
    <script>
        $(document).ready(function() {
            if($('#status').val()=='1'){
                $('#status').attr('checked','checked').iCheck('update');
            }
            $('#status').on('ifChecked', function(){
                $("#status" ).attr('value', '1');
            });
            $('#status').on('ifUnchecked', function(){
                $("#status" ).attr('value', '0');
            });
            tinymce.init({
                selector:'textarea.description',
                width: 800,
                height: 300
            });
        });
    </script>
@stop