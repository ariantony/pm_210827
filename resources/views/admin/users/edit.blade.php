@extends('layouts.app')

@section('page-header')
	{{ __('global.users.title') }}
@stop

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">{{ __('global.user-management.title') }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ __('global.app_edit') }}</li>
    </ol>
@endsection

@section('content')
	{!! Form::model($user, [
			'action' => ['Admin\UsersController@update', $user->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

    <div class="card p-30 bd">

		@include('admin.users.form')

	</div>

	<div class="mt-4">
		<a href="{{ route('admin.users.index') }}" class="btn btn-light">{{ trans('global.app_cancel') }}</a>
		{!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
	</div>

	{!! Form::close() !!}
	
@stop
