@extends('layouts.app')

@section('page-header')
	{{ __('global.users.title') }}
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __('app.home') }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ __('global.user-management.title') }}</li>
    </ol>
@endsection

@section('content')

    <div class="card bd bdrs-3 p-20 mB-20">
        <div class="mb-3">
            <a href="{{ route('admin.users.create') }}" class="btn btn-info">
                {{ trans('global.app_create') }}
            </a>
        </div>
        <table id="dataTables" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>{{ __('app.name') }}</th>
                    <th>{{ __('app.email') }}</th>
                    <th>{{ __('app.updated') }}</th>
                    <th>{{ __('app.action') }}</th>
                </tr>
            </thead>
        </table>
    </div> 

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('#dataTables').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('admin.users.datatables') }}',
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'updated_at', name: 'updated_at', searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                "pageLength": 50
            });
        });
    </script>
@stop