		<div class="form-group row m-t-30">
			{!! Form::label('name', 'Name*', ['class' => 'col-form-label col-sm-3']) !!}
			<div class="col-sm-6">
			{!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
			<p class="help-block"></p>
			@if($errors->has('name'))
				<p class="help-block">
					{{ $errors->first('name') }}
				</p>
			@endif
			</div>
		</div>

		<div class="form-group row">
			{!! Form::label('email', 'Email*', ['class' => 'col-form-label col-sm-3']) !!}
			<div class="col-sm-6">
			{!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
			<p class="help-block"></p>
			@if($errors->has('email'))
				<p class="help-block">
					{{ $errors->first('email') }}
				</p>
			@endif
			</div>
		</div>

		<div class="form-group row">
			{!! Form::label('password', 'Password*', ['class' => 'col-form-label col-sm-3']) !!}
			<div class="col-sm-6">
            <input id="password" type="password" class="form-control" name="password" required>
			<p class="help-block"></p>
			@if($errors->has('password'))
				<p class="help-block">
					{{ $errors->first('password') }}
				</p>
			@endif
			</div>
		</div>

		<div class="form-group row">
			{!! Form::label('roles', 'Roles*', ['class' => 'col-form-label col-sm-3']) !!}
			<div class="col-sm-6">
				{!! Form::select('roles[]', $roles, old('roles'), ['class' => 'form-control select2', 'multiple' => 'multiple', 'required' => '']) !!}
				<p class="help-block"></p>
				@if($errors->has('roles'))
					<p class="help-block">
						{{ $errors->first('roles') }}
					</p>
				@endif
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Photo Profile </label>
			<div class="col-sm-6">
				<input type="file" name="photo" class="form-control">
			</div>
		</div>
		
		@if (old('photo'))
			<div class="form-group row">
				<label class="col-sm-3 col-form-label text-right"> </label>
				<div class="col-sm-2">
					<img src="{{ asset('storage'.$user->photo) }}" class="img-fluid img-thumbnail w-75">
				</div>
			</div>
		@endif

