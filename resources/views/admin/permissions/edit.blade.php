@extends('layouts.app')

@section('page-header')
    @lang('global.permissions.title') <small>{{ trans('app.update_item') }}</small>
@endsection


@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.permissions.index') }}">@lang('global.permissions.title') </a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('global.app_edit') </li>
    </ol>
@endsection

@section('content')
    
    {!! Form::model($permission, ['method' => 'PUT', 'route' => ['admin.permissions.update', $permission->id]]) !!}

        <div class="card bd bdrs-3 p-20 mB-20">

            <div class="form-group row m-t-30">
                {!! Form::label('name', 'Name*', ['class' => 'control-label col-form-label col-sm-2']) !!}
                <div class="col-sm-5">
                    {!! Form::text('name', old('title'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <div class='col-sm-2'> </div>
                <div class="col-sm-5">
                    {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-danger']) !!}
                </div>
            </div>
            
        </div>
    {!! Form::close() !!}
@stop

