@extends('layouts.app')

@section('page-header')
    @lang('app.change_password')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('app.change_password')</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" method="POST" action="{{ route('profile.change_password') }}">
        {{ csrf_field() }}

        <div class="card bd bdrs-3 p-30 mB-20">

            <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }} row m-t-30">
                <label for="new-password" class="col-md-3 col-form-label">@lang('passwords.current_password')</label>

                <div class="col-md-4">
                    <input id="current-password" type="password" class="form-control" name="current-password" required>

                    @if ($errors->has('current-password'))
                        <span class="help-block">
                            {{ $errors->first('current-password') }}
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }} row">
                <label for="new-password" class="col-md-3 col-form-label">@lang('passwords.new_password')</label>

                <div class="col-md-4">
                    <input id="new-password" type="password" class="form-control" name="new-password" required>

                    @if ($errors->has('new-password'))
                        <span class="help-block">
                            {{ $errors->first('new-password') }}
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="new-password-confirm" class="col-md-3 col-form-label">@lang('passwords.confirm_new_password')</label>

                <div class="col-md-4">
                    <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="new-password-confirm" class="col-md-3 col-form-label"></label>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-danger">
                        @lang('global.app_submit')
                    </button>
                </div>
            </div>
        </div>

       
    </form>
@endsection