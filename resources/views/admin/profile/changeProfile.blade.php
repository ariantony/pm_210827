@extends('layouts.app')

@section('page-header')
    @lang('app.change_profile')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">  @lang('app.home')</a></li>
        <li class="breadcrumb-item active" aria-current="page">  @lang('app.change_profile')</li>
    </ol>
@endsection

@section('content')
        {!! Form::model($users, [
                    'route' => ['profile.change_profile'],
					'method' => 'POST', 
					'files' => true
				])
			!!}
        {{ csrf_field() }}

        <div class="card bd bdrs-3 p-20 mB-20">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row m-t-30">
                <label for="email" class="col-md-3 col-form-label">Name</label>

                <div class="col-md-4">
                    <input id="name" type="text" class="form-control" name="name" required value="{{ $users->username }}">

                    @if ($errors->has('name'))
                        <span class="help-block">
                            {{ $errors->first('name') }}
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
                <label for="email" class="col-md-3 col-form-label">Email Address</label>

                <div class="col-md-4">
                    {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}

                    @if ($errors->has('email'))
                        <span class="help-block">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-sm-3 col-form-label text-right">Photo Profile </label>
                <div class="col-sm-4">
                    <input type="file" name="photo" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label text-right"> </label>
                <div class="col-sm-2">
                    @if ($users->photo)
                        <img src="{{ asset('storage'.$users->photo) }}" class="img-fluid img-thumbnail w-75">
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="new-password-confirm" class="col-md-3 col-form-label"></label>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-danger">
                        @lang('global.app_submit')
                    </button>
                </div>
            </div>

        </div>
       
    </form>
@endsection