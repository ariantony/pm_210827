@extends('layouts.app')

@section('page-header')
    @lang('global.roles.title') 
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.roles.index') }}">@lang('global.roles.title') </a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('global.app_create') </li>
    </ol>
@endsection

@section('content')
    {!! Form::open(['method' => 'POST', 'route' => ['admin.roles.store']]) !!}

        <div class="card bd bdrs-3 p-20 mB-20">
            <div class="form-group row">
                {!! Form::label('name', 'Name*', ['class' => 'control-label col-form-label col-sm-2']) !!}
                <div class="col-sm-5">
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('permission', 'Permissions', ['class' => 'control-label col-form-label col-sm-2']) !!}
                <div class="col-sm-5">
                    {!! Form::select('permission[]', $permissions, old('permission'), ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('permission'))
                        <p class="help-block">
                            {{ $errors->first('permission') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-2"></div>
                <div class="col-sm-5">
                    {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
                </div>
            </div>
            
        </div>

    {!! Form::close() !!}
@stop

