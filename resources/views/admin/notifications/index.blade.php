@extends('admin.default')

@section('page-header')
   Notifications <small>{{ trans('app.manage') }}</small>
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route(ADMIN . '.dashboard') }}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Notifications</li>
    </ol>
@endsection


@section('content')
<div class="row mB-40">

    <div class="col-sm-3">
        
    </div>

    <div class="col-sm-9">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            
        </div>
    </div>

 </div>
@endsection