<?php

    $uri = Request::segment(2);
    $com_active = $group_active = $obj_active = $general_active = $measure_active  = '' ;
    if($uri == "competency"){
        $com_active  = 'active';
    }elseif($uri == "group_rater"){
        $group_active  = 'active';
    }elseif($uri == "objective_category"){
        $obj_active  = 'active';
    }elseif($uri == "measure"){
        $measure_active  = 'active';
    }elseif($uri == "general"){
        $general_active  = 'active';
    }else{
        $com_active = $group_active = $obj_active = $general_active = $measure_active  = '';
    }
?>
<div class="col-sm-3">
    <div class="card p-20 bd">
        <h6>@lang('app.setting')</h6>
        <ul class="nav flex-sm-column flex-r mT-40">
            <li><a class="nav-link {{ $general_active }}" href="{{ route('setting.general.index') }}"> @lang('app.general')</a></li>
            <li><a class="nav-link {{ $com_active }}" href="{{ route('setting.competency.index') }}"> @lang('pm.competency')</a></li>
            <li><a class="nav-link {{ $group_active }}" href="{{ route('setting.group_rater.index') }}">@lang('app.group_raters')</a></li>
            <li><a class="nav-link {{ $obj_active }}" href="{{ route('setting.objective_category.index') }}">@lang('pm.objective_category')</a></li>
            <li><a class="nav-link {{ $measure_active }}" href="{{ route('setting.measure.index') }}">@lang('pm.measure')</a></li>
        </ul>
    </div>
</div>