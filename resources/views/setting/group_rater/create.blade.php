@extends('layouts.app')

@section('page-header')
    @lang('app.setting')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="#">@lang('app.setting')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('app.group_raters')</li>
    </ol>
@endsection

@section('content')

	<div class="row mB-40">
   
        @include('setting.menu')

        <div class="col-sm-9">
            <div class="card p-20 bd">
                
                <h6><a class="float-left" href="{{ route('setting.group_rater.index') }}"><i class="ti-arrow-left m-r-10"></i></a>   @lang('app.group_raters')</h6>
                <hr class="mB-30">

                {!! Form::open(['method' => 'POST', 'route' => ['setting.group_rater.store']]) !!}
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label text-right"> @lang('app.name') <span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('name'))
                                <p class="help-block">
                                    {{ $errors->first('name') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label text-right"> @lang('app.weight')  <span class="text-danger">*</span></label>
                        <div class="col-sm-2">
                            <div class="input-group">
                                    {!! Form::number('weight', old('weight'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-percent"></i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block"></p>
                            @if($errors->has('weight'))
                                <p class="help-block">
                                    {{ $errors->first('weight') }}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label text-right"> @lang('pm.total_raters')  <span class="text-danger">*</span></label>
                        <div class="col-sm-2">
                            {!! Form::text('raters', old('raters'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('raters'))
                                <p class="help-block">
                                    {{ $errors->first('raters') }}
                                </p>
                            @endif
                        </div>
                    </div>

                    <hr>
                    <div class="form-group">
                        <div class="ml-auto float-right">
	                        <a href="{{ route('setting.group_rater.index') }}" class="btn btn-light">{{ trans('global.app_cancel') }}</a>
                            <button type="submit" class="btn bg-gradient-success"><i class="ti-save m-r-10"></i> {{ __('app.submit') }}</button>
                        </div>
                    </div>

                {!! Form::close() !!}       
            </div>  
        </div>
    </div>
        
@endsection
