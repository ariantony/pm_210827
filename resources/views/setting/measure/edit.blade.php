@extends('layouts.app')

@section('page-header')
    @lang('app.setting')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="#">@lang('app.setting')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('pm.measure')</li>
    </ol>
@endsection

@section('content')
<div class="row mB-40">

    @include('setting.menu')

	<div class="col-sm-9">
        <div class="card p-30 bd">
            <h6><a class="float-left" href="{{ route('setting.measure.index') }}"><i class="ti-arrow-left m-r-10"></i></a> @lang('pm.measure')</h6>
            <hr class="mB-30">
            
            {!! Form::model($competency, [
                    'route' => ['setting.measure.update', $competency->id],
					'method' => 'put', 
					'files' => true
				])
			!!}
                
                
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label text-right"> @lang('app.name') <span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('title'))
                                <p class="help-block">
                                    {{ $errors->first('title') }}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label text-right"> @lang('app.status')</label>
                        <div class="col-sm-6">
                            <input type="checkbox" name="status" class="switch switch-info" id="status" value="{{ $competency->status or '0'}}">
                            <p class="help-block"></p>
                            @if($errors->has('status'))
                                <p class="help-block">
                                    {{ $errors->first('status') }}
                                </p>
                            @endif
                        </div>
                    </div>
                
                <hr>
                <div class="form-group">
                    <div class="ml-auto float-right">
                        <a href="{{ route('setting.measure.index') }}" class="btn btn-light">{{ trans('global.app_cancel') }}</a>
                        <button type="submit" class="btn bg-gradient-success"><i class="ti-save m-r-10"></i> {{ __('app.submit') }}</button>
                    </div>
                </div>
                    
				
			{!! Form::close() !!}
		</div>  
	</div>
</div>
	
@stop

@section('js')
    <script>
        $(document).ready(function() {
            if($('#status').val()=='1'){
                $('#status').attr('checked','checked').iCheck('update');
            }
            $('#status').on('ifChecked', function(){
                $("#status" ).attr('value', '1');
            });
            $('#status').on('ifUnchecked', function(){
                $("#status" ).attr('value', '0');
            });
        });
    </script>
@stop