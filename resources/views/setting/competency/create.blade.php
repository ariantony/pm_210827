@extends('layouts.app')

@section('page-header')
    @lang('app.setting')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="#">@lang('app.setting')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('app.competency')</li>
    </ol>
@endsection

@section('content')

	<div class="row mB-40">
   
        @include('setting.menu')

        <div class="col-sm-9">
            <div class="card p-20 bd">
                
                <h6><a class="float-left" href="{{ route('setting.competency.index') }}"><i class="ti-arrow-left m-r-10"></i></a>  @lang('pm.competency')</h6>
                <hr class="mB-30">

                {!! Form::open(['method' => 'POST', 'route' => ['setting.competency.store']]) !!}
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label text-right"> @lang('app.name') <span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('name'))
                                <p class="help-block">
                                    {{ $errors->first('name') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label text-right"> @lang('app.description') </label>
                        <div class="col-sm-6">
                            {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'rows' => 3, 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('description'))
                                <p class="help-block">
                                    {{ $errors->first('description') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    
                    <hr>
                    <div class="form-group">
                        <div class="ml-auto float-right">
                            <a href="{{ route('setting.competency.index') }}" class="btn btn-light">{{ trans('global.app_cancel') }}</a>
                            <button type="submit" class="btn bg-gradient-success"><i class="ti-save m-r-10"></i> {{ __('app.submit') }}</button>
                        </div>
                    </div>

                {!! Form::close() !!}       
            </div>  
        </div>
    </div>
        
@endsection
