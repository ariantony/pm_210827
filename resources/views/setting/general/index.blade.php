@extends('layouts.app')

@section('page-header')
    @lang('app.setting')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="#">@lang('app.setting')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('app.general')</li>
    </ol>
@endsection

@section('content')

    <div class="row mB-40">
 
        @include('setting.menu')

        <div class="col-sm-9">
                    
            <div class="card">
                <div class="card-body p-30">
                
                    <div class="row">
                        <div class="col-lg-12">
                            <h5 class="header-title">
                                {{ __('app.setting') }} 
                            </h5>
                            <hr>

                            {!! Form::open(['method' => 'POST', 'route' => ['setting.general.store'],'id' =>'form' ]) !!}
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">{{ __('app.setting_field_1') }}  
                                    <div class="error text-danger"></div>
                                </label>
                                <div class="col-lg-2">
                                    <label>{{ __('pm.goals') }}</label>
                                    <div class="input-group">
                                        <input type="hidden" name="key[]"  value="weight_kpi">
                                        <input type="number" id="weight_kpi" step='0.01' name="val[]"  value="{{ getSetting('weight_kpi') }}" class="form-control" >
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="fa fa-percent"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label>{{ __('pm.competency') }}</label>
                                    <div class="input-group">
                                        <input type="hidden" name="key[]"  value="weight_competency">
                                        <input type="number" id="weight_competency" step='0.01' name="val[]"  value="{{ getSetting('weight_competency') }}" class="form-control" >
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="fa fa-percent"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>	
                            <hr>
                            <div class="form-group mt-4">
                                <div class="ml-auto float-right">
                                    <button type="submit" class="btn bg-gradient-success" id="submit"><i class="ti-save m-r-10"></i> {{ __('app.submit') }}</button>
                                </div>
                            </div>
                        </form>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

@endsection

@section('js')
<script>
    $(document).ready(function() {

        $("#form").validate({

        rules: {
            weight_kpi: {
                required: true,
            },
            weight_competency: {
                required: true,
            }

        },
        submitHandler: function(form){
            var total = parseInt($("#weight_kpi").val()) + parseInt($("#weight_competency").val()); 
            if (total != 100) {
                $("#form div.error").html("Your percantage fields must sum to 100.")
                return false;
            } else form.submit();
        }

        }); 
    });

    </script>
@endsection