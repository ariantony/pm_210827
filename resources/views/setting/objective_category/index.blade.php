@extends('layouts.app')

@section('page-header')
    @lang('app.setting')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="#">@lang('app.setting')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('pm.objective_category')</li>
    </ol>
@endsection

@section('content')

<div class="row mB-40">
 
    @include('setting.menu')

    <div class="col-sm-9">
        <div class="card p-20 bd">
            <h6>
                {{ trans('pm.objective_category') }}
            </h6>
            <hr class="mB-30">
            <div class="mB-20">
                <a href="{{ route('setting.objective_category.create') }}" class="btn btn-info">
                    {{ trans('global.app_create') }}
                </a>
            </div>

            <div class="bd bdrs-3 p-20 mB-20">
                <table id="dataTables" class="table table-striped " cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{ trans('app.name') }}</th>
                            <th>{{ trans('app.description') }}</th>
                            <th>{{ trans('app.status') }}</th>
                            <th>{{ trans('app.updated') }}</th>
                            <th>{{ trans('app.action') }}</th>
                        </tr>
                    </thead>
               
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('setting.objective_category.datatables') }}',
            columns: [
                {data: 'name', name: 'name'},
                {data: 'description', name: 'description',  orderable: false, searchable: false},
                {data: 'status', name: 'status',  orderable: false, searchable: false},
                {data: 'updated_at', name: 'updated_at', searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@stop