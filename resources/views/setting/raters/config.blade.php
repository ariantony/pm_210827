@extends('layouts.app')

@section('page-header')
    @lang('app.setting')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="#">@lang('app.setting')</a></li>
		<li class="breadcrumb-item"><a href="{{ route('setting.rater.index') }}">@lang('pm.raters')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('pm.form_setting_raters')</li>
    </ol>
@endsection

@section('content')

	<div class="row">
		<div class="col-lg-12">
		
			<div class="card">
				<div class="card-body p-30">
				<h6>@lang ('pm.form_setting_raters') </h6>
					<hr class="mB-30">    
					<div class="form-group row">
						<label class="col-sm-2 text-right">@lang('pm.emp_name')</label>
						<div class="col-sm-6">
						: {{ $employee->firstname }}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 text-right">@lang('pm.emp_id')</label>
						<div class="col-sm-6">
						: {{ $employee->nip }}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 text-right">{{ __('pm.position') }} </label>
						<div class="col-sm-6">
							: {{ $employee->jabatan }}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 text-right">{{ __('pm.division') }} /  {{ __('pm.department') }}</label>
						<div class="col-sm-6">
							: {{ $employee->department}}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 text-right">{{ __('pm.workarea') }} </label>
						<div class="col-sm-6">
							: {{ $employee->workarea}}
						</div>
					</div>	
			
					<div class="alert alert-info m-t-30">
						@lang('pm.setting_raters_info')
					</div>

					
					<form action="{{ route('setting.rater.store') }}" method="POST">
                    {{ csrf_field() }}
						<input type="hidden" value="{{ $rater->employee_id }}" name="employee_id">

						<table class="table table-bordered">
							<thead>
								<tr>
									<th style="width:200px"> {{ __('app.group_raters') }}</th>
									<th> {{ __('pm.raters') }}</th>
								</tr>
							</thead>

							<?php 
								$rater = json_decode($rater->rate,true);
							?>

							@foreach ($rater as $val)
								<?php 
									$employee='';
									if($val['value'] != null){
										$getEmployee = getDataByParam('db_klola_hes.sys_ttrs_employee','nip',$val['value']);
										if($getEmployee){
											$employee = strtoupper($getEmployee->firstname);
										}else{
											$employee='';
										}
									}
								?>
								<tr>
									<td>
										{{ $val['key'] }}
										<input type="hidden" value="{{$val['key']}}" name="group[]">
									</td>
									<td>
										<select class="form-control employee" name="rater_id[]">
											<option value="{{$val['value']}}" selected>{{$val['value']}} - {{ $employee }}</option>
										</select>
									</td>
								</tr>
							@endforeach

						</table>
						<hr>						
						<div class="float-right">
							<div class="col-sm-12">
								<a href="{{ route('setting.group_rater.index') }}" class="btn btn-light">{{ trans('global.app_cancel') }}</a>
								<button type="submit" class="btn bg-gradient-success"><i class="ti-save m-r-10"></i> {{ __('app.submit') }}</button>
							</div>
						</div>

					</form>
					
				</div>
			</div>
				
		</div>
	</div>

@endsection

@section('js')
	<script>
		$(document).ready(function(){

			$('.employee').select2({
                placeholder: 'Cari Employee dengan mengetik 3 huruf...',
				allowClear: true,
				minimumInputLength: 3,
                ajax: {
                    url: '/get_employee/',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.nik + " [" + item.name + "/" + item.department + "/" + item.workarea + "]",
                                    id: item.nik
                                }
                            })
                        };
                    },
                    cache: true
                }
            });
			
			$(document).on("click", "#btn-submit", function(e) {
				$('input[name="status"]').val('1');
			});
		
		});
	</script>
@endsection

