@extends('layouts.app')

@section('page-header')
    @lang('app.setting')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="#">@lang('app.setting')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('setting.rater.index') }}">@lang('pm.raters')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('global.app_search')</li>
    </ol>
@endsection

@section('content')

    <div class="row">
    <div class="col-lg-12">
                    
        <div class="card">
            <div class="card-body p-30">
            
                <div class="pad-btm">
                    <div class="table-toolbar-right">
                        <div class="btn-group">
                        <button class="btn btn-default"  data-toggle="collapse" data-target="#filter"><i class="ti-filter"></i> Filter</button>
                        <a href="{{ route('setting.rater.import') }}" class="btn bg-gradient-success"><i class="fa fa-file-excel-o"></i> Import</a>
                        </div>
                    </div>

                    <div class="collapse" id="filter">
                        <hr>
                        <form class="form-inline" action="{{ route('setting.rater.search')}}" method='GET'>
                            {{ csrf_field() }}
                            <div class="form-group mb-2 col-sm-3">
                                <label>{{ trans('pm.company') }}</label>
                                {!! Form::select('company_id', $company, null, ['class' => 'form-control select2']) !!}
                            </div>
                            <div class="form-group mb-2 col-sm-3">
                                <label>{{ trans('pm.workarea') }}</label>
                                {!! Form::select('workarea_id', $workarea, null, ['class' => 'form-control select2']) !!}
                            </div>
                            <div class="form-group mb-2 col-sm-3">
                                <label>{{ trans('pm.department') }}</label>
                                {!! Form::select('department_id', $department, null, ['class' => 'form-control select2']) !!}
                            </div>
                            <div class="form-group mb-2 col-sm-3">
                                <label>{{ trans('pm.level') }}</label>
                                {!! Form::select('level_id', $level, null, ['class' => 'form-control select2']) !!}
                            </div>
                            <div class="form-group m-t-20 col-sm-12">
                                <button type="submit" class="btn bg-gradient-blue float-right">@lang('global.app_search')</button>
                            </div>
                        </form>
                        <hr>
                    </div>

                </div>

                <p>{!! $search !!}</p>
                <div class="table-responsive">
                    <table id="dataTables" class="table table-striped " cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>{{ trans('pm.emp_id') }}</th>
                                <th>{{ trans('pm.emp_name') }}</th>
                                <th>{{ trans('pm.workarea') }}</th>
                                <th>{{ trans('pm.department') }}</th>
                                <th>{{ trans('app.action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($rater) > 0)
                                @foreach ($rater as $item)
                                    <tr >
                                        <td>{{ $item->nip }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->workarea }}</td>
                                        <td>{{ $item->department }}</td>
                                        <td>
                                            <?php
                                                $url_config = "<a href='".route('setting.rater.config',$item->nip)."' title='".trans('global.app_edit')."' data-toggle='tooltip' class='btn btn-outline'><span class='ti-settings icon-lg'></span> </a>";  
                                                echo $url_config;
                                            ?>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan="5" class="text-center"> @lang('global.app_no_entries_in_table')</td></tr>
                            @endif
                        </tbody>
                    </table>
                    {{ $rater->links() }}
                </div>

            </div>
        </div>
            
    </div>
    </div>

@endsection

@section('js')
    <script>
    $(document).ready(function() {

        var $company = $(".company");
            var $department = $(".department");
            var $workarea = $(".workarea");
           
            $department.select2({
                placeholder:"{{ __('global.app_choose') }}"
            });
            $workarea.select2({
                placeholder:"{{ __('global.app_choose') }}"
            });

            $company.select2({
                placeholder:"{{ __('global.app_choose') }}"
            }).on('change', function() {
                $.ajax({
                    url:"/get_department/" + $company.val(), 
                    type:'GET',
                    success:function(data) {
                        $department.empty();
                        $department.append($("<option></option>").attr("value", "").text("{{ __('global.app_choose') }}")); 
                        $.each(data, function(value, key) {
                            $department.append($("<option></option>").attr("value", value).text(key)); 
                        });
                        $department.select2();
                    }
                });
                $.ajax({
                    url:"/get_workarea/" + $company.val(), 
                    type:'GET',
                    success:function(data) {
                        $workarea.empty();
                        $workarea.append($("<option></option>").attr("value", "").text("{{ __('global.app_choose') }}")); 
                        $.each(data, function(value, key) {
                            $workarea.append($("<option></option>").attr("value", value).text(key));
                        });
                        $workarea.select2();
                    }
                });
            });

    });
</script>
@stop