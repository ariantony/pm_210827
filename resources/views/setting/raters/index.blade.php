@extends('layouts.app')

@section('page-header')
    @lang('app.setting')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="#">@lang('app.setting')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('pm.raters')</li>
    </ol>
@endsection

@section('content')

    <div class="row">
    <div class="col-lg-12">
                    
        <div class="card">
            <div class="card-body p-30">
            
                <div class="pad-btm">
                    <div class="table-toolbar-right">
                        <div class="btn-group">
                        <button class="btn btn-default"  data-toggle="collapse" data-target="#filter"><i class="ti-filter"></i> Filter</button>
                        <a href="{{ route('setting.rater.import') }}" class="btn bg-gradient-success"><i class="fa fa-file-excel-o"></i> Import</a>
                        </div>
                    </div>

                    <div class="collapse" id="filter">
                        <hr>
                        <form class="form-inline" action="{{ route('setting.rater.search')}}" method='GET'>
                            {{ csrf_field() }}
                            <div class="form-group mb-2 col-sm-3">
                                <label>{{ trans('pm.company') }}</label>
                                {!! Form::select('company_id', $company, null, ['class' => 'form-control select2']) !!}
                            </div>
                            <div class="form-group mb-2 col-sm-3">
                                <label>{{ trans('pm.workarea') }}</label>
                                {!! Form::select('workarea_id', $workarea, null, ['class' => 'form-control select2']) !!}
                            </div>
                            <div class="form-group mb-2 col-sm-3">
                                <label>{{ trans('pm.department') }}</label>
                                {!! Form::select('department_id', $department, null, ['class' => 'form-control select2']) !!}
                            </div>
                            <div class="form-group mb-2 col-sm-3">
                                <label>{{ trans('pm.level') }}</label>
                                {!! Form::select('level_id', $level, null, ['class' => 'form-control select2']) !!}
                            </div>
                            <div class="form-group m-t-20 col-sm-12">
                                <button type="submit" class="btn bg-gradient-blue float-right">@lang('global.app_search')</button>
                            </div>
                        </form>
                        <hr>
                    </div>

                </div>

                <div class="table-responsive">
                    <table id="dataTables" class="table table-striped " cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>{{ trans('pm.emp_id') }}</th>
                                <th>{{ trans('pm.emp_name') }}</th>
                                <th>{{ trans('pm.position') }}</th>
                                <th>{{ trans('pm.department') }}</th>
                                <th>{{ trans('pm.workarea') }}</th>
                                <th>{{ trans('app.action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
            
    </div>
    </div>

@endsection

@section('js')
    <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('setting.rater.datatables') }}',
            columns: [
                {data: 'employee_id', name: 'employee_id'},
                {data: 'nama', name: 'employee.firstname'},
                {data: 'jabatan', name: 'occupation.alias', searchable: false},
                {data: 'department', name: 'department.name', searchable: false},
                {data: 'workarea', name: 'unit.name', searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });


        var $company = $(".company");
            var $department = $(".department");
            var $workarea = $(".workarea");
           
            $department.select2({
                placeholder:"{{ __('global.app_choose') }}"
            });
            $workarea.select2({
                placeholder:"{{ __('global.app_choose') }}"
            });

    });
</script>
@stop