@extends('layouts.app')

@section('page-header')
    @lang('app.setting')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="#">@lang('app.setting')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('setting.rater.index') }}">@lang('pm.raters')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('app.import')</li>
    </ol>
@endsection

@section('content')

	<div class="row mB-40">
   
        <div class="col-sm-12">
            <div class="card p-20 bd">
                
                <h6><a class="float-left" href="{{ route('setting.rater.index') }}"><i class="ti-arrow-left m-r-10"></i></a>   @lang('pm.raters')</h6>
                <hr class="mB-30">
                <div class="alert alert-info">
                    1. Gunakan template ini untuk mengisi settingan Rater <a download href="/docs/template-rater.xlsx" class="btn btn-md bord-all" title="@lang('global.app_download')" data-toggle='tooltip'><i class="fa fa-file-excel-o text-success"></i> </a> <br>
                    2. Clear All Setting, befora upload 
                    
                    <form class='delete' action="{{ route('setting.rater.destroy') }}" method='POST'>
                         {{csrf_field()}}
                        <button class='btn btn-md bord-all' title="@lang('global.app_clearall')" data-toggle='tooltip'  ><i class='ti-trash icon-lg text-danger'></i></button>
                    </form>
                </div>
                <form action="{{ route('setting.rater.importstore') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                     <div class="form-group row mt-4">
                        <label class="col-sm-3 text-right">Choose your xls/csv File 
                        </label>
                        <div class="col-sm-5">
                            <input type="file" name="file" class="form-control">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="ml-auto float-right">
	                        <a href="{{ route('setting.rater.index') }}" class="btn btn-light">{{ trans('global.app_cancel') }}</a>
                            <button type="submit" class="btn bg-gradient-success"><i class="ti-save m-r-10"></i> {{ __('app.submit') }}</button>
                        </div>
                    </div>

                {!! Form::close() !!}       
            </div>  
        </div>
    </div>
        
@endsection
