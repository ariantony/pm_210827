    <!-- Footer -->
	<footer id="footer">
        <div class="pad-lft"> 
            Powered By <a href="http://klola.id" target="_blank"> <img class="img-fluid img-login text-center ml-1" src="/images/logo.png" style="width:35px"></a> 
        <span class="float-right"> 
            Made with <i class="fa fa-heart text-danger"></i> in Indonesia.
        </span>
        </div>
    </footer>
    
	<button class="scroll-top btn">
		<i class="ti-angle-up chevron-up"></i>
    </button>