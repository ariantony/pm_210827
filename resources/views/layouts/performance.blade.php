<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    
    @yield('css')

</head>
    <body id="app">

    <header id="navbar" class="bg-white">
        <!--Navbar Dropdown-->
        <nav class="navbar navbar-expand-lg navbar-light p-0">
            <a class="nav-link active" href="{{ route('myperformance.dashboard') }}"> 
                <img class="img-fluid img-login text-center" src="/images/{{ config('app.logo') }}" style="height:30px">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-3">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('myperformance.personal.index') }}"> {{ __('pm.my_performance') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('myassessment.index') }}"> {{ __('pm.my_assessment') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('myapproval.index') }}"> {{ __('pm.my_approval') }}</a>
                    </li>
                </ul>
            </div>

            <ul class="nav navbar-top-links">
                
                <li id="dropdown-user" class="dropdown dropdown-caret">
                    <a href="" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                        <span class="fsz-sm c-grey-900">Hi, {{ Session::get('emp_name')}}</span>
                    </a>
                    <ul class="dropdown-menu fsz-sm">
                        <li class="dropdown-item">
                            <a href="/logout">
                                <i class="ti-power-off m-r-10"></i>
                                <span>@lang('app.logout')</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown dropdown-without-caret">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                        <span class="flag {{ str_replace('_', '-', app()->getLocale()) }}"></span> {{ strtoupper(str_replace('_', '-', app()->getLocale())) }}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        @if ( str_replace('_', '-', app()->getLocale()) !='id')
                        <a href="{{ url('locale/id') }}" class="media dropdown-item"><span class="flag id"></span> ID</a>
                        @elseif ( str_replace('_', '-', app()->getLocale()) !='en')
                        <a href="{{ url('locale/en') }}" class="media dropdown-item"><span class="flag en"></span> EN</a>
                        @endif
                    </div>
                </li>
                
            </ul>
        </nav>
        <!--End Navbar Dropdown-->
    </header>

        <div id="page-content">
            <h4 class="c-grey-900 mT-10">@yield('page-header')</h4>
            <div class="mb-3">@yield('breadcrumbs')</div>
            @include('layouts.messages') 

            <div class="row justify-content-between">
                @yield('content')
            </div>
        </div>

        @include('layouts.footer')
        <script src="{{ mix('/js/app.js') }}"></script>
        <script src="{{ asset('/js/index.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

        @yield('js')

    </body>
</html>
