<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    
    @yield('css')

</head>
    <body id="app">

        @include('layouts.header')

        <div id="page-content">
            <h4 class="c-grey-900 mT-10">@yield('page-header')</h4>
            <div class="mb-3">@yield('breadcrumbs')</div>
            @include('layouts.messages') 

            @yield('content')
        </div>

        @include('layouts.footer')

        <script src="{{ mix('/js/app.js') }}"></script>
        <script src="{{ asset('/js/index.js') }}"></script>
       
        @yield('js')

    </body>
</html>
