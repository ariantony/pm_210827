<header id="navbar" class="bg-white">
    <!--Navbar Dropdown-->
    <nav class="navbar navbar-expand-lg navbar-light p-0">
        <a class="nav-link active" href="{{ route('admin.dashboard') }}"> 
            <img class="img-fluid img-login text-center" src="/images/{{ config('app.logo') }}" style="height:30px">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <?php if ( Gate::allows('performance')) { ?>
                    <li class="dropdown nav-item">
                        <a class="dropdown-toggle nav-link" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="title">{{ __('pm.performance') }}</span>
                        </a>
                        <ul class="dropdown-menu multi-level">
                            <li class="dropdown-item"> <a href="{{ route('performance.data') }}">{{ __('pm.performance') }}</a></li>
                            <li class="dropdown-item"> <a href="{{ route('performance.monitoring') }}">{{ __('app.monitoring') }} {{ __('pm.assessment') }}</a></li>
                            <li class="dropdown-item"> <a href="{{ route('performance.monitoring.kpi') }}">{{ __('app.monitoring') }} {{ __('pm.performance') }}</a></li>
                            <li class="dropdown-item"> <a href="{{ route('performance.adjust.upload') }}">{{ __('pm.adjusted_score') }} </a></li>
                        </ul>
                    </li>
                <?php } ?>

                <?php if ( Gate::allows('performance')) { ?>
                    <li class="dropdown nav-item">
                        <a href="{{ route('admin.announcement.index') }}" class="nav-link">
                            <span class="title">@lang('app.announcement')</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if ( Gate::allows('setting')) { ?>
                    <li class="dropdown nav-item">
                        <a class="dropdown-toggle nav-link" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="title">@lang('app.setting')</span>
                        </a>
                        <ul class="dropdown-menu multi-level">
                            <li class="dropdown-item"> <a href="{{ route('setting.rater.index') }}">@lang('pm.raters')</a> </li>
                            <li class="dropdown-item"> <a href="{{ route('setting.general.index') }}">@lang('app.configuration')</a> </li>
                        </ul>
                    </li>
                <?php } ?>

                <?php if ( Gate::allows('setting')) { ?>
                    <li class="dropdown nav-item">
                        <a class="dropdown-toggle nav-link" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="title">{{ __('global.user-management.title') }}</span>
                        </a>
                        <ul class="dropdown-menu multi-level">
                            <li class="dropdown-item"> <a href="{{ route('admin.users.index') }}">{{ __('global.users.title') }}</a></li>
                            <li class="dropdown-item"> <a href="{{ route('admin.roles.index') }}">{{ __('global.roles.title') }}</a></li>
                            <li class="dropdown-item"> <a href="{{ route('admin.permissions.index') }}">{{ __('global.permissions.title') }}</a></li>
                        </ul>
                    </li>
                <?php } ?>

            </ul>
        </div>

        <ul class="nav navbar-top-links">

            <li id="dropdown-user" class="dropdown dropdown-without-caret">
                <a href="" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                    @if (auth()->user()->photo != null) 
                        <img class="w-2r h-2r bdrs-50p" src="{{ asset('storage'.auth()->user()->photo) }}" alt="" style="width:32px">
                    @else
                        <img class="w-2r h-2r bdrs-50p" src="/images/avatar.png" style="width:32px"> 
                    @endif
                    <span class="fsz-sm c-grey-900">{{ auth()->user()->name }}</span>
                </a>
                <ul class="dropdown-menu fsz-sm">
                    <li class="dropdown-item">
                        <a href="/profile/change_profile" >
                            <i class="ti-user m-r-10"></i>
                            <span>@lang('app.change_profile')</span>
                        </a>
                    </li>
                    <li  class="dropdown-item">
                        <a href="/profile/change_password">
                            <i class="ti-lock m-r-10"></i>
                            <span>@lang('app.change_password')</span>
                        </a>
                    </li>
                    <li role="separator" class="dropdown-divider"></li>
                    <li class="dropdown-item">
                        <a href="/logout">
                            <i class="ti-power-off m-r-10"></i>
                            <span>@lang('app.logout')</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="dropdown dropdown-without-caret">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                    <span class="flag {{ str_replace('_', '-', app()->getLocale()) }}"></span> {{ strtoupper(str_replace('_', '-', app()->getLocale())) }}
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    @if ( str_replace('_', '-', app()->getLocale()) !='id')
                    <a href="{{ url('locale/id') }}" class="media dropdown-item"><span class="flag id"></span> ID</a>
                    @elseif ( str_replace('_', '-', app()->getLocale()) !='en')
                    <a href="{{ url('locale/en') }}" class="media dropdown-item"><span class="flag en"></span> EN</a>
                    @endif
                </div>
            </li>

        </ul>
    </nav>
    <!--End Navbar Dropdown-->
</header>