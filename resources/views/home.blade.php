@extends('layouts.auth')

@section('content')

    <div class="row no-gutters" id="main-wrapper">
        <div class="col-lg-7 d-none d-lg-block">
            <div class="section bg-primary">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                    <div class="text-center">
                            <img class="img-fluid img-login" src="/images/bg.png">
                            <h4 class="mt-5 text-white font-bold ">Performance Management System</h4>
                            <p class="text-white font-14 mt-3">{{ __('app.klola_description') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-5">
            <div class="section bg-white">
            <div class="dropdown dropdown-without-caret pos-abs" style="bottom:20px;right:20px">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                    <span class="flag {{ str_replace('_', '-', app()->getLocale()) }}"></span> {{ strtoupper(str_replace('_', '-', app()->getLocale())) }}
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    @if ( str_replace('_', '-', app()->getLocale()) !='id')
                    <a href="{{ url('locale/id') }}" class="media dropdown-item"><span class="flag id"></span> ID</a>
                    @elseif ( str_replace('_', '-', app()->getLocale()) !='en')
                    <a href="{{ url('locale/en') }}" class="media dropdown-item"><span class="flag en"></span> EN</a>
                    @endif
                </div>
            </div>
                <div class="auth-box ">
                    <div class="form-box">
                        <div class="text-center">
                            <img class="img-fluid img-login text-center" src="/images/{{ config('app.logo') }}" style="width:130px">
                        </div>

                        @if (session('error'))
                            <div class="alert alert-danger mt-4" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                {{ session('error') }}
                            </div>
                        @endif

                        <form class="form-horizontal m-t-30" id="loginform" method="POST" action="{{ route('signin') }}">
                            @csrf
                            <div class="form-group m-b-20">
                                <label for="input" class="control-label text-light">{{ __('app.input_nik') }}</label><i class="bar border-white"></i>
                                <input class="form-control @error('nik') is-invalid @enderror" name="nik" value="{{ old('nik') }}" required autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group m-b-20">
                                <label for="input" class="control-label text-light">{{ __('app.password') }}</label><i class="bar border-white"></i>
                                <input id="password-field" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group-material m-t-20">
                                <button class="btn bg-gradient-success btn-md p-t-10 p-b-10  text-center btn-block text-uppercase font-bold" type="submit">{{ __('app.continue') }}</button>
                            </div>
                        </form>
                        <div class="text-muted mt-4">
                            Powered By <a href="http://klola.id" target="_blank">
                            <img class="img-fluid img-login text-center ml-1" src="/images/logo.png" style="width:35px">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
