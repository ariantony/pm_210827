@extends('layouts.performance')

@section('content')


    <div class="col-sm-4">
        <div class="card">
            <div class="p-25">
                <div class="float-right">
                    <a class="text-semibold"  href="{{ route('announcement.read')}}">@lang('global.app_showall')</a>
                </div>
                <h5 class="header-title"><i class="ti-announcement font-bold mr-2 icon-lg"></i>  @lang('app.announcement')</h5>
                <hr>
                <div class="layer w-100 mt-3">
                    <a href="#" class="nav-link font-weight-bold p-0 m-b-10 " data-toggle="modal" data-target="#modalAnnouncement">{{ $announcement->title }}</a>
                    <?php echo substr($announcement->content,0, 100) ?>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body p-25">
                <div class="float-right">
                    <a class="text-semibold" href="{{ route('myassessment.index') }}">@lang('global.app_showall')</a>
                </div>
                <h5 class="header-title"><i class="ti-comment-alt font-bold mr-2 icon-lg"></i> {{__('pm.assessment')}}</h5>
                <hr>
                @if(count($assessment) > 0)
                    @foreach ($assessment as $item)
                    <?php 
                        if($item->group_rate =='superior'){
                            if (isStaff($item->level_id)) { 
                                    $assessment_action = '';
                                    if($item->status == 0 ){
                                        $url  = "<a href='".route('myassessment.assess', ['type' => 'midyear' , 'id' => Hashids::encode($item->id)])."' title='".trans('pm.assessment')."' data-toggle='tooltip' class='btn btn-warning btn-sm text-white mr-1'> MID YEAR</a>";  
                                        $assessment_action .= $url;
                                    }
                                    if($item->status == 1 ){
                                        $url = "<a href='".route('myassessment.assess', ['type' => 'yearend' , 'id' => Hashids::encode($item->id)])."' title='".trans('pm.assessment')."' data-toggle='tooltip' class='btn btn-danger btn-sm text-white'> YEAR END</a>";  
                                        $assessment_action .= $url;
                                    }
                                ?>
                                <div class="media">
                                    @if ( $item->avatar != null) 
                                        <img class="w-2r h-2r rounded-circle img-thumbnail" src="{{ getAvatar($item->emp_nik) }}" alt="" style="width:50px;height:50px">
                                    @else
                                        <img class="w-2r h-2r rounded-circle img-thumbnail" src="/images/avatar.png" style="width:30px"> 
                                    @endif
                                    <div class="media-body ml-2">
                                        <div class="float-right">
                                            <?php echo $assessment_action; ?>
                                            <br>
                                            <span class="text-primary font-11 text-uppercase text-bold">{{ $item->group }}</span>
                                        </div>
                                        <span class="text-primary font-12 text-uppercase text-bold">{{ $item->emp_name }}</span> <br><span class="font-10">{{ $item->emp_occupation }} -  {{ $item->emp_department }}</span>
                                        <hr>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if( $item->is_achievement_midyear == 1 || $item->is_achievement_yearend == 1) { 
                                    $assessment_action = '';
                                    if($item->status == 0 && $item->is_achievement_midyear == 1 ){
                                        $url_midyear  = "<a href='".route('myassessment.assess', ['type' => 'midyear' , 'id' => Hashids::encode($item->id)])."' title='".trans('pm.assessment')."' data-toggle='tooltip' class='btn btn-warning btn-sm text-white mr-1'> MID YEAR</a>";  
                                        $assessment_action .= $url_midyear;
                                    }
                                    if($item->status == 1 && $item->is_achievement_yearend == 1){
                                        $url_yearend  = "<a href='".route('myassessment.assess', ['type' => 'yearend' , 'id' => Hashids::encode($item->id)])."' title='".trans('pm.assessment')."' data-toggle='tooltip' class='btn btn-danger btn-sm text-white'> YEAR END</a>";  
                                        $assessment_action .= $url_yearend;
                                    }
                                ?>
                                <div class="media">
                                    @if ( $item->avatar != null) 
                                        <img class="w-2r h-2r rounded-circle img-thumbnail" src="{{ getAvatar($item->emp_nik) }}" alt="" style="width:50px;height:50px">
                                    @else
                                        <img class="w-2r h-2r rounded-circle img-thumbnail" src="/images/avatar.png" style="width:30px"> 
                                    @endif
                                    <div class="media-body ml-2">
                                        <div class="float-right">
                                            <?php echo $assessment_action; ?>
                                            <br>
                                            <span class="text-primary font-11 text-uppercase text-bold">{{ $item->group }}</span>
                                        </div>
                                        <span class="text-primary font-12 text-uppercase text-bold">{{ $item->emp_name }}</span> <br><span class="font-10">{{ $item->emp_occupation }} -  {{ $item->emp_department }}</span>
                                        <hr>
                                    </div>
                                </div>
                            <?php } 
                        } else{
                            $assessment_action = '';
                            if($item->status == 0 ){
                                $url_midyear  = "<a href='".route('myassessment.assess', ['type' => 'midyear' , 'id' => Hashids::encode($item->id)])."' title='".trans('pm.assessment')."' data-toggle='tooltip' class='btn btn-warning btn-sm text-white mr-1'> MID YEAR</a>";  
                                $assessment_action .= $url_midyear;
                            }
                            if($item->status == 1 ){
                                $url_yearend  = "<a href='".route('myassessment.assess', ['type' => 'yearend' , 'id' => Hashids::encode($item->id)])."' title='".trans('pm.assessment')."' data-toggle='tooltip' class='btn btn-danger btn-sm text-white'> YEAR END</a>";  
                                $assessment_action .= $url_yearend;
                            }
                        ?>
                            <div class="media">
                                @if ( $item->avatar != null) 
                                    <img class="w-2r h-2r rounded-circle img-thumbnail" src="{{ getAvatar($item->emp_nik) }}" alt="" style="width:50px;height:50px">
                                @else
                                    <img class="w-2r h-2r rounded-circle img-thumbnail" src="/images/avatar.png" style="width:30px"> 
                                @endif
                                <div class="media-body ml-2">
                                    <div class="float-right">
                                        <?php echo $assessment_action; ?>
                                        <br>
                                        <span class="text-primary font-11 text-uppercase text-bold">{{ $item->group }}</span>
                                    </div>
                                    <span class="text-primary font-12 text-uppercase text-bold">{{ $item->emp_name }}</span> <br><span class="font-10">{{ $item->emp_occupation }} -  {{ $item->emp_department }}</span>
                                    <hr>
                                </div>
                            </div>
                        <?php }
                    ?>

                    @endforeach
                @else
                    <div class="mt-3">
                        <p>{{ __('global.app_no_data')}}</p>
                        <hr>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-sm-8">
        <div class="card">
            <div class="card-body p-30">

                <div class="media">
                    @if ( Session::get('emp_avatar') != null) 
                        <img class="w-2r h-2r bdrs-50p rounded-circle img-thumbnail" src="{{ Session::get('emp_avatar') }}" alt="" style="width:80px;height:80px">
                    @else
                        <img class="w-2r h-2r bdrs-50p rounded-circle img-thumbnail" src="/images/avatar.png" style="width:80px"> 
                    @endif
                    <div class="media-body ml-3">
                        <h5 class="text-main text-bold text-uppercase m-b-10">{{ Session::get('emp_name')}}</h5>
                        <table class="float-left" style="width: 50%;">
                            <tr>
                                <td> <span class="text-muted mr-2">{{ __('pm.emp_id') }}</span></td>
                                <td> <span class="text-muted mr-2">: {{ Session::get('emp_nik')}}</span></td>
                            </tr>
                            <tr>
                                <td> <span class="text-muted mr-2">{{ __('pm.position') }} </span></td>
                                <td> <span class="text-muted mr-2">: {{ Session::get('emp_occupation') }}</span></td>
                            </tr>
                            <tr>
                                <td> <span class="text-muted mr-2">Level</span></td>
                                <td> <span class="text-muted mr-2">: {{ Session::get('emp_level') }}</span></td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td> <span class="text-muted mr-2">{{ __('pm.department') }}</span></td>
                                <td> <span class="text-muted mr-2">: {{ Session::get('emp_department')}}</span></td>
                            </tr>
                            <tr>
                                <td> <span class="text-muted mr-2">{{ __('pm.workarea') }} </span></td>
                                <td> <span class="text-muted mr-2">: {{ Session::get('emp_unit') }}</span></td>
                            </tr>
                            <tr>
                                <td> <span class="text-muted mr-2">{{ __('pm.company') }} </span></td>
                                <td> <span class="text-muted mr-2">: {{ Session::get('emp_company') }}</span></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <hr class="new-section-xs">

                <div class="row">
                    <div class="col-md-6">
                        <p class="text-uppercase text-muted text-normal">{{ __('pm.pa_before')}} {{ $pm_score->year }}</p>
                        <div class="row mar-top">
                            <div class="col-sm-6">
                                <div class="text-center">
                                    <p class="text-5x text-thin text-main mar-no">{{ round($pm_score->pa_score,2) }}</p>
                                    <p class="text-sm">{{ __('pm.performance_appraisal')}} {{ __('pm.score')}} {{ $pm_score->is_score }}</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-group bg-trans mar-no">
                                    <li class="mb-3 text-muted"><span class="text-main font-16">{{ round($pm_score->kpi_score,2) }}</span><span class="text-sm"> {{ __('pm.kpi')}} </span></li>
                                    <li class="mb-3 text-muted"><span class="text-main font-16">{{ round($pm_score->competency_score,2) }}</span><span class="text-sm"> {{ __('pm.competency')}} </span> </li>
                                </ul>
                            </div>
                        </div>
                        @if($pm_score->id != 0)
                            <a class="btn bg-gradient-success mar-ver" href=" {{ route('myperformance.personal.show', Hashids::encode($pm_score->id)) }}">Detail Performance</a>
                        @endif
                    </div>

                    <div class="col-sm-6">
                        <p class="text-uppercase text-muted text-normal">{{ __('pm.dashoard_title_2')}} 
                        
                        </p>
                    </div>

                </div>
                
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script>

        $(document).ready(function() {

            var ctx4 = document.getElementById("chart4").getContext("2d");
            var Chart4 = new Chart(ctx4, {
                type: "horizontalBar",
                data: {
                    labels: ["Objective", "Competency"],
                    datasets: [
                        {
                            label: "2018",
                            backgroundColor: "rgba(44, 130, 190,1)",
                            hoverBackgroundColor: "rgba(44, 130, 190,0.8)",
                            data: [4.0,3.2]
                        },
                        {
                            label: "2019",
                            backgroundColor: "rgba(118, 221, 251,1)",
                            hoverBackgroundColor: "rgba(118, 221, 251,0.8)",
                            data: [4.3,4.0]
                        }
                    ]
                },
                options: {
                    responsive: true,
                    tooltips: {
                        mode: "index",
                        intersect: true
                    },
                    legend: {
                        display: false
                    },
                    scales: {
                        xAxes: [
                            {
                                scaleLabel: {
                                    display: true
                                },
                                gridLines: {
                                    display: false
                                },
                                ticks: {
                                    fontSize: 11
                                }
                            }
                        ],
                        yAxes: [
                            {
                                display: true,
                                barThickness: 15,
                                ticks: {
                                    beginAtZero: true,
                                    fontSize: 11
                                },
                                gridLines: {
                                    display: false
                                }
                            }
                        ]
                    }
                }
            });
            document.getElementById("chart4-legend").innerHTML = Chart4.generateLegend();

        });

    </script>
@endsection
