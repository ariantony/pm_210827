
@extends('layouts.performance')

@section('page-header')
    @lang('pm.my_approval')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ route('myperformance.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('myapproval.index') }}">@lang('pm.my_approval')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('app.action')</li>
    </ol>
@endsection


@section('content')

		<div class="col-lg-12">
			<div class="card">
				<div class="card-body p-30">
				<h5 class="header-title ">
					{{ __('app.form') }} {{ __('pm.my_approval') }}
				</h5>
				<hr>

				<h5 class="text-main text-bold text-uppercase m-b-10 mt-5">{{ $pm->nama }}</h5>
				<table style="width: 100%;">
					<tr>
						<td style="width: 33%;">
							<table>
								<tr>
									<td> <span class="text-muted mr-2">{{ __('pm.emp_id') }}</span></td>
									<td> <span class="text-muted mr-2">:  {{ $pm->nik}} </span></td>
								</tr>
								<tr>
									<td> <span class="text-muted mr-2">{{ __('pm.position') }} </span></td>
									<td> <span class="text-muted mr-2">: {{ $pm->jabatan }} </span></td>
								</tr>
							</table>
						</td>

						<td style="width: 33%;">
							<table>
								<tr>
									<td> <span class="text-muted mr-2">{{ __('pm.department') }}</span></td>
									<td> <span class="text-muted mr-2">: {{ $pm->department }}</span></td>
								</tr>
								<tr>
									<td> <span class="text-muted mr-2">{{ __('pm.workarea') }} </span></td>
									<td> <span class="text-muted mr-2">: {{ $pm->workarea  }} </span></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>


			    {!! Form::open(['method' => 'POST', 'route' => ['myapproval.store'], 'id'=>'formPM']) !!}
					<input type="hidden" name="pm_id" value="{{ $pm->id }}">
					
					<div class="table-responsive">
							<table class="table table-bordered table-striped mt-5">
								<thead class="text-uppercase font-11">
									<th style="width:50px"><input name="checkedAll" id="checkedAll" type="checkbox" class="magic-checkbox"><label for="checkedAll"></label> </th>
									<th>{{ __('pm.performance_aspect') }}</th>
									<th>{{ __('pm.area_of_performance') }}</th>						
									<th style="width:350px">{{ __('pm.kpi') }}</th>
									<th class="text-center">{{ __('app.weight') }} (%)</th>
									<th class="text-center">{{ __('pm.target') }}</th>
									<th class="text-center">{{ __('pm.measure') }}</th>
									<th class="text-center">{{ __('pm.target_revision') }}</th>
								</thead>
								<tbody class="item_form">
									@if (count($pm_items) > 0)
										@php
											$no = 1;
										@endphp
										@foreach ($pm_items as $item)
											<tr>
                                            	<td><input name="pm_item[]" class="checkSingle magic-checkbox" type="checkbox" value="{{ $item->id }}" id="{{ $item->id }}" checked><label for="{{ $item->id }}"></label></td>
												<td>{{$item->category}}</td>
												<td>{{$item->area}}</td>
												<td>
													{{$item->kpi}}
												</td>
												<td class="text-center">{{ $item->weight}}</td>
												<td class="text-center">
													{{ $item->target}}
												</td>
												<td class="text-center">
													@if($item->measure_id == NULL)
														{{$item->measure}}
													@else
														{{$item->measureTitle}}
													@endif	
												</td>
												<td class="text-center">
													<input type="text" name="target_revision[]" class="form-control" value="">
												</td>
												
											</tr>
											@php
												$no++
											@endphp
										@endforeach
									@endif
								</tbody>
							</table>
						</div>

					<div class="form-group mt-4">
						<div class="ml-auto float-right">
							<a class="btn btn-default" id="btn-cancel" href="{{ route('myperformance.personal.index') }}">{{ __('global.app_cancel') }}</a>
							<button type="submit" class="btn bg-gradient-success" id="btn-submit"><i class="ti-save m-r-10"></i> {{ __('global.app_submit') }}</button>
						</div>
					</div>

				</form>

				</div>
			</div>
		</div>

@endsection

@section('js')
	<script  type='text/javascript'>
		$(document).ready(function(){
			$("#formPM").validate({ 
				rules: { 
						"pm_item[]": { 
								required: true, 
								minlength: 1 
						} 
				}, 
				messages: { 
						"pm_item[]": "Minimal Checklist 1 Item"
				} 
			}); 

		
			$(document).on("click", ".btn-reject", function(e) {
				$('input[name="status"]').val('2');
			});
			$(document).on("click", ".btn-submit", function(e) {
				$('input[name="status"]').val('1');
			});

			$("#checkedAll").change(function(){
				if(this.checked){
				$(".checkSingle").each(function(){
					this.checked=true;
				})              
				}else{
				$(".checkSingle").each(function(){
					this.checked=false;
				})              
				}
			});

			$(".checkSingle").click(function () {
				if ($(this).is(":checked")){
				var isAllChecked = 0;
				$(".checkSingle").each(function(){
					if(!this.checked)
					isAllChecked = 1;
				})              
				if(isAllChecked == 0){ $("#checkedAll").prop("checked", true); }     
				}else {
				$("#checkedAll").prop("checked", false);
				}
			});

			@if (count($pm_items) > 0)
				@php
					$no=1;
				@endphp
				@foreach ($pm_items as $item)
					$('#editTarget_{{ $no }}').on('click', function(){
						$('#revision_{{ $no }}').show();
						$('#revision_close_{{ $no }}').show();
					});

					$('#revision_close_{{ $no }}').on('click', function(){
						$('#revision_{{ $no }}').hide();
						$('#revision_close_{{ $no }}').hide();
					});
					@php
						$no++;
					@endphp
				@endforeach
			@endif
			
		});

	</script>
@endsection		
