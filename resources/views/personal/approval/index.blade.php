@extends('layouts.performance')

@section('page-header')
    @lang('pm.my_approval')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('myperformance.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('pm.my_approval')</li>
    </ol>
@endsection

@section('content')

        <div class="col-lg-12">
                        
            <div class="card">
                <div class="card-body p-30">
                
                    <div class="table-responsive m-t-5">
                        <table id="dataTables" class="table table-striped " cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-uppercase">{{ trans('pm.emp_data') }}</th>
                                    <th class="text-uppercase">{{ trans('pm.periode') }}</th>
                                    <th class="text-uppercase">{{ trans('app.action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
                
        </div>

@endsection

@section('js')
    <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('myapproval.datatables') }}',
            columns: [
                {data: 'emp_data', name: 'emp_data'},
                {data: 'year', name: 'year'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>
@stop