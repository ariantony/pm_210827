@extends('layouts.performance')

@section('page-header')
    @lang('pm.my_assessment')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('myperformance.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('pm.my_assessment')</li>
    </ol>
@endsection

@section('content')

        <div class="col-lg-12">
                        
            <div class="card">
                <div class="card-body p-30">
                
                    <div class="pad-btm">
                        <div class="table-toolbar-right">
                            <div class="btn-group">
                                <a href="{{ route('myassessment.history') }}" class="btn btn-default" ><i class="ti-server"></i> @lang('pm.history')</a>
                            </div>
                        </div>
                    </div>  

                    <div class="table-responsive">
                        <table id="dataTables" class="table table-striped " cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-uppercase">{{ trans('pm.emp_data') }}</th>
                                    <th class="text-uppercase">{{ trans('pm.periode') }}</th>
                                    <th class="text-uppercase">{{ __('pm.rater_as') }} </th>
                                    <th class="text-uppercase">{{ trans('pm.assessment') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
                
        </div>

@endsection

@section('js')
    <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('myassessment.datatables') }}',
            columns: [
                {data: 'emp_data', name: 'emp_data'},
                {data: 'year', name: 'year'},
                {data: 'group', name: 'group'},
                {data: 'achievement', name: 'achievement', orderable: false, searchable: false},
            ]
        });
    });
</script>
@stop