
@extends('layouts.performance')

@section('page-header')
    @lang('pm.my_assessment')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ route('myperformance.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('myassessment.index') }}">@lang('pm.my_assessment')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('myassessment.history') }}">@lang('pm.history')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('global.app_detail')</li>
    </ol>
@endsection


@section('content')

<div class="col-lg-12">
			<div class="card">
				<div class="card-body p-30">
					@if($type == 'yearend')	
						<h5 class="text-main text-bold text-uppercase">{{ __('pm.assessment') }}  YEAR-END {{ $pm->year }} </h5>
					@else
						<h5 class="text-main text-bold text-uppercase">{{ __('pm.assessment') }}  MID-TERM {{ $pm->year }}</h5>
					@endif
					<hr>

					<table style="width: 100%;">
						<tr>
							<td style="width: 33%;">
								<table>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.emp_name') }}</span></td>
										<td> <span class="text-muted mr-2">: {{ $pm->emp_name }}</span></td>
									</tr>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.emp_id') }}</span></td>
										<td> <span class="text-muted mr-2">:  {{ $pm->emp_nik }} </span></td>
									</tr>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.position') }} </span></td>
										<td> <span class="text-muted mr-2">: {{ $pm->emp_occupation }} </span></td>
									</tr>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.level') }} </span></td>
										<td> <span class="text-muted mr-2">: {{ $pm->emp_level }} </span></td>
									</tr>
								</table>
							</td>

							<td style="width: 33%;">
								<table>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.department') }}</span></td>
										<td> <span class="text-muted mr-2">: {{ $pm->emp_department }}</span></td>
									</tr>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.workarea') }} </span></td>
										<td> <span class="text-muted mr-2">: {{ $pm->emp_workarea  }} </span></td>
									</tr>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.company') }} </span></td>
										<td> <span class="text-muted mr-2">: {{ $pm->emp_company  }} </span></td>
									</tr>
								</table>
							</td>
							<td style="width: 33%;">
								<table>
									<tr>
										<td>{{ __('pm.rater_as') }} <h6 class="text-uppercase text-danger">{{ $pm->group }}</h6></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>

					@if($pm->group_rate == 'superior'  || $pm->group_rate == 'self')
						
						@php 
							$staff = explode(",",config('app.staff_id'));
						@endphp
						@if(!in_array($pm->emp_level_id, $staff))
							<h5 class="m-t-20">{{ __('pm.kpi') }} </h5>
							<div class="table-responsive">
								<table class="table table-bordered table-striped mt-2">
									<thead class="text-uppercase font-11">
										<th>No</th>
										<th>{{ __('pm.kpi') }}</th>
										<th class="text-center">{{ __('app.weight') }} (%)</th>
										<th class="text-center">{{ __('pm.target') }}</th>
										<th class="text-center">{{ __('pm.measure') }}</th>
										<th class="text-center">{{ __('pm.midterm_result') }}</th>
										<th class="text-center">{{ __('pm.actual_annual_achievement') }}</th>
										<th class="text-center">{{ __('pm.kpi_score_achievement') }}</th>
										<th class="text-center">{{ __('pm.final_score') }} <br> <small>({{ __('app.weight') }} x {{ __('pm.kpi_score_achievement') }})</small></th>
									</thead>
									<tbody class="item_form">
										
										@if (count($pm_items) > 0)
											@php
												$no = 1;
												$weight=0;
												$kpi_score =0;
											@endphp
											@foreach ($pm_items as $item)
													<?php 
														if ($pm->is_score == 2){
															$year_score		= $item->yearend_score;
															$final_score    = ($item->weight/100) * $item->yearend_score;
															$kpi_score += $final_score;
														}
														else{
															$year_score  = '-';
															$final_score = '-';
															$kpi_score = '-';
														}
													?>

													<tr>
														<td>{{$no}}</td>
														<td><span class="badge badge-info"> {{$item->category}} </span> <br><span class="text-uppercase font-11 font-weight-bold">{{$item->area}}</span>
															<p class="mt-1">{{$item->kpi}}</p>
														</td>
														<td  class="text-center">{{ $item->weight}}</td>
														<td>{{ $item->target}}</td>
														<td>@if($item->measure_id == NULL)
																{{$item->measure}}
															@else
																{{$item->measureTitle}}
															@endif
														</td>
														<td class="text-center">
															@if($item->midyear_achievement_revision == NULL)
																{{ $item->midyear_achievement}}
															@else
															<span class="text-muted">
																<strike>
																	{{ $item->midyear_achievement}}
																</strike> 
															</span>
															<br>
																{{ $item->midyear_achievement_revision }}
															@endif	
														</td>
														<td class="text-center" >
															@if($item->yearend_achievement_revision == NULL)
																{{ $item->yearend_achievement}}
															@else
															<span class="text-muted">
																<strike>
																	{{ $item->yearend_achievement}}
																</strike> 
															</span>
															<br>
																{{ $item->yearend_achievement_revision }}
															@endif
														</td>
														<td class="text-center">{{ $year_score}}</td>
														<td class="text-center">{{ $final_score}}</td>
													</tr>
													@php
														$weight +=$item->weight;
														$no++
													@endphp
											@endforeach
										@endif
										<tr>
											<td colspan="8" class="text-right font-weight-bold">{{ __('pm.total_score') }}</td>
											<td class="text-center">{{  $kpi_score ?? '' }}</td>
										</tr>
									</tbody>
								</table>
							</div>
						@endif
						
						<h5 class="m-t-40 text-uppercase">@lang('pm.competency')</h5>
						<table class="table table-bordered table-striped mt-4">
						<thead>
							<tr class="text-uppercase">
								<th rowspan="2" style="width:50px;border-bottom: 1px solid #e6ecf5;">No</th>
								<th rowspan="2" style="width:300px;border-bottom: 1px solid #e6ecf5;">{{ __('pm.competency') }}</th>
								<th rowspan="2" style="width:500px;border-bottom: 1px solid #e6ecf5;">{{ __('pm.comment') }}</th>
								@foreach ($group_raters as $value)
									@php
										$totalspan 	= 0;
									@endphp	
									<?php 
										$colom = [];
										foreach ($appraisal as $val){ 
											if(strtolower($value->name) == $val->group_rate ){
												if($val->group_rate == 'self'){
													$totalspan = 0;
												}else{
													$totalspan  +=1;
												}
												$colom []= $value->name;
											}
										}
										$kolom = array_unique($colom );
										foreach ($kolom as $val ){
											echo"<th class='text-center' style='border-bottom: 1px solid #e6ecf5;' colspan='".$totalspan."'>".$val."</th>";
										}
									?>
								@endforeach
							</tr>
							
							<tr>

								@foreach ($group_raters as $value)
									<?php 
										$i = 1;
										foreach ($appraisal as $val){
											if(strtolower($value->name) == $val->group_rate ){
												if($val->group_rate == 'self'){
													echo "<th class='text-center bd'  style='max-width:50px;border-bottom: 1px solid #e6ecf5;'><small>1</small></th>";
												}else{
													echo "<th class='text-center bd'  style='max-width:50px;border-bottom: 1px solid #e6ecf5;'><small>".$i."</small></th>";
													$i++;
												}
											}
										}
									?>
								@endforeach
							</tr>
						</thead>
						<tbody>
							@php
								$no 			= 1;
								$totalscore 	= 0;
								$nilaiGroup = [];

							@endphp	

								
							@foreach ($assessment as $key => $value)
								<tr>
									<td>{{ $no }}</td>
									<td>
										<h6 class="font-12 font-weight-bold">{{ $value->competencyName }}</h6> <span class="font-12 text-muted">{{ $value->competencyDesc }} </span>
									</td>
									<td>
										@if($type == 'midyear')
											<?php 
												if($value->comment_midyear != NULL){
													$comment = json_decode($value->comment_midyear,true);
													foreach ($comment as $val){
														if($val['value'] != NULL){
															echo "<p class='mb-0'>- ".$val['value']."</p>";
														}else{
															echo "<p class='mb-0'>- </p>";
														}
													}
												}else{
													$comment = explode('-',$value->comment_all);
													foreach($comment as $val){
														if($val) {
															echo "- ".$val." <br>";
														}
													}
												}
											?>
										@else
											<?php 
												$comment = json_decode($value->comment_yearend,true);
												foreach ($comment as $val){
													if($val['value'] != NULL){
														echo "<p class='mb-0'>- ".$val['value']."</p>";
													}else{
														echo "<p class='mb-0'>- </p>";
													}
												}
											?>
										@endif
									</td>
									
										<?php 
											if($type == 'midyear'){
												$rate = json_decode($value->rate,true);
											}else{
												$rate = json_decode($value->rate_yearend,true);
											}
											$i=1;
											$inc =0;
										?>

										@foreach ($group_raters as $value)
											<?php 
												$i = 1;
												$nilai = [];
												foreach ($rate as $val) {
													$nilai[$val['nik']] = $val['value'];
												}

												foreach ($appraisal as $val){

													if(strtolower($value->name) == $val->group_rate ){
														if(array_key_exists($val->rater_id,$nilai)){
															echo "<td class='text-center'>".$nilai[$val->rater_id]. "</td>";
															$nilaiGroup[$val->group_rate.'_'.$inc][] = $nilai[$val->rater_id];
														}else{
															echo "<td class='text-center'>-</td>";
															$nilaiGroup[$val->group_rate.'_'.$inc][] = 0;
														}
														$inc +=1;
													}
												}
											?>
										@endforeach

								</tr>
								@php
									$group_rate_sum = $nilai;
									$no++
								@endphp

							@endforeach

						</tbody>
						
						<tfoot>
							<tr>
								<td colspan="3"></td>
								<?php

									$key = array_keys($nilaiGroup);
									
									for($i = 0;$i<count($key);$i++){
										$groupRater = explode("_",$key[$i]);
										$nilaiGroupSum = 0;
										foreach($nilaiGroup[$key[$i]] as $item){
											$nilaiGroupSum += $item;
										}
										echo "<td class='text-center'>".$nilaiGroupSum ."</td>";
									}
								?>
							</tr>
							<tr>
								<td colspan="3"></td>
								<?php

									$key = array_keys($nilaiGroup);
									$NilaiSubordinates = $NilaiPeers= $count_peers = $count_subordinates = $nilaiGroupSumSubordinates = $nilaiGroupSumPeers = $colspan_subordinates = $colspan_peers = 0;

									for($i = 0;$i<count($key);$i++){
										$groupRater = explode("_",$key[$i]);
										$nilaiGroupSum = 0;
										foreach($nilaiGroup[$key[$i]] as $item){
											$nilaiGroupSum += $item;
										}
										
										$bobotRater = getBobot($groupRater[0],$pm->rater_weight);
										$nilaiGroupRaterSum = $bobotRater /100 * $nilaiGroupSum;
										
										if($groupRater[0] == 'peers'){
											$colspan_peers ++;
											if($nilaiGroupSum != 0){
												$count_peers ++;
												$nilaiGroupSumPeers += $nilaiGroupSum ;
											}
										}

										if($groupRater[0] == 'subordinates'){
											$colspan_subordinates ++;
											if($nilaiGroupSum != 0){
												$count_subordinates ++;
												$nilaiGroupSumSubordinates += $nilaiGroupSum ;
											}
											
										}
										if($groupRater[0] == 'self'){
											$NilaiSelf  = $nilaiGroupRaterSum;
											echo "<td class='text-center'>".$nilaiGroupRaterSum ."</td>";
										}
										if($groupRater[0] == 'superior' ){
											$NilaiSuperior  = $nilaiGroupRaterSum;
											echo "<td class='text-center'>".$nilaiGroupRaterSum ."</td>";
										}
									}

									if($nilaiGroupSumPeers != 0){
										$NilaiPeers	=  round((getBobot('peers',$pm->rater_weight)/100) * $nilaiGroupSumPeers/$count_peers,2);
									}
									if($nilaiGroupSumSubordinates !=0){
										$NilaiSubordinates =  round((getBobot('subordinates',$pm->rater_weight)/100) * $nilaiGroupSumSubordinates/$count_subordinates,2);
									}
									if(	$colspan_peers > 0){
										echo "<td class='text-center' colspan='".$colspan_peers."'>".$NilaiPeers."</td>";
									}	
									if(	$colspan_peers > 0){
										echo "<td class='text-center' colspan='".$colspan_subordinates."'>".$NilaiSubordinates."</td>";
									}	
								?>
							</tr>
						
							<tr>
								<td colspan="3" class="text-right font-weight-bold">{{ __('pm.total_score') }}</td>
								<td colspan="{{ $inc }}" class="text-center">
									<?php 
										$totalNilaiPA = ($NilaiSelf  + $NilaiPeers + $NilaiSubordinates + $NilaiSuperior)/6;
										echo round($totalNilaiPA,2);
									?>
								</td>
							</tr>


						</tfoot>

					</table>
					@else
						<h5 class="m-t-40  text-uppercase">@lang('pm.competency')</h5>
						<hr>							
						<table class="table table-bordered table-striped mt-4">	
							<thead>
								<tr class="text-uppercase">
									<th style="width:50px;border-bottom: 1px solid #e6ecf5;">No</th>
									<th style="width:50%;border-bottom: 1px solid #e6ecf5;">{{ __('pm.competency') }}</th>
									<th style="width:30%;border-bottom: 1px solid #e6ecf5;">{{ __('pm.comment') }}</th>
									<th style="border-bottom: 1px solid #e6ecf5;">{{ __('pm.score') }}</th>
								</tr>
							</thead>
							<tbody>
								@php
									$no = 1;
									$totalscore = 0;
								@endphp	

												
								@foreach ($assessment as $key => $value)
									<tr>
										<td>{{ $no }}</td>
										<td>
											<h6>{{ $value->competencyName }}</h6> <span class="font-12 text-muted">{{ $value->competencyDesc }} </span>
										</td>
										<td>
											@if($type == 'midyear')
											   
												<?php 
													if($value->comment_midyear != NULL){
														$comment = json_decode($value->comment_midyear,true);
														foreach ($comment as $val){
															if($val['nik'] == $pm->rater_id){
																echo "<p class='mb-0'>".$val['value']."</p>";
															}
														}
													}else{
														$comment = explode('-',$value->comment_all);
														foreach($comment as $val){
															if($val)
															echo "-". $val."<br>";
														}
													}
												?>
												
											@else
												<?php 
													$comment = json_decode($value->comment_yearend,true);
													foreach ($comment as $val){
														if($val['nik'] == $pm->rater_id){
															echo "<p class='mb-0'>".$val['value']."</p>";
														}
													}
												?>
											@endif
										</td>
										
										<?php 
											$item_sum = 0;
											$items = 1;
											if($type == 'midyear'){
												$rate = json_decode($value->rate,true);
											}else{
												$rate = json_decode($value->rate_yearend,true);
											}

											$inc =0;
											
											foreach ($rate as $val){
												if($val['nik'] == $pm->rater_id){
													$item_sum += $val['value'];
													echo "<td class='text-center'>".$val['value']."</td>";
												}
												$inc +=1;
											}
										?>

									</tr>
									@php
										$totalscore += ($item_sum); 
										$no++
									@endphp
								@endforeach
							</tbody>
							<tfoot>
								<td colspan="3" class="text-right font-weight-bold">{{ __('pm.total_score') }}</td>
								<td  class="text-center">{{ round($totalscore/6,2) }}</td>
							</tfoot>
						</table>
					@endif
					
					

					@if($pm->group_rate == 'superior')
						
					<h5 class="m-t-40">@lang('pm.performance_appraisal')</h5>
					<table class="table table-bordered table-striped mt-4">
						@if(!in_array($pm->emp_level_id, $staff))
							<tr>	
								<td style="width:50px">1</td>
								<td> @lang('pm.kpi')</td>
								<td> <?php echo $kpi_score  ?> x  {{  $pm->kpi_weight }} %</td>
								<td class="text-right"> 
									<?php 
										$kpiScoreFinal =  $kpi_score * ($pm->kpi_weight/100);
										echo round($kpiScoreFinal,2);
									?> 
								</td>
							</tr>
							<tr>	
								<td>2</td>
								<td> @lang('pm.competency')</td>
								<td> <?php echo round($totalNilaiPA,2); ?> x  {{  $pm->competency_weight }} %</td>
								<td class="text-right"> 
									<?php 
										$competenctyScoreFinal =  $totalNilaiPA  * ($pm->competency_weight/100);
										echo round($competenctyScoreFinal,2);
									?> 
								</td>
							</tr>
							<tr>	
								<td colspan="3" class="text-right font-weight-bold text-uppercase"> 
									Total @lang('pm.performance_appraisal')
								</td> 
								<td class="text-right font-weight-bold text-uppercase"> 
									<?php 
										$ScoreFinal =  $kpiScoreFinal + $competenctyScoreFinal;
										echo round($ScoreFinal,2);
									?> 
								</td>
							</tr>
						@else
									
						<tr>	
								<td style="width:50px">1</td>
								<td> @lang('pm.competency')</td>
								<td class="text-right text-bold"> 
									<?php 
										$competenctyScoreFinal =  $totalNilaiPA;
										echo round($competenctyScoreFinal,2);
									?> 
								</td>
							</tr>	
							<tr>	
								<td colspan="2" class="text-right font-weight-bold text-uppercase"> 
									Total @lang('pm.performance_appraisal')
								</td> 
								<td class="text-right font-weight-bold text-uppercase"> 
									<?php 
										$ScoreFinal =  $competenctyScoreFinal;
										echo round($ScoreFinal,2);
									?> 
								</td>
							</tr>
									
						@endif

					</table>

						<h5 class="m-t-40 text-uppercase">@lang('pm.rater')</h5>
						<table class=" table-bordered w-md p-5">
							<thead>
								<tr>
									<th class="p-5">Group</th>
									<th class="p-5">@lang('pm.emp_name')</th>
								</tr>
							</thead>
							<tbody>
							<?php 
								$i = 1;
								foreach ($appraisal as $val){
									echo "<tr>";
										echo "<td class='text-uppercase p-5'>".$val->group_rate."</td>";
										echo "<td class='text-uppercase p-5'>".$val->emp_name."</td>";
									echo "</tr>";
								}
							?>
							</tbody>
						</table>

					@endif
					

				</div>
			</div>
		</div>

@endsection
	