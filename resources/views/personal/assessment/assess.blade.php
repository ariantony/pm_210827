
@extends('layouts.performance')

@section('page-header')
    @lang('pm.my_assessment')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ route('myperformance.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('myassessment.index') }}">@lang('pm.my_assessment')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('pm.assessment')</li>
    </ol>
@endsection


@section('content')

		<div class="col-lg-12">
			<div class="card">
				<div class="card-body p-30">
				<button type="button" class="float-right btn btn-info btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
					{{ __('pm.assessment_guide') }}
				</button>
				<h5 class="header-title ">
					{{ __('app.form') }} {{ __('pm.performance_appraisal') }}
				</h5>
				<hr>

				<h5 class="text-main text-bold text-uppercase m-b-10 mt-5">{{ $pm->emp_name }}</h5>
				<table style="width: 100%;">
					<tr>
						<td style="width: 33%;">
							<table>
								<tr>
									<td> <span class="text-muted mr-2">{{ __('pm.emp_id') }}</span></td>
									<td> <span class="text-muted mr-2">:  {{ $pm->emp_nik }} </span></td>
								</tr>
								<tr>
									<td> <span class="text-muted mr-2">{{ __('pm.position') }} </span></td>
									<td> <span class="text-muted mr-2">: {{ $pm->emp_occupation }} </span></td>
								</tr>
								<tr>
									<td> <span class="text-muted mr-2">{{ __('pm.level') }} </span></td>
									<td> <span class="text-muted mr-2">: {{ $pm->emp_level }} </span></td>
								</tr>
							</table>
						</td>

						<td style="width: 33%;">
							<table>
								<tr>
									<td> <span class="text-muted mr-2">{{ __('pm.department') }}</span></td>
									<td> <span class="text-muted mr-2">: {{ $pm->emp_department }}</span></td>
								</tr>
								<tr>
									<td> <span class="text-muted mr-2">{{ __('pm.workarea') }} </span></td>
									<td> <span class="text-muted mr-2">: {{ $pm->emp_workarea  }} </span></td>
								</tr>
								<tr>
									<td> <span class="text-muted mr-2">{{ __('pm.company') }} </span></td>
									<td> <span class="text-muted mr-2">: {{ $pm->emp_company  }} </span></td>
								</tr>
							</table>
						</td>
						<td style="width: 33%;">
							<table>
								<tr>
									<td>{{ __('pm.rater_as') }} <h6 class="text-uppercase text-danger">{{ $pm->group }}</h6></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>


			    {!! Form::open(['method' => 'POST', 'route' => ['myassessment.store'], 'id'=>'formPM']) !!}
					<input type="hidden" name="pm_id" value="{{ $pm->pm_id }}">
					<input type="hidden" name="tipe" class="form-control" value="{{$type}}" >
					<input type="hidden" name="group_rater" value="{{ $pm->group_rater }}">
					<div class="table-responsive  mt-5">
						<table class="table table-bordered table-striped mt-2">
							<thead>
								<tr>
									<th style="width:50px">No</th>
									<th >{{ __('pm.competency') }}</th>
									<th style="width:200px">{{ __('pm.score') }}</th>
									<th style="width:300px" >{{ __('pm.comment') }}</th>
								</tr>
							</thead>
							<tbody>
								@php
									$no = 1;
								@endphp		
								@foreach ($competency as $item)
									<tr class="item_{{ $no }}">
										<input type="hidden" name="competency[]" value="{{ $item->code }}">
										<input type="hidden" name="competency_id[]" value="{{ $item->id }}">
										<td>{{ $no }}</td>
										<td>
											<h6>{{ $item->name}}</h6> <span class="font-12">{{ $item->description }} </span>
										</td>
										<td>
											<ul id='stars' class="list-inline p-0" data-value="{{$no}}">
												<li class='star list-inline-item' data-value='1'>
													<i class='fa fa-star fa-2x text-muted'></i>
												</li>
												<li class='star list-inline-item' data-value='2'>
													<i class='fa fa-star fa-2x text-muted'></i>
												</li>
												<li class='star list-inline-item' data-value='3'>
													<i class='fa fa-star fa-2x text-muted'></i>
												</li>
												<li class='star list-inline-item' data-value='4'>
													<i class='fa fa-star fa-2x text-muted'></i>
												</li>
												<li class='star list-inline-item' data-value='5'>
													<i class='fa fa-star fa-2x text-muted'></i>
												</li>
											</ul>
											<br>
											<span class="text-danger rate_comp_{{ $no }}"></span>
											<input type="hidden" name="rate[]" id="rate_comp_{{ $no }}" class="rate" required>
										</td>
										<td>
											<textarea type="text" name="comment_{{ $no }}" class="form-control comment" required></textarea>
										</td>
									</tr>
									@php
										$no++
									@endphp
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="form-group mt-4">
						<div class="ml-auto float-right">
							<a class="btn btn-default" id="btn-cancel" href="{{ route('myperformance.personal.index') }}">{{ __('global.app_cancel') }}</a>
							<button type="submit" class="btn bg-gradient-success" id="btn-submit"><i class="ti-save m-r-10"></i> {{ __('global.app_submit') }}</button>
						</div>
					</div>

				</form>

				</div>
			</div>
		</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{ __('pm.assessment_guide') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			<div class="table-responsive">
				<table class="table table-bordered">
					<tbody>
						<th>{{ __('pm.score') }}</th>
						<th>{{ __('app.title') }}</th>
						<th>{{ __('app.description') }}</th>
					</tbody>
					@foreach ($score as $val)
						<tr>
							<td class="text-center">{{ $val->score }}</td>
							<td>
								@php
									$star = 5 - $val->score;
								@endphp	
								{{ $val->title }}
								<ul class="list-inline p-0">
									<?php for($i=0;$i<$val->score ;$i++){ ?>
										<li class='star list-inline-item'>
											<i class='fa fa-star icon-lg text-warning'></i>
										</li>
									<?php } ?>
									<?php for($i=0;$i<$star ;$i++){ ?>
										<li class='star list-inline-item'>
											<i class='fa fa-star icon-lg text-muted'></i>
										</li>
									<?php } ?>
								</ul>
							</td>
							<td>{{ $val->description }}</td>
						</tr>
					@endforeach
				</table>
			</div>
	 

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
	<script  type='text/javascript'>
		$(document).ready(function(){
			$('#stars li').on('mouseover', function(){
				var onStar = parseInt($(this).data('value'), 10); 
			
				$(this).parent().children('li.star').each(function(e){
				if (e < onStar) {
					$(this).children().addClass('text-warning');
					$(this).addClass('hover');
				}
				else {
					$(this).children().removeClass('text-warning');
					$(this).removeClass('hover');
				}
				});
				
			}).on('mouseout', function(){
				$(this).parent().children('li.star').each(function(e){
					if($(this).hasClass('selected')){
						$(this).children().addClass('text-warning');
					}else{
						$(this).children().removeClass('text-warning');
					}
					$(this).removeClass('hover');

				});
			});
		
			$('#stars li').on('click', function(){
				var onStar 	= parseInt($(this).data('value'), 10); 
				var stars 	= $(this).parent().children('li.star');
				
				for (i = 0; i < stars.length; i++) {
					$(stars[i]).removeClass('selected');
					$(stars[i]).children().removeClass('text-warning');
				}
				
				for (i = 0; i < onStar; i++) {
					$(stars[i]).addClass('selected');
					$(stars[i]).children().addClass('text-warning');
				}
				var value = $(this).attr("data-value");
				var code  = $(this).parent().attr("data-value");

				$('#rate_comp_'+code).val(value);
				$('.rate_comp_'+code).text('');
			});


			$(document).on("click", "#btn-submit", function(e) {

				var _this = $(this);
				var form = _this.parents('form');


				if ($(".rate").length){
					var arr_id = [];
					var validform  = '';

					$('.rate').each(function(){
						var value = $(this).val();
						var id    = $(this).attr('id');

						if (value != 0){
							validform = true;
						}else{
							arr_id.push(id);
							var current_arr = arr_id.pop();
							$('.'+current_arr).text('@lang("pm.validation_rate")');
							var element = current_arr.split(/_/);
							var focus = element[1];
							$('.item_'+focus).find('input').focus(); 
							validform = false;
							Swal.fire(
								'@lang("global.app_information")',
								'@lang("pm.info_validation_rate")',
								'warning'
							);
							return false;
						}
					});
				}else{
					validform = true;
				}

				e.preventDefault();
				if (form.valid() && validform) {
					Swal.fire({
						title: '@lang("global.app_confirmation")', 
						text: '@lang("global.app_are_you_sure")', 
						type: 'question',
						showCancelButton: true,
						confirmButtonColor: 'null',
						cancelButtonColor: 'null',
						confirmButtonClass: 'btn btn-danger',
						cancelButtonClass: 'btn btn-primary',
						confirmButtonText: '@lang("global.app_yes")', 
						cancelButtonText: '@lang("global.app_cancel")', 
					}).then(res => {
						if (res.value) {
							_this.closest("form").submit();
						}
					});
				}

			});
		
		});



	</script>
@endsection		
