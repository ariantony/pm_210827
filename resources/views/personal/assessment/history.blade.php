@extends('layouts.performance')

@section('page-header')
    @lang('pm.my_assessment')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('myperformance.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('myassessment.index') }}">@lang('pm.my_assessment')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('pm.history')</li>
    </ol>
@endsection

@section('content')

        <div class="col-lg-12">
                        
            <div class="card">
                <div class="card-body p-30">
                
                    <div class="pad-btm">
                        <div class="table-toolbar-right">
                            <div class="btn-group">
                                <button class="btn btn-default"  data-toggle="collapse" data-target="#filter"><i class="ti-filter"></i> Filter</button>
                            </div>
                        </div>
                        <div class="collapse" id="filter">
                            <hr>
                            <form class="form-inline">
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{__('pm.year')}}</label><br>
							        <input type="text" name="year" class="form-control yearpicker">
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.department') }}</label>
                                    {!! Form::select('department_id', $department, null, ['class' => 'form-control select2', 'required' => '']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.workarea') }}</label>
                                    {!! Form::select('workarea_id', $workarea, null, ['class' => 'form-control select2', 'required' => '']) !!}
                                </div>
                                <div class="float-right">
                                    <button type="submit" class="btn bg-gradient-blue">FILTER</button>
                                </div>
                            </form>
                            <hr>
                        </div>
                    </div>  

                    <div class="table-responsive">
                        <table id="dataTables" class="table table-striped " cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-uppercase">{{ trans('pm.emp_data') }}</th>
                                    <th class="text-uppercase">{{ __('pm.rater_as') }}</th>
                                    <th class="text-uppercase">{{ trans('pm.periode') }}</th>
                                    <th class="text-uppercase">{{ trans('app.action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
                
        </div>

@endsection

@section('js')
    <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('myassessment.history.datatables','kpi') }}',
            columns: [
                {data: 'emp_data', name: 'emp_data'},
                {data: 'group', name: 'group'},
                {data: 'year', name: 'year'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@stop