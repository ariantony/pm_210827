
@extends('layouts.performance')

@section('page-header')
	@lang('pm.my_assessment')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ route('myperformance.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('myassessment.index') }}">@lang('pm.my_assessment')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('pm.assessment') </li>
    </ol>
@endsection


@section('content')

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body p-30">
					<h5 class="header-title text-uppercase ">
						{{ __('app.form') }} {{ __('pm.yearend') }}
					</h5>
					<hr>

					<table style="width: 100%;">
						<tr>
							<td style="width: 33%;">
								<table>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.emp_name') }}</span></td>
										<td> <span class="text-muted mr-2">:  {{ $pm->emp_name }} </span></td>
									</tr>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.emp_id') }}</span></td>
										<td> <span class="text-muted mr-2">:  {{ $pm->emp_nik }} </span></td>
									</tr>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.position') }} </span></td>
										<td> <span class="text-muted mr-2">: {{ $pm->emp_occupation }} </span></td>
									</tr>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.level') }} </span></td>
										<td> <span class="text-muted mr-2">: {{ $pm->emp_level  }} </span></td>
									</tr>
								</table>
							</td>

							<td style="width: 33%;">
								<table>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.department') }}</span></td>
										<td> <span class="text-muted mr-2">: {{ $pm->emp_department }}</span></td>
									</tr>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.workarea') }} </span></td>
										<td> <span class="text-muted mr-2">: {{ $pm->emp_workarea  }} </span></td>
									</tr>
									<tr>
										<td> <span class="text-muted mr-2">{{ __('pm.company') }} </span></td>
										<td> <span class="text-muted mr-2">: {{ $pm->emp_company  }} </span></td>
									</tr>
								</table>
							</td>
							<td style="width: 33%;">
								<table>
									<tr>
										<td>{{ __('pm.rater_as') }} <h6 class="text-uppercase text-danger">{{ $pm->group }}</h6></td>
									</tr>
									
								</table>
							</td>
						</tr>
					</table>

					<hr>
			
					

					<form action="{{ route('myassessment.store') }}" method="POST" id="form">
						<input type="hidden" name="tipe" class="form-control" value="{{$type}}" >
						<input type="hidden" value="{{ $pm->pm_id }}" name="pm_id">
						<input type="hidden" value="{{ $pm->id }}" name="assessment_id">
						<input type="hidden" name="group_rater" value="{{ $pm->group_rater }}">

						{{ csrf_field() }}	

						<div class="step-app">
							<ul class="step-steps">
								<li><a href="#step1"><small>Step 1 0f 2 </small><br><span class="font-weight-bold text-uppercase">@lang('pm.kpi')</span></a></li>
								<li><a href="#step2"><small>Step 2 0f 2 </small><br><span class="font-weight-bold text-uppercase">@lang('pm.competency')</span></a></li>
							</ul>
							<div class="step-content" style="border:none !important">

								<!-- STEP 1 -->
                           	 	<div class="step-tab-panel" id="step1">
									<button type="button" class="float-right btn btn-info btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
										{{ __('pm.assessment_guide') }}
									</button>
									<div class="table-responsive">
										<table class="table table-bordered table-striped mt-4">
											<thead class="text-uppercase text-center font-11">
												<th>No</th>
												<th style="width:300px">{{ __('pm.kpi') }}</th>
												<th class="text-center">{{ __('app.weight') }} (%)</th>
												<th class="text-center">{{ __('pm.target') }}</th>
												<th class="text-center">{{ __('pm.measure') }}</th>
												<th class="text-center">{{ __('pm.midterm_result') }}</th>
												<th class="text-center">{{ __('pm.midterm_kpi_score') }}</th>
												<th class="text-center">{{ __('pm.actual_annual_achievement') }}</th>
												<th class="text-center" style="width:170px">{{ __('pm.kpi_score_achievement') }}</th>
												<th class="text-center">{{ __('pm.final_score') }} <br> <small>({{ __('app.weight') }} x {{ __('pm.kpi_score_achievement') }})</small></th>
											</thead>
											<tbody class="item_form">
												
												@if (count($kpi_items) > 0)
													@php
														$no = 1;
														$weight=0;
														$sum_final_score=0;
													@endphp
													@foreach ($kpi_items as $item)
															<?php 
																if($pm->is_score ==2){
																	$final_score 	= ($item->weight/100) * $item->yearend_score;
																	$sum_final_score += $final_score;
																}
																else{
																	$final_score = '-';
																	$sum_final_score = '-';
																}
															?>

															<tr class="kpi_item_{{$no}}">
																<input type="hidden" name="weight[]" class="form-control" value="{{$item->weight}}" id="weight_{{$no}}">
																<input type="hidden" name="pm_item[]" class="form-control" value="{{ $item->id }}">
																<td>{{$no}}</td>
																<td><span class="badge badge-info"> {{$item->category}} </span> <br><span class="text-uppercase font-11 font-weight-bold">{{$item->area}}</span>
																	<p class="mt-1">{{$item->kpi}}</p>
																</td>
																<td class="text-center">{{$item->weight}}</td>
																<td class="text-center">{{$item->target}}</td>
																<td class="text-center">
																	@if($item->measure_id == NULL)
																		{{$item->measure}}
																	@else
																		{{$item->measureTitle}}
																	@endif
																</td>
																<td class="text-center" >
																	@if($item->midyear_achievement_revision == NULL)
																		{{ $item->midyear_achievement}}
																	@else
																	<span class="text-muted">
																		<strike>
																			{{ $item->midyear_achievement}}
																		</strike> 
																	</span>
																	<br>
																		{{ $item->midyear_achievement_revision }}
																	@endif	
																</td>
																<td class="text-center">{{$item->midyear_score}}</td>
																<td class="text-center" >
																	{{$item->yearend_achievement}}
																	<a id="editAchievement_{{$no}}" class="text-danger float-right"><i class="ti-pencil"></i></a>
																	<input type="text" name="achievement[]" id="achievement_{{$no}}" class="form-control" value="" style="display:none;">
																</td>
																<td class="text-center" >
																	<ul id='stars' class="list-inline p-0" data-value="{{$no}}">
																		<li class='star list-inline-item' data-value='1'>
																			<i class='fa fa-star icon-lg text-muted'></i>
																		</li>
																		<li class='star list-inline-item' data-value='2'>
																			<i class='fa fa-star icon-lg text-muted'></i>
																		</li>
																		<li class='star list-inline-item' data-value='3'>
																			<i class='fa fa-star icon-lg text-muted'></i>
																		</li>
																		<li class='star list-inline-item' data-value='4'>
																			<i class='fa fa-star icon-lg text-muted'></i>
																		</li>
																		<li class='star list-inline-item' data-value='5'>
																			<i class='fa fa-star icon-lg text-muted'></i>
																		</li>
																	</ul>
																	<br>
																	<span class="text-danger rate_{{ $no }}"></span>
																	<input type="hidden" name="score[]" id="rate_{{$no}}" class="rate">
																</td>
																<td class="text-center">
																	<input type="text" readonly name="final_score[]" class="form-control final_score" id="final_score_{{$no}}">
																</td>
															</tr>
															@php
																$no++
															@endphp
													@endforeach
												@endif
												<tr>
													<td colspan="9" class="text-right font-weight-bold">{{ __('pm.total_score') }}</td>
													<td  class="text-center"><span id="totalScore"></span></td>
												</tr>
											</tbody>
										</table>
									</div>

									<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
											
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">{{ __('pm.assessment_guide') }}</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>

											<div class="modal-body">
												<div class="table-responsive">
													<table class="table table-bordered">
														<thead>
															<th>{{ __('pm.score') }}</th>
															<th>{{ __('app.description') }}</th>
														</thead>
														<tbody>
															@foreach ($achievement_score as $val)
															<tr>
																<td>
																	@php
																		$star = 5 - $val->score;
																	@endphp	
																	<span class="text-uppercase font-weight-bold">{{ $val->title }}</span>
																	<ul class="list-inline p-0 m-0">
																		<?php for($i=0;$i<$val->score ;$i++){ ?>
																			<li class='star list-inline-item'>
																				<i class='fa fa-star icon-lg text-warning'></i>
																			</li>
																		<?php } ?>
																		<?php for($i=0;$i<$star ;$i++){ ?>
																			<li class='star list-inline-item'>
																				<i class='fa fa-star icon-lg text-muted'></i>
																			</li>
																		<?php } ?>
																	</ul>
																</td>
																<td>{{ $val->description }}</td>
															</tr>
														@endforeach
														<tbody>
													</table>
												</div>
											</div>

											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
											
											</div>
										</div>
									</div>
								
								</div>


								<!--STEP 2 -->
								<div class="step-tab-panel" id="step2">
									<button type="button" class="float-right btn btn-info btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal2">
										{{ __('pm.assessment_guide') }}
									</button>									
									<div class="table-responsive">
										<table class="table table-bordered table-striped mt-4">
											<thead>
												<tr>
													<th style="width:50px">No</th>
													<th >{{ __('pm.competency') }}</th>
													<th style="width:200px">{{ __('pm.score') }}</th>
													<th style="width:350px">{{ __('pm.comment') }}</th>
												</tr>
											</thead>
											<tbody>
												@php
													$no = 1;
												@endphp		
												@foreach ($competency as $item)
													<tr class="comp_item_{{ $no }}">
														<input type="hidden" name="competency[]" value="{{ $item->code }}">
														<input type="hidden" name="competency_id[]" value="{{ $item->id }}">
														<td>{{ $no }}</td>
														<td>
															<h6>{{ $item->name}}</h6> <span class="font-12">{{ $item->description }} </span>
														</td>
														<td>
															<ul id='stars_comp' class="list-inline p-0" data-value="{{$no}}">
																<li class='star list-inline-item' data-value='1'>
																	<i class='fa fa-star fa-2x text-muted'></i>
																</li>
																<li class='star list-inline-item' data-value='2'>
																	<i class='fa fa-star fa-2x text-muted'></i>
																</li>
																<li class='star list-inline-item' data-value='3'>
																	<i class='fa fa-star fa-2x text-muted'></i>
																</li>
																<li class='star list-inline-item' data-value='4'>
																	<i class='fa fa-star fa-2x text-muted'></i>
																</li>
																<li class='star list-inline-item' data-value='5'>
																	<i class='fa fa-star fa-2x text-muted'></i>
																</li>
															</ul>
															<br>
															<span class="text-danger rate_comp_{{ $no }}"></span>
															<input type="hidden" name="rate[]" id="rate_comp_{{$no}}" class="rate_comp">
														</td>
														<td>
															<textarea type="text" name="comment_{{ $no }}" class="form-control" required></textarea>
														</td>
													</tr>
													@php
														$no++
													@endphp
												@endforeach
											</tbody>
										</table>
									</div>

									<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">{{ __('pm.assessment_guide') }}</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
													<div class="table-responsive">
														<table class="table table-bordered">
															<tbody>
																<th>{{ __('pm.score') }}</th>
																<th>{{ __('app.description') }}</th>
															</tbody>
															@foreach ($score as $val)
																<tr>
																	<td class="text-center">{{ $val->score }}</td>
																	<td>
																		@php
																			$star = 5 - $val->score;
																		@endphp	
																		<span class="text-uppercase font-weight-bold">{{ $val->title }}</span>
																			<ul class="list-inline p-0 m-0">
																			<?php for($i=0;$i<$val->score ;$i++){ ?>
																				<li class='star list-inline-item'>
																					<i class='fa fa-star icon-lg text-warning'></i>
																				</li>
																			<?php } ?>
																			<?php for($i=0;$i<$star ;$i++){ ?>
																				<li class='star list-inline-item'>
																					<i class='fa fa-star icon-lg text-muted'></i>
																				</li>
																			<?php } ?>
																		</ul>
																	</td>
																	<td>{{ $val->description }}</td>
																</tr>
															@endforeach
														</table>
													</div>
											

											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class="step-footer text-right">
								<button data-direction="prev" class="btn btn-light"> {{ __('global.app_previous') }}</button>
								<button data-direction="next" class="btn btn-success"> {{ __('global.app_next') }}</button>
								<button data-direction="finish" class="btn btn-danger" ><i class="ti-save m-r-10"></i> {{ __('global.app_submit') }}</button>
							</div>
						</div>
					
					</form>
					
				</div>
			</div>
		</div>
	</div>

@endsection





@section('js')
	<script>

		function calculateSum(){
			var sum = 0;
			$(".final_score").each(function() {
				if (!isNaN(this.value) && this.value.length != 0) {
					sum += parseFloat(this.value);
				}
			});
			$("#totalScore").text(sum.toFixed(2));
		}
	
		$(document).ready(function(){

			$('#stars li').on('mouseover', function(){
				var onStar = parseInt($(this).data('value'), 10); 
			
				$(this).parent().children('li.star').each(function(e){
				if (e < onStar) {
					$(this).children().addClass('text-warning');
					$(this).addClass('hover');
				}
				else {
					$(this).children().removeClass('text-warning');
					$(this).removeClass('hover');
				}
				});
				
			}).on('mouseout', function(){
				$(this).parent().children('li.star').each(function(e){
					if($(this).hasClass('selected')){
						$(this).children().addClass('text-warning');
					}else{
						$(this).children().removeClass('text-warning');
					}
					$(this).removeClass('hover');

				});
			});
		
			$('#stars li').on('click', function(){
				var onStar 	= parseInt($(this).data('value'), 10); 
				var stars 	= $(this).parent().children('li.star');
				
				for (i = 0; i < stars.length; i++) {
					$(stars[i]).removeClass('selected');
					$(stars[i]).children().removeClass('text-warning');
				}
				
				for (i = 0; i < onStar; i++) {
					$(stars[i]).addClass('selected');
					$(stars[i]).children().addClass('text-warning');
				}
				var value = $(this).attr("data-value");
				var code  = $(this).parent().attr("data-value");

				$('#rate_'+code).val(value);
				$('.rate_'+code).text('');
				
				var weight = $('#weight_'+code).val();
				var final_score = value * weight /100
				$('#final_score_'+code).val(final_score);
				calculateSum();

			});

		

			@if (count($kpi_items) > 0)
				@php
					$no=1;
				@endphp
				@foreach ($kpi_items as $item)
					$('#editAchievement_{{ $no }}').on('click', function(){
						$('#achievement_{{ $no }}').show();
					});
					@php
						$no++;
					@endphp
				@endforeach
			@endif
			

			$('#stars_comp li').on('mouseover', function(){
				var onStar = parseInt($(this).data('value'), 10); 
			
				$(this).parent().children('li.star').each(function(e){
				if (e < onStar) {
					$(this).children().addClass('text-warning');
					$(this).addClass('hover');
				}
				else {
					$(this).children().removeClass('text-warning');
					$(this).removeClass('hover');
				}
				});
				
			}).on('mouseout', function(){
				$(this).parent().children('li.star').each(function(e){
					if($(this).hasClass('selected')){
						$(this).children().addClass('text-warning');
					}else{
						$(this).children().removeClass('text-warning');
					}
					$(this).removeClass('hover');

				});
			});
		

			$('#stars_comp li').on('click', function(){
				var onStar 	= parseInt($(this).data('value'), 10); 
				var stars 	= $(this).parent().children('li.star');
				
				for (i = 0; i < stars.length; i++) {
					$(stars[i]).removeClass('selected');
					$(stars[i]).children().removeClass('text-warning');
				}
				
				for (i = 0; i < onStar; i++) {
					$(stars[i]).addClass('selected');
					$(stars[i]).children().addClass('text-warning');
				}
				var value = $(this).attr("data-value");
				var code  = $(this).parent().attr("data-value");

				$('#rate_comp_'+code).val(value);
				$('.rate_comp_'+code).text('');

			});


			$("#form").steps({
                onChange: function(event, currentIndex, newIndex) {
                    form.validate().settings.ignore = ":disabled,:hidden";
					if ($(".rate").length){
						var arr_id = [];
						var validform  = '';

						$('.rate').each(function(){
							var value = $(this).val();
							var id    = $(this).attr('id');

							if (value != 0){
								validform = true;
							}else{
								arr_id.push(id);
								var current_arr = arr_id.pop();

								$('.'+current_arr).text('@lang("pm.validation_rate")');
								var element = current_arr.split(/_/);
								var focus = element[1];
								$('.kpi_item_'+focus).find('input').focus(); 
								validform = false;
								Swal.fire(
									'@lang("global.app_information")',
									'@lang("pm.info_validation_rate_kpi")',
									'warning'
								);
								return false;
							}
						});
					}else{
						validform = true;
					}
					if(validform){
						return form.valid();
					}else{
						return false;
					}
                },
                onFinish: function () { 

					if ($(".rate_comp").length){
						var arr_id = [];
						var validform_comp = '';

						$('.rate_comp').each(function(){
							var value = $(this).val();
							var id    = $(this).attr('id');

							if (value != 0){
								validform_comp = true;
							}else{
								arr_id.push(id);
								var current_arr = arr_id.pop();
								$('.'+current_arr).text('@lang("pm.validation_rate")');
								var element = current_arr.split(/_/);
								var focus = element[2];
								$('.comp_item_'+focus).find('input').focus(); 
								validform_comp = false;
								console.log(validform_comp);

								Swal.fire(
									'@lang("global.app_information")',
									'@lang("pm.info_validation_rate")',
									'warning'
								);
								return false;
							}
						});
					}else{
						validform_comp = true;
					}

					if(validform_comp){
						Swal.fire({
						title: '@lang("global.app_confirmation")', 
						text: '@lang("global.app_are_you_sure")', 
						type: 'question',
						showCancelButton: true,
						confirmButtonColor: 'null',
						cancelButtonColor: 'null',
						confirmButtonClass: 'btn btn-danger',
						cancelButtonClass: 'btn btn-primary',
						confirmButtonText: '@lang("global.app_yes")', 
						cancelButtonText: '@lang("global.app_cancel")', 
						}).then(res => {
							if (res.value) {
								return form.submit();
							}
						});
					}else{
						return false;
					}
                }
            });

			var form = $("#form");
			
			form.validate({
                errorPlacement: function errorPlacement(error, element) {
                    element.after(error);
                },
                rules: {
                    achievement: {
                        required: true,
                    },
                },
                onfocusout: function(element) {
                    $(element).valid();
                },
            });

		
		});
	</script>
@endsection		
