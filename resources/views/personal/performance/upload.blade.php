@extends('layouts.performance')

@section('page-header')
    @lang('pm.my_performance')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ route('myperformance.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('myperformance.personal.index') }}">@lang('pm.my_performance')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('global.upload_data')</li>
    </ol>
@endsection

@section('content')

   
        <div class="col-sm-12">
            <div class="card p-20 bd">
                
                <h6> @lang('global.upload_data') </h6>
                <hr class="mB-30">
                <div class="alert alert-info">
                    @lang('pm.form_upload_caption') <a download href="/docs/template-kpi.xlsx" class="btn btn-sm btn-success" title="@lang('global.app_download')" data-toggle='tooltip'><i class="fa fa-file-excel-o"></i> @lang('global.app_download') Template </a> <br>
                </div>
                <form action="{{ route('myperformance.upload.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group row m-t-30">
						<label class="col-lg-3 col-form-label">{{ __('pm.periode') }}</label>
						<div class="col-lg-4">
							<div class="input-group">
                                {!! Form::text('period_1', old('period_1'), ['class' => 'form-control monthpicker', 'placeholder' => '','id' =>'yearpicker','required' => 'required']) !!}
								<div class="input-group-prepend border-darken-1">
									<span class="input-group-text" >
										<small>s/d</small>
									</span>
								</div>
                                {!! Form::text('period_2', old('period_2'), ['class' => 'form-control monthpicker', 'placeholder' => '','id' =>'yearpicker','required' => 'required']) !!}
							</div>
						</div>
					</div>
                    <div class="form-group row mt-4">
                        <label class="col-sm-3 text-right">@lang('pm.year')</label>
                        <div class="col-sm-2">
                            {!! Form::text('year', old('year'), ['class' => 'form-control yearpicker', 'placeholder' => '','id' =>'yearpicker','required' => 'required']) !!}
                        </div>
                    </div>
                    
                     <div class="form-group row mt-4">
                        <label class="col-sm-3 text-right">@lang('global.file_caption')
                        </label>
                        <div class="col-sm-5">
                            <input type="file" name="file" class="form-control">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="ml-auto float-right">
	                        <a href="{{ route('performance.data') }}" class="btn btn-light">{{ trans('global.app_cancel') }}</a>
                            <button type="submit" class="btn bg-gradient-success"><i class="ti-save m-r-10"></i> {{ __('app.submit') }}</button>
                        </div>
                    </div>

                {!! Form::close() !!}       
            </div>  
        </div>
        
@endsection


@section('js')
	<script>
		$(document).ready(function(){

			
		});
	</script>
@endsection