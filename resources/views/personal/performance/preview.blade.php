@extends('layouts.performance')

@section('page-header')
    @lang('pm.my_performance')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ route('myperformance.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('myperformance.personal.index') }}">@lang('pm.my_performance')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('global.app_preview')</li>
    </ol>
@endsection

@section('content')

		<div class="col-lg-12">
			<div class="card">
				<div class="card-body p-30">
					
					<form action="{{ route('myperformance.preview.store') }}" method="POST">
							{{ csrf_field() }}	
							<input type="hidden" name="performance_id" value="{{ $pm->id }}">
								<div class="ml-auto float-right">
									<input type="hidden" value="0" name="status">
									<button type="submit" name="save" class="btn btn-default"><i class="m-r-10"></i> {{ __('global.app_cancel') }}</button>
									<button type="submit" class="btn bg-gradient-success" id="btn-submit"><i class="ti-save m-r-10"></i> {{ __('global.app_submit') }}</button>
								</div>
						</form>
					<h5>@lang('global.app_preview')</h5>
					<hr class="m-t-30">
					<h5>{{ $pm->nama}} </h5>
					<div class="row m-t-20">
						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-3">@lang('pm.emp_id') </div>
								<div class="col-sm-5">: {{  $pm->nik }}  </div>
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.position') }}  </div>
								<div class="col-sm-5">: {{ $pm->jabatan }} </div>
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.level') }}</div>
								<div class="col-sm-5">: {{  $pm->level }}</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-3">{{ __('pm.department') }}</div>
								<div class="col-sm-5">: {{  $pm->department }}</div>
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.workarea') }} </div>
								<div class="col-sm-5">: {{ $pm->workarea }}</div>
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.company') }} </div>
								<div class="col-sm-5">: {{ $pm->company }}</div> 
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.periode') }} </div>
								<div class="col-sm-5">:  {{ $pm->year }}</div> 
							</div>
						</div>
					</div>
		

					<div class="table-responsive  m-t-40">
						<h5>@lang('pm.kpi')</h5>
						<hr>
						<table class="table table-bordered table-striped m-t-20">
							<thead>
								<th>No</th>
								<th>{{ __('pm.performance_aspect') }}</th>
								<th>{{ __('pm.area_of_performance') }}</th>
								<th style="width:300px">{{ __('pm.kpi') }}</th>
								<th>{{ __('app.weight') }} (%)</th>
								<th>{{ __('pm.target') }}</th>
								<th>{{ __('pm.measure') }}</th>
							</thead>
							<tbody class="item_form">
								
								@if (count($kpi_items) > 0)
									@php
										$no = 1;
										$weight=0;
									@endphp
									@foreach ($kpi_items as $item)
											<tr>
												<td>{{$no}}</td>
												<td>{{$item->category}} </td>
												<td>{{$item->area}} </td>
												<td>{{$item->kpi}} </td>
												<td>{{$item->weight}}</td>
												<td>{{$item->target}}</td>
												<td>{{$item->measure}}</td>
											</tr>
											@php
												$weight +=$item->weight;
												$no++
											@endphp
									@endforeach
								@endif
								<tr>
									<td colspan="4" class="text-right font-weight-bold">{{ __('pm.total_weight') }}</td>
									<td>{{ $weight }}</td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>

						
						<form action="{{ route('myperformance.preview.store') }}" method="POST">
							{{ csrf_field() }}	
							<input type="hidden" name="performance_id" value="{{ $pm->id }}">
							<div class="mt-4">
								<div class="ml-auto float-right">
									<input type="hidden" value="0" name="status">
									<button type="submit" name="save" class="btn btn-default"><i class="m-r-10"></i> {{ __('global.app_cancel') }}</button>
									<button type="submit" class="btn bg-gradient-success" id="btn-submit"><i class="ti-save m-r-10"></i> {{ __('global.app_submit') }}</button>
								</div>
							</div>
						</form>
						</div>
					</div>

				</div>
			</div>
		</div>

@endsection

@section('js')
	<script  type='text/javascript'>

		$(document).ready(function(){
			
			$(document).on("click", "#btn-submit", function(e) {
				$('input[name="status"]').val('1');
			});
			
		});

	</script>
@endsection		
