
@extends('layouts.performance')

@section('page-header')
    @lang('pm.my_performance')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ route('myperformance.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('myperformance.personal.index') }}">@lang('pm.my_performance')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('global.app_detail') {{ __('pm.kpi') }}</li>
    </ol>
@endsection


@section('content')

		<div class="col-lg-12">
			<div class="card">
				<div class="card-body p-30">
					<div class="row mb-1 justify-content-end">
						<div class="col-sm-6">
								<h5 class="text-uppercase"> {{ __('pm.kpi') }} {{ $pm->year }} </h5>
						</div>
						<div class="col-sm-6">
							<a class="btn btn-outline float-right" href="{{ route('myperformance.print.kpi',['id' => Hashids::encode($pm->id) ,'type' => 'pdf']) }}" title="Download PDF"><i class="fa fa-file-pdf-o text-danger icon-lg"></i></a>
							<a class="btn btn-outline float-right" href="#" title="Print Data" onclick='printExternal("/myperformance/print_kpi/{{ Hashids::encode($pm->id) }}/print")'><i class="ti-printer icon-lg"></i></a>
						</div>
					</div>
					<hr>

						<div class="row m-t-20">
							<div class="col-sm-12">
								<table style="width: 100%;">
									<tr>
										<td style="width: 40%;">
											<table>
											<tr>
													<td> <span class="text-muted mr-2">{{ __('pm.emp_name') }}</span></td>
													<td> <span class="text-muted mr-2">:  {{  $pm->nama }}  </span></td>
												</tr>
												<tr>
													<td> <span class="text-muted mr-2">{{ __('pm.emp_id') }}</span></td>
													<td> <span class="text-muted mr-2">:  {{  $pm->nik }}  </span></td>
												</tr>
												<tr>
													<td> <span class="text-muted mr-2">{{ __('pm.position') }} </span></td>
													<td> <span class="text-muted mr-2">: {{ $pm->jabatan }} </span></td>
												</tr>
												
												<tr>
													<td> <span class="text-muted mr-2">{{ __('pm.level') }}</span></td>
													<td> <span class="text-muted mr-2">:  {{  $pm->level }}</span></td>
												</tr>
											</table>
										</td>

										<td style="width: 40%;">
											<table>
												<tr>
													<td> <span class="text-muted mr-2">{{ __('pm.department') }}</span></td>
													<td> <span class="text-muted mr-2">:  {{  $pm->department }}</span></td>
												</tr>
												<tr>
													<td> <span class="text-muted mr-2">{{ __('pm.workarea') }} </span></td>
													<td> <span class="text-muted mr-2">: {{ $pm->workarea }} </span></td>
												</tr>
												<tr>
													<td> <span class="text-muted mr-2">{{ __('pm.company') }} </span></td>
													<td> <span class="text-muted mr-2">: {{ $pm->company }} </span></td>
												</tr>
												<tr>
													<td> <span class="text-muted mr-2">{{ __('pm.periode') }} </span></td>
													<td> <span class="text-muted mr-2">: {{ $pm->year }}</span></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</div>
						</div>

						<div class="table-responsive">
							<table class="table table-bordered table-striped mt-5">
								<thead class="text-uppercase font-11">
									<th>No</th>
									<th>{{ __('pm.performance_aspect') }}</th>
									<th>{{ __('pm.area_of_performance') }}</th>
									<th>{{ __('pm.kpi') }}</th>
									<th class="text-center">{{ __('app.weight') }} (%)</th>
									<th class="text-center">{{ __('pm.target') }}</th>
									<th class="text-center">{{ __('pm.measure') }}</th>
								</thead>
								<tbody class="item_form">
									
									@if (count($pm_items) > 0)
										@php
											$no = 1;
										@endphp
										@foreach ($pm_items as $item)
												@php
													$strike = '';
												@endphp
												@if ($pm_items == NULL)
													$strike = 'line-through';
												@endif
												<tr style='text-decoration:{{ $strike }}'>
													<td>{{$no}}</td>
													<td>{{$item->aspect}}</td>
													<td>{{$item->area}}</td>
													<td>{{$item->kpi}}</td>
													<td class="text-center">{{ $item->weight}}</td>
													<td class="text-center">
														@if($item->target_revision == NULL)
															{{ $item->target}}
														@else
															<span class="text-muted">
																<strike>
																	{{ $item->target}}
																</strike> 
															</span>
															<br>
															{{ $item->target_revision }}
														@endif
													</td>
													<td class="text-center">
														@if($item->measure_id == NULL)
															{{$item->measure}}
														@else
															{{$item->measureTitle}}
														@endif	
													</td>
												</tr>
												@php
													$no++
												@endphp
										@endforeach
									@endif
								</tbody>
							</table>
						</div>
													
					</div>
				</div>
			</div>
		</div>

@endsection

@section('js')
	<script  type='text/javascript'>
		function printExternal(url) {
			var printWindow = window.open(url, 'Print', 'left=200, top=200, width=950, height=800, toolbar=0, resizable=0');
			printWindow.addEventListener('load', function() {
				printWindow.print();
			}, true);
		}
	</script>
@endsection		
