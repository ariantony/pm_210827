@extends('layouts.performance')

@section('page-header')
    @lang('pm.my_performance')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('myperformance.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('pm.my_performance')</li>
    </ol>
@endsection

@section('content')

        <div class="col-lg-12">
                        
            <div class="card">
                <div class="card-body p-30">
                
                    <div class="pad-btm mb-4">
                        <div class="table-toolbar-left">
                            <a class="btn bg-gradient-danger mr-2 btn-sm text-white"  href="{{ route('myperformance.personal.create') }}">{{ __('pm.input_performance') }}</a> 
                           <a class="btn bg-gradient-success mr-2 btn-sm text-white"  href="{{ route('myperformance.upload') }}">{{ __('global.upload_data') }}</a> 
                        </div>
                    </div>  
                    <hr>

                    <div class="table-responsive">
                        <table id="dataTables" class="table table-striped " cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>{{ trans('pm.periode') }}</th>
                                    <th>{{ trans('pm.year') }}</th>
                                    <th>{{ trans('app.status') }}</th>
                                    <th>{{ trans('app.date_input') }}</th>
                                    <th>{{ trans('app.action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
                
        </div>

@endsection

@section('js')
    <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('myperformance.personal.datatables') }}',
            columns: [
                {data: 'period', name: 'period'},
                {data: 'year', name: 'year'},
                {data: 'status', name: 'status', orderable: false, searchable: false},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@stop