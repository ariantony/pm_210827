


<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Harvest PM') }}</title>
	
	<style>
	  	body {
			font-size:12px;
		 	margin: 10px 20px; 
		}
		.page-break {
			page-break-after: always;
		}
		.table { 
			min-width: 100%; 
			border-collapse: collapse; 
		}
		/* Zebra striping */
		.table tr:nth-of-type(odd) { 
		}
		.table th { 
			background: #333; 
			color: white; 
			font-weight: bold; 
		}
		.table-bordered td, th { 
			padding: 6px; 
			border: 1px solid #ccc; 
		}
		.text-uppercase{
			text-transform: uppercase;
		}
		.text-center {
			text-align:center;
		}
		.text-right{
			text-align:right;
		}
		.text-bold {
			font-weight:bold;
		}
		.border-0{
			border: none;
		}
	</style>

</head>
<body class="bg-white">
		
	<div class="text-center mt-4">
		<img src="{{asset('images/logo-harvest-black.png')}}" alt="Logo" style="width:80px;position:absolute;left:10px;top:-20px" >
		<span class="text-bold" style="font-weight:bold; font-size: 16px;">PERFORMANCE MANAGEMENT SYSTEM <br> {{ __('pm.kpi') }} </span>
	</div>

	<table class="table border-0" style="margin-top:30px">
		<tr>
			<td class="border-0" style="width:33%"> 
				<table  class="table">
					<tr>
						<td class="border-0 p-0">{{ __('pm.emp_name') }}</td>
						<td class="border-0 p-0">: {{ $pm->nama }} </td>
					</tr>
					<tr>
						<td class="border-0 p-0">{{ __('pm.emp_id') }}</td>
						<td class="border-0 p-0">: {{ $pm->nik }} </td>
					</tr>
					<tr>
						<td class="border-0 p-0">{{ __('pm.position') }} </td>
						<td class="border-0 p-0">: {{ $pm->jabatan }} </td>
					</tr>
					<tr>
						<td class="border-0 p-0">{{ __('pm.level') }} </td>
						<td class="border-0 p-0">: {{ $pm->level }} </td>
					</tr>
					
				</table>
			</td>
			<td class="border-0" style="width:33%"> 
				<table>
					<tr>
						<td class="border-0 p-0">{{ __('pm.department') }} </td>
						<td class="border-0 p-0">: {{ $pm->department }}</td>
					</tr>
					<tr>
						<td class="border-0 p-0">{{ __('pm.workarea') }} </td>
						<td class="border-0 p-0">: {{ $pm->workarea }}</td>
					</tr>
					<tr>
						<td class="border-0 p-0">{{ __('pm.company') }} </td>
						<td class="border-0 p-0">: {{ $pm->company }} </td>
					</tr>
					<tr>
						<td class="border-0 p-0">{{ __('pm.periode') }} </td>
						<td class="border-0 p-0">: {{ $pm->year }} </td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<hr>
	<table class="table table-bordered" style="margin-top:20px">
		<thead class="text-uppercase font-9">
			<tr>
				<th>No</th>
				<th>{{ __('pm.performance_aspect') }}</th>
				<th style="width:300px">{{ __('pm.area_of_performance') }}</th>
				<th style="width:300px">{{ __('pm.kpi') }}</th>
				<th>{{ __('app.weight') }} (%)</th>
				<th>{{ __('pm.target') }}</th>
				<th>{{ __('pm.measure') }}</th>
			</tr>
		</thead>
		<tbody class="item_form">
			
			@if (count($pm_items) > 0)
				@php
					$no = 1;
					$weight=0;
				@endphp
				@foreach ($pm_items as $item)
						<tr>
							<td>{{$no}}</td>
							<td>{{$item->category}}</td>
							<td>{{$item->area}}</td>
							<td>{{$item->kpi}}</td>
							<td class="text-center">{{$item->weight}}</td>
							<td class="text-center">
								@if($item->target_revision == NULL)
									{{ $item->target}}
								@else
									<span class="text-muted">
										<strike>
											{{ $item->target}}
										</strike> 
									</span>
									<br>
									{{ $item->target_revision }}
								@endif
							</td>
							<td class="text-center">
								@if($item->measure_id == NULL)
									{{$item->measure}}
								@else
									{{$item->measureTitle}}
								@endif
							</td>
						</tr>
						@php
							$weight +=$item->weight;
							$no++
						@endphp
				@endforeach
			@endif
			<tr>
				<td colspan="4" class="text-right text-bold text-uppercase">{{ __('pm.total') }}</td>
				<td>{{ $weight }}</td>
				<td></td>
				<td></td>
			</tr>
		</tbody>
	
	</table>
</body>

</html>