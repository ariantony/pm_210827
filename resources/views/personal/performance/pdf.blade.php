
<html>
  	<head>
	   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    	<title>Performance Appraisal - Harvest Group</title>
  	</head>
	<style>
	  	body {
			font-size:12px;
		 	margin: 10px 20px; 
		}
		.page-break {
			page-break-after: always;
		}
		.table { 
			min-width: 100%; 
			border-collapse: collapse; 
		}
		/* Zebra striping */
		.table tr:nth-of-type(odd) { 
		}
		.table th { 
			background: #333; 
			color: white; 
			font-weight: bold; 
		}
		.table-bordered td, th { 
			padding: 6px; 
			border: 1px solid #ccc; 
		}
		.text-uppercase{
			text-transform: uppercase;
		}
		.text-center {
			text-align:center;
		}
		.text-right{
			text-align:right;
		}
		.text-bold {
			font-weight:bold;
		}
		.border-0{
			border: none;
		}
		.mb-0{
			margin-bottom:0px !important;
		}
	</style>
  	<body>	
		@php 
			$staff = explode(",",config('app.staff_id'));
			$kpiscore=0;
		@endphp


	  	<img class="img-fluid img-login text-center" src="./images/logo-harvest.png" style="height:40px;float:left">
	 	 <h3 class="text-center" style="font-weight:bold; font-size: 16px;">PERFORMANCE APPRASIAL<br> HARVEST GROUP </h3>

		<table style="width: 100%;">
			<tr>
				<td style="width: 40%;">
					<table>
						<tr>
							<td> <span class="text-muted mr-2">{{ __('pm.emp_name') }}</span></td>
							<td> <span class="text-muted mr-2">:  {{ $pm->nama  }} </span></td>
						</tr>
						<tr>
							<td> <span class="text-muted mr-2">{{ __('pm.emp_id') }}</span></td>
							<td> <span class="text-muted mr-2">:  {{  $pm->nik  }} </span></td>
						</tr>
						<tr>
							<td> <span class="text-muted mr-2">{{ __('pm.position') }} </span></td>
							<td> <span class="text-muted mr-2">:  {{ $pm->jabatan }}  </span></td>
						</tr>
						<tr>
							<td> <span class="text-muted mr-2">{{ __('pm.level') }} </span></td>
							<td> <span class="text-muted mr-2">:  {{ $pm->level }}  </span></td>
						</tr>
					
					</table>
				</td>

				<td style="width: 40%;">
					<table>
						<tr>
							<td> <span class="text-muted mr-2">{{ __('pm.department') }}</span></td>
							<td> <span class="text-muted mr-2">:  {{ $pm->department }}</span></td>
						</tr>
						<tr>
							<td> <span class="text-muted mr-2">{{ __('pm.workarea') }} </span></td>
							<td> <span class="text-muted mr-2">: {{ $pm->workarea }} </span></td>
						</tr>
						<tr>
							<td> <span class="text-muted mr-2">{{ __('pm.company') }} </span></td>
							<td> <span class="text-muted mr-2">: {{ $pm->company }} </span></td>
						</tr>
						<tr>
							<td> <span class="text-muted mr-2">{{ __('pm.periode') }} </span></td>
							<td> <span class="text-muted mr-2">: {{ $pm->year }}</span></td>
						</tr>
					</table>
				</td>
				<td style="width: 20%;"></td>
			</tr>
		</table>

		
		@if(!in_array($pm->level_id, $staff))			
			<h3 class="m-t-20">{{ __('pm.kpi') }} </h3>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center text-uppercase" style="width:30px">No</th>
						<th class="text-uppercase">{{ __('pm.performance_aspect') }}</th>
						<th class="text-uppercase" >{{ __('pm.area_of_performance') }}</th>
						<th class="text-uppercase">{{ __('pm.kpi') }}</th>
						<th class="text-center text-uppercase">{{ __('app.weight') }} (%)</th>
						<th class="text-center text-uppercase" style="width:100px">{{ __('pm.target') }}</th>
						<th class="text-center text-uppercase">{{ __('pm.measure') }}</th>
						<th class="text-center text-uppercase">{{ __('pm.midterm_result') }}</th>
						<th class="text-center text-center text-uppercase">{{ __('pm.midterm_kpi_score') }}</th>
						@if($type == 'yearend')									
							<th class="text-center text-uppercase">{{ __('pm.actual_annual_achievement') }}</th>
							<th class="text-center text-uppercase">{{ __('pm.kpi_score_achievement') }}</th>
							<th class="text-center text-uppercase">{{ __('pm.final_score') }} <br> <small>({{ __('app.weight') }} x {{ __('pm.kpi_score_achievement') }})</small></th>
						@endif	
					</tr>
				</thead>
				<tbody>
					
					@if (count($pm_items) > 0)
						@php
							$no = 1;
							$weight=0;
							$kpi_score=0;
						@endphp
						@foreach ($pm_items as $item)
								<?php 
									if($type == 'yearend'){								
										$itemscore = ($item->weight/100) * $item->yearend_score;
									}else{
										$itemscore = ($item->weight/100) * $item->midyear_score;
									}
									$kpi_score+= $itemscore;

								?>

								<tr>
									<td>{{$no}}</td>
									<td>{{$item->category}}</td>
									<td>{{$item->area}}</td>
									<td>{{$item->kpi}}</td>
									<td class="text-center">{{ $item->weight}}</td>
									<td class="text-center">{{ $item->target}}</td>
									<td class="text-center">
										@if($item->measure_id == NULL)
											{{$item->measure}}
										@else
											{{$item->measureTitle}}
										@endif	
									</td>
									<td class="text-center">
										@if($item->midyear_achievement_revision == NULL)
											{{ $item->midyear_achievement}}
										@else
										<span class="text-muted">
											<strike>
												{{ $item->midyear_achievement}}
											</strike> 
										</span>
										<br>
											{{ $item->midyear_achievement_revision }}
										@endif
									</td>
									<td class="text-center">{{ $item->midyear_score}}</td>
									@if($type == 'yearend')									
										<td class="text-center" >
											@if($item->yearend_achievement_revision == NULL)
												{{ $item->yearend_achievement}}
											@else
											<span class="text-muted">
												<strike>
													{{ $item->yearend_achievement}}
												</strike> 
											</span>
											<br>
												{{ $item->yearend_achievement_revision }}
											@endif
										</td>
										<td class="text-center">{{ $item->yearend_score}}</td>
										<td class="text-center">{{ $itemscore}}</td>
									@endif
								</tr>
								@php
									$no++
								@endphp
						@endforeach
					@endif
					<tr>
						@if($type == 'yearend')									
							<td colspan="11" class="text-right font-weight-bold text-uppercase">{{ __('pm.total_score') }}</td>
							<td class="text-center">{{ $kpi_score}}</td>
						@else
							<td colspan="8" class="text-right font-weight-bold text-uppercase">{{ __('pm.total_score') }}</td>
							<td class="text-center">{{ $kpi_score}}</td>	
						@endif
					</tr>
				</tbody>
			</table>
			<div class="page-break"></div>
		@endif
									
		<h3 class="text-uppercase">@lang('pm.competency')</h3>
		<hr>
	
		<table class="table table-bordered table-striped mt-4">
			<thead>
				<tr class="text-uppercase">
					<th rowspan="2" style="width:50px;border-bottom: 1px solid #e6ecf5;">No</th>
					<th rowspan="2" style="width:300px;border-bottom: 1px solid #e6ecf5;">{{ __('pm.competency') }}</th>
					<th rowspan="2" style="width:350px;border-bottom: 1px solid #e6ecf5;">{{ __('pm.comment') }}</th>
					
					@foreach ($group_raters as $value)
						@php
							$totalspan 	= 0;
						@endphp	
						<?php 
							$colom = [];
							foreach ($appraisal as $val){ 
								if(strtolower($value->name) == $val->group_rate ){
									if($val->group_rate == 'self'){
										$totalspan = 0;
									}else{
										$totalspan  +=1;
									}
									$colom []= $value->name;
								}
							}
							$kolom = array_unique($colom );
							foreach ($kolom as $val ){
								echo"<th class='text-center' style='border-bottom: 1px solid #e6ecf5;' colspan='".$totalspan."'>".$val."</th>";
							}
						?>
					@endforeach
				</tr>
				
				<tr style="">

					@foreach ($group_raters as $value)
						<?php 
							$i = 1;
							foreach ($appraisal as $val){
								if(strtolower($value->name) == $val->group_rate ){
									if($val->group_rate == 'self'){
										echo "<th class='text-center bd'  style='max-width:50px;border-bottom: 1px solid #e6ecf5;'><small>1</small></th>";
									}else{
										echo "<th class='text-center bd'  style='max-width:50px;border-bottom: 1px solid #e6ecf5;'><small>".$i."</small></th>";
										$i++;
									}
								}
							}
						?>
					@endforeach
				</tr>
			</thead>
			<tbody>
				@php
					$no 			= 1;
					$totalscore 	= 0;
					$nilaiGroup = [];

				@endphp	

					
				@foreach ($assessment as $key => $value)
					<tr>
						<td>{{ $no }}</td>
						<td>
							<strong>{{ $value->competencyName }}</strong><br> <span class="font-12 text-muted">{{ $value->competencyDesc }} </span>
						</td>
						<td>
							@if($type == 'midyear')
								<?php 
									if($value->comment_midyear != NULL){
										$comment = json_decode($value->comment_midyear,true);
										foreach ($comment as $val){
											if($val['value'] != NULL){
												echo "<p class='mb-0'>- ".$val['value']."</p>";
											}else{
												echo "<p class='mb-0'>- </p>";
											}
										}
									}else{
										$comment = explode('-',$value->comment_all);
										foreach($comment as $val){
											if($val) {
												echo "- ".$val." <br>";
											}
										}
									}
								?>
							@else
								<?php 
									$comment = json_decode($value->comment_yearend,true);
									foreach ($comment as $val){
										if($val['value'] != NULL){
											echo "<p class='mb-0'>- ".$val['value']."</p>";
										}else{
											echo "<p class='mb-0'>- </p>";
										}
									}
								?>
							@endif
						</td>
						
							<?php 
								if($type == 'midyear'){
									$rate = json_decode($value->rate,true);
								}else{
									$rate = json_decode($value->rate_yearend,true);
								}
								$i=1;
								$inc =0;
							?>

							@foreach ($group_raters as $value)
								<?php 
									$i = 1;
									$nilai = [];
									foreach ($rate as $val) {
										$nilai[$val['nik']] = $val['value'];
									}

									foreach ($appraisal as $val){

										if(strtolower($value->name) == $val->group_rate ){
											if(array_key_exists($val->rater_id,$nilai)){
												echo "<td class='text-center'>".$nilai[$val->rater_id]. "</td>";
												$nilaiGroup[$val->group_rate.'_'.$inc][] = $nilai[$val->rater_id];
											}else{
												echo "<td class='text-center'>-</td>";
												$nilaiGroup[$val->group_rate.'_'.$inc][] = 0;
											}
											$inc +=1;
										}
									}
								?>
							@endforeach

					</tr>
					@php
						$group_rate_sum = $nilai;
						$no++
					@endphp

				@endforeach

			</tbody>
			
			<tfoot>
				<tr>
					<td colspan="3"></td>
					<?php
						$key = array_keys($nilaiGroup);
						
						for($i = 0;$i<count($key);$i++){
							$groupRater = explode("_",$key[$i]);
							$nilaiGroupSum = 0;
							foreach($nilaiGroup[$key[$i]] as $item){
								$nilaiGroupSum += $item;
							}
							echo "<td class='text-center'>".$nilaiGroupSum ."</td>";
						}
					?>
				</tr>
				<tr>
					<td colspan="3"></td>
					<?php

						$key = array_keys($nilaiGroup);
						$NilaiSubordinates = $NilaiPeers= $count_peers = $count_subordinates = $nilaiGroupSumSubordinates = $nilaiGroupSumPeers = $colspan_subordinates = $colspan_peers = 0;

						for($i = 0;$i<count($key);$i++){
							$groupRater = explode("_",$key[$i]);
							$nilaiGroupSum = 0;
							foreach($nilaiGroup[$key[$i]] as $item){
								$nilaiGroupSum += $item;
							}
							
							$bobotRater = getBobot($groupRater[0],$pm->rater_weight);
							$nilaiGroupRaterSum = $bobotRater /100 * $nilaiGroupSum;
							
							if($groupRater[0] == 'peers'){
								$colspan_peers ++;
								if($nilaiGroupSum != 0){
									$count_peers ++;
									$nilaiGroupSumPeers += $nilaiGroupSum ;
								}
							}

							if($groupRater[0] == 'subordinates'){
								$colspan_subordinates ++;
								if($nilaiGroupSum != 0){
									$count_subordinates ++;
									$nilaiGroupSumSubordinates += $nilaiGroupSum ;
								}
								
							}
							if($groupRater[0] == 'self'){
								$NilaiSelf  = $nilaiGroupRaterSum;
								echo "<td class='text-center'>".$nilaiGroupRaterSum ."</td>";
							}
							if($groupRater[0] == 'superior' ){
								$NilaiSuperior  = $nilaiGroupRaterSum;
								echo "<td class='text-center'>".$nilaiGroupRaterSum ."</td>";
							}
						}

						if($nilaiGroupSumPeers != 0){
							$NilaiPeers 		=  round((getBobot('peers',$pm->rater_weight)/100) * $nilaiGroupSumPeers/$count_peers,2);
						}
						if($nilaiGroupSumSubordinates !=0){
							$NilaiSubordinates  =  round((getBobot('subordinates',$pm->rater_weight)/100) * $nilaiGroupSumSubordinates/$count_subordinates,2);
						}
						if(	$colspan_peers > 0){
							echo "<td class='text-center' colspan='".$colspan_peers."'>".$NilaiPeers."</td>";
						}	
						if(	$colspan_peers > 0){
							echo "<td class='text-center' colspan='".$colspan_subordinates."'>".$NilaiSubordinates."</td>";
						}	
					?>
				</tr>
			
				<tr>
					<td colspan="3" class="text-right font-weight-bold">{{ __('pm.total_score') }}</td>
					<td colspan="{{ $inc }}" class="text-center">
						<?php 
							$totalNilaiPA = ($NilaiSelf  + $NilaiPeers + $NilaiSubordinates + $NilaiSuperior)/6;
							echo round($totalNilaiPA,2);
						?>
					</td>
				</tr>

			</tfoot>

		</table>
		
		<h3 class="text-uppercase">@lang('pm.performance_appraisal')</h3>
		<table class="table table-bordered table-striped mt-4">
			@if(!in_array($pm->level_id, $staff))
				<tr>	
					<td style="width:50px">1</td>
					<td> @lang('pm.kpi')</td>
					<td> <?php echo $kpi_score  ?> x  {{  $pm->kpi_weight }} %</td>
					<td class="text-right"> 
						<?php 
							$kpiScoreFinal =  $kpi_score * ($pm->kpi_weight/100);
							echo round($kpiScoreFinal,2);
						?> 
					</td>
				</tr>
				<tr>	
					<td>2</td>
					<td class="text-uppercase"> @lang('pm.competency')</td>
					<td> <?php echo round($totalNilaiPA,2); ?> x  {{  $pm->competency_weight }} %</td>
					<td class="text-right"> 
						<?php 
							$competenctyScoreFinal =  $totalNilaiPA  * ($pm->competency_weight/100);
							echo round($competenctyScoreFinal,2);
						?> 
					</td>
				</tr>
				<tr>	
					<td colspan="3" class="text-right font-weight-bold text-uppercase"> 
						Total @lang('pm.performance_appraisal')
					</td> 
					<td class="text-right font-weight-bold text-uppercase"> 
						<?php 
							$ScoreFinal =  $kpiScoreFinal + $competenctyScoreFinal;
							echo round($ScoreFinal,2);
						?> 
					</td>
				</tr>
			@else
				<tr>	
					<td style="width:50px">1</td>
					<td> @lang('pm.competency')</td>
					<td class="text-right text-bold"> 
						<?php 
							$competenctyScoreFinal =  $totalNilaiPA;
							echo round($competenctyScoreFinal,2);
						?> 
					</td>
				</tr>	
				<tr>	
					<td colspan="2" class="text-right font-weight-bold text-uppercase"> 
						Total @lang('pm.performance_appraisal')
					</td> 
					<td class="text-right font-weight-bold text-uppercase"> 
						<?php 
							$ScoreFinal =  $competenctyScoreFinal;
							echo round($ScoreFinal,2);
						?> 
					</td>
				</tr>
						
			@endif

		</table>

		<h3 class="text-uppercase">@lang('pm.adj_score')</h3>
		<table class="table table-bordered table-striped mt-4">
			<thead>
				<tr>
					<th style="width:50px">@lang('pm.adjusted') </th>
					<th>@lang('pm.date_input') </th>
					<th>@lang('pm.score') </th>
				</tr>
			</thead>
			<tbody>
				@if($pm->score_adjustment_1 != NULL || $pm->score_adjustment_2 != NULL)
					@if($pm->score_adjustment_1 != NULL)
						<tr>
							<td class="text-center"> 1 </td>
							<td> {{ date('d M Y',strtotime($pm->score_adjustment_1_date))  }}</td>
							<td> {{ $pm->score_adjustment_1 }}</td>
						</tr>
					@endif
					@if($pm->score_adjustment_2 != NULL)
						<tr>
							<td class="text-center"> 2 </td>
							<td> {{ date('d M Y',strtotime($pm->score_adjustment_2_date))  }}</td>
							<td> {{ $pm->score_adjustment_2 }}</td>
						</tr>
					@endif
				@else
					<tr>
						<td class="text-center" colspan="3">@lang('pm.data_not_found')</td>
					</tr>
				@endif
			</tbody>
		</table>

	</body>
</html>