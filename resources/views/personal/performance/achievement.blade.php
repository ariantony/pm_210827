
@extends('layouts.performance')

@section('page-header')
	@lang('pm.my_performance')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="{{ route('myperformance.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('myperformance.personal.index') }}">@lang('pm.my_performance')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('pm.achievement')</li>
    </ol>
@endsection


@section('content')

	<div class="col-lg-12">
		<div class="card">
			<div class="card-body p-30">
				@if($type =='midyear')
					<h5>{{ __('pm.midterm_result') }}</h5>
				@else
					<h5>{{ __('pm.actual_annual_achievement') }}</h5>
				@endif
				<hr>

				<div class="row m-t-20">
					<div class="col-sm-6">
						<div class="row">
							<div class="col-sm-3">{{ __('pm.periode') }} </div>
							<div class="col-sm-5">:  {{ $pm->year }}</div> 
						</div>
					</div>
					<div class="col-sm-6">
						
					</div>
				</div>


				<form action="{{ route('myperformance.achievement.store') }}" method="POST" id="form">
					{{ csrf_field() }}	
					<input type="hidden" name="type" class="form-control" value="{{$type}}" >
					<div class="table-responsive  m-t-40">
						<table class="table table-bordered table-striped mt-2">
							<thead class="text-uppercase font-11">
								<th>No</th>
								<th>{{ __('pm.kpi') }}</th>
								<th class="text-center">{{ __('app.weight') }} (%)</th>
								<th>{{ __('pm.target') }}</th>
								<th>{{ __('pm.measure') }}</th>
								@if($type =='yearend')
									<th class="bg-danger">{{ __('pm.midterm_result') }}</th>
									<th style="min-width:300px">{{ __('pm.actual_annual_achievement') }}</th>
								@else
									<th style="min-width:200px">{{ __('pm.midterm_result') }}</th>
								@endif
							</thead>
							<tbody class="item_form">
								
								@if (count($kpi_items) > 0)
									@php
										$no = 1;
										$weight=0;
									@endphp
									@foreach ($kpi_items as $item)
											<tr class="item_{{ $no }}">
												<input type="hidden" name="weight[]" class="form-control" value="{{$item->weight}}" id="weight_{{$no}}">
												<input type="hidden" name="pm_item[]" class="form-control" value="{{ $item->id }}">
												<td>{{$no}}</td>
												<td><span class="badge badge-info"> {{$item->category}} </span> <br><span class="text-uppercase font-11 font-weight-bold">{{$item->area}}</span>
													<p class="mt-1">{{$item->kpi}}</p>
												</td>
												<td class="text-center">{{$item->weight}}</td>
												<td>{{$item->target}}</td>
												<td>
													@if($item->measure_id == NULL)
														{{$item->measure}}
													@else
														{{$item->measureTitle}}
													@endif
												</td>
												@if($type =='yearend')
													<td class="alert alert-danger text-center">
														@if($item->midyear_achievement_revision == NULL)
															{{ $item->midyear_achievement}}
														@else
														<span class="text-muted">
															<strike>
																{{ $item->midyear_achievement}}
															</strike> 
														</span>
														<br>
															{{ $item->midyear_achievement_revision }}
														@endif	
													</td>
													<td>
														<input type="text" name="achievement[]" class="form-control achievement" id="achievement_{{ $no }}" value="{{  $item->yearend_achievement != NULL ? $item->yearend_achievement : '' }}" required>
														<br>
														<span class="text-danger achievement_{{ $no }}"></span>
													</td>
												@else
													<td>
														<input type="text" name="achievement[]" class="form-control achievement" id="achievement_{{ $no }}" value="{{  $item->midyear_achievement != NULL ? $item->midyear_achievement : '' }}" required>
														<br>
														<span class="text-danger achievement_{{ $no }}"></span>
													</td>
												@endif
											</tr>
											@php
												$weight +=$item->weight;
												$no++
											@endphp
									@endforeach
								@endif
							</tbody>
						</table>


					</div>
					<div class="mt-4">
						<div class="ml-auto float-right">
							<input type="hidden" value="{{ $pm->id }}" name="pm_id">
							<input type="hidden" value="0" name="status">
							<button type="submit" class="btn bg-gradient-blue" id="btnDraft"><i class="ti-save m-r-10"></i> {{ __('global.app_save_draft') }}</button>
							<button type="submit" class="btn bg-gradient-success" id="btnSubmitAchievement"><i class="ti-save m-r-10"></i> {{ __('global.app_submit') }}</button>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>

@endsection



@section('js')
	<script>
	
		$(document).ready(function(){

			$("#form").validate({
			submitHandler: function(form) {
				form.submit();
			},
			ignore: [],
			rules: {
				'achievement[]': {
					required: true
				}
			}
			});

			$('.achievement').on('keyup', function(){
				var id    = $(this).attr('id');
				$('.'+id).text('');
			});

			$(document).on('click', "#btnDraft", function(e) {
				var _this = $(this);
				var form = _this.parents('form');

				form.validate({
					submitHandler: function(form) {
						form.submit();
					},
					ignore: [],
					rules: {
						'achievement[]': {
							required: true
						}
					},
					onfocusout: false,
					invalidHandler: function(form, validator) {
						var errors = validator.numberOfInvalids();
						if (errors) {
							validator.errorList[0].element.focus();
						}
					}
				});

				e.preventDefault();
				if (form.valid()) {
					Swal.fire({
						title: '@lang("global.app_confirmation")', 
						text: '@lang("global.app_are_you_sure")', 
						type: 'question',
						showCancelButton: true,
						confirmButtonColor: 'null',
						cancelButtonColor: 'null',
						confirmButtonClass: 'btn btn-danger',
						cancelButtonClass: 'btn btn-primary',
						confirmButtonText: '@lang("global.app_yes")', 
						cancelButtonText: '@lang("global.app_cancel")', 
					}).then(res => {
						if (res.value) {
							_this.closest("form").submit();
						}
					});
				}
			});


			$(document).on('click', "#btnSubmitAchievement", function(e) {
				$('input[name="status"]').val('1');
				var _this = $(this);
				var form = _this.parents('form');

				if ($(".achievement").length){
					var arr_id = [];
					var validform  = '';

					$('.achievement').each(function(){
						var value = $(this).val();
						var id    = $(this).attr('id');

						if (value != ''){
							validform = true;
						}else{
							arr_id.push(id);
							var current_arr = arr_id.pop();
							$('.'+current_arr).text('@lang("validation.required")');
							var element = current_arr.split(/_/);
							var focus = element[1];
							$('.item_'+focus).find('input').focus(); 
							validform = false;
							Swal.fire(
								'@lang("global.app_information")',
								'@lang("pm.info_validation_achievement")',
								'warning'
							);
							return false;
						}
					});
				}else{
					validform = true;
				}


				e.preventDefault();
				if (form.valid() && validform) {
					Swal.fire({
						title: '@lang("global.app_confirmation")', 
						text: '@lang("global.app_are_you_sure")', 
						type: 'question',
						showCancelButton: true,
						confirmButtonColor: 'null',
						cancelButtonColor: 'null',
						confirmButtonClass: 'btn btn-danger',
						cancelButtonClass: 'btn btn-primary',
						confirmButtonText: '@lang("global.app_yes")', 
						cancelButtonText: '@lang("global.app_cancel")', 
					}).then(res => {
						if (res.value) {
							_this.closest("form").submit();
						}
					});
				}
			});
	
			
		});
	</script>
@endsection		
