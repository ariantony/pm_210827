@extends('layouts.performance')

@section('page-header')
    @lang('pm.my_performance')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ route('myperformance.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('myperformance.personal.index') }}">@lang('pm.my_performance')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('global.app_edit')</li>
    </ol>
@endsection

@section('content')

	<div class="col-lg-12">
	
		<div class="card">
			<div class="card-body p-30">
			
				<h5 class="header-title text-uppercase">
					{{ __('app.form') }} {{ __('pm.performance_appraisal') }}
				</h5>
				<hr>

				{!! Form::model($pm, [
                    'route' => ['myperformance.personal.update', $pm->id],
					'method' => 'put', 
					'files' => true
					])
				!!}
				<div class="alert alert-info">
					{{ __('pm.total_weight_information') }}
				</div>

				<div class="form-group row">
						<label class="col-lg-3 col-form-label">{{ __('pm.periode') }}</label>
						<div class="col-lg-6">
							<div class="input-group">
								<input type="text" name="period_1" class="form-control monthpicker" value="{{$pm->period_start}}" required>
								<div class="input-group-prepend border-darken-1">
									<span class="input-group-text" >
										<small>s/d</small>
									</span>
								</div>
								<input type="text" name="period_2" class="form-control monthpicker" value="{{$pm->period_end}}" required>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 col-form-label">{{ __('pm.year') }}</label>
						<div class="col-lg-2">
							<input type="text" name="year" class="form-control yearpicker" id="yearpicker" value="{{$pm->year}}" required>
						</div>
					</div>

					<a class="btn btn-success btn-sm text-white add_item float-right mb-2 "><i class="ti-plus"></i>{{ __('global.app_add_new') }}</a>
					<table class="table table-bordered table-striped mt-2">
						<thead>
							<th>{{ __('pm.performance_aspect') }}</th>
							<th style="width:250px">{{ __('pm.area_of_performance') }}</th>
							<th style="width:350px">{{ __('pm.kpi') }}</th>
							<th>{{ __('app.weight') }} (%)</th>
							<th style="width:250px">{{ __('pm.target') }} / {{ __('pm.measure') }}</th>
							<th>{{ __('app.action') }}</th>
						</thead>
						<tbody class="item_form">
							
						@if (count($pm_items) > 0)
							@php
								$no = 1
							@endphp
							@foreach ($pm_items as $item)
									<tr>
											<td>
												{!! Form::select('pm_aspect[]', $category, $item->category, ['class' => 'form-control select2 mB-5', 'id'=>'category_'.$no]) !!}
											</td>
											<td>
												<input type="text" name="area[]" class="form-control" value="{{$item->area}}" id="area_{{$no}}" required>
											</td>
											<td>
												<input type="text" name="kpi[]" class="form-control" value="{{$item->kpi}}" id="kpi_{{$no}}" required>
											</td>
											<td>
												<input type="number" name="weight[]" class="form-control weight" value="{{$item->weight}}"  min="0" step="0.1" oninput="this.value = Math.abs(this.value)">
											</td>
											<td>
												<input type="text" name="target[]" class="form-control" value="{{$item->target}}" id="target_{{$no}}" required> 
												{!! Form::select('measure[]', $measure, $item->measure_id, ['class' => 'form-control select2', 'required' => '']) !!}
											</td>
											<td>
												<a href="#" class="remove_item btn btn-danger btn-md pull-right"><i class="ti-trash"></i></a>
											</td>
									</tr>
									@php
										$no++
									@endphp
							@endforeach
                    	@endif

						</tbody>
					</table>

					<hr>
					<div class="form-group mt-4">
						<div class="ml-auto float-right">
							<a class="btn btn-default" href="{{ route('myperformance.personal.index') }}">{{ __('global.app_cancel') }}</a>
							<input type="hidden" value="0" name="status">
							<button type="submit" class="btn bg-gradient-blue" id="btn-draft"><i class="ti-save m-r-10"></i> {{ __('global.app_save_draft') }}</button>
							<button type="submit" class="btn bg-gradient-success" id="btn-submit"><i class="ti-save m-r-10"></i> {{ __('global.app_submit') }}</button>
						</div>
					</div>

				</form>
				
			</div>
		</div>
			
	</div>

@endsection

@section('js')
	<script>
		$(document).ready(function(){
			
			var wrapper = $(".item_form");
			var add_sk = $(".add_item");
			var i = 0;
			var m = 0;
			$(document).on("click", ".add_item", function(e) {
				e.preventDefault();
				i++;

				$.getJSON("/myperformance/get_objective_category/" , function(data){
					$.each(data, function(value, key) {
						$('#pm_aspect_'+i).append($("<option></option>").attr("value", value).text(key)); // name refers to the objects value when you do you ->lists('name', 'id') in laravel
					});
				});

				$.getJSON("/myperformance/get_measure/" , function(data){
					$.each(data, function(value, key) {
						$('#measure_'+i).append($("<option></option>").attr("value", value).text(key)); // name refers to the objects value when you do you ->lists('name', 'id') in laravel
					});
				});

				var id = "#row_" + i;
				$(wrapper).append(
					'<tr>' +
						'<td>' +
							'<select name="pm_aspect[]" class="form-control" id="pm_aspect_'+i+'" required></select>' +
						'</td>' +
						'<td>' +
							'<input type="text" name="area[]" class="form-control" required>' +
						'</td>' +
						'<td>' +
							'<input type="text" name="kpi[]" class="form-control" required>' +
						'</td>' +
						'<td>' +
							'<input type="number" name="weight[]" class="form-control weight"  id="weight_'+i+'" oninput="this.value = Math.abs(this.value)"  min="0" step="1" required>' +
						'</td>'+
						'<td>' +
							'<input type="text" name="target[]" class="form-control" required>' +
							'<select name="measure[]" class="form-control" id="measure_'+i+'" required></select>' +
						'</td>' +
						'<td>' +
							'<a href="#" class="remove_item btn btn-danger btn-md pull-right"><i class="ti-trash"></i></a>' +
						'</td>' +
					'</tr>'
				);

				$(".weight").on("keyup", function() {
					calculateSum($(this).attr("id"));
				});
				m++;
			});

			$(document).on("click", ".remove_item", function() {
				$(this).parents("tr").remove();
			});

			$(document).on("click", "#btn-submit", function(e) {
				$('input[name="status"]').val('1');
			});

			$(document).on("click", "#btn-draft", function(e) {
				$('input[name="status"]').val('0');
			});
			
			
		
		});

		function calculateSum(id){
			var sum = 0;
			var sumt = 0;
			$(".weight").each(function() {
				//add only if the value is number
				if (!isNaN(this.value) && this.value.length != 0) {
					sum += parseFloat(this.value);
					$(this).css("background-color", "#FEFFB0");
					if(sum > 100){
						Swal.fire(
							'{{ __("app.notice") }}!',
							'{{__("pm.message_failed_weight") }}',
							'warning'
						);
						$("#"+id).val('');
					}else{
						sumt += parseFloat(this.value);
					}
				}
				
			});

			$("#totalWeight").text(sumt.toFixed(2));
		}

	</script>
@endsection

