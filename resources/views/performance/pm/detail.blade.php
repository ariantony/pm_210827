
@extends('layouts.app')

@section('page-header')
    @lang('pm.performance')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('performance.data') }}">@lang('pm.performance')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('global.app_detail')</li>
    </ol>
@endsection


@section('content')

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body p-30">
					<div class="row mb-1 justify-content-end">
						<div class="col-sm-6">
							<a class="btn btn-outline float-right" href="{{ route('performance.print.kpi',['id' => Hashids::encode($pm->id) ,'type' => 'pdf']) }}" title="Download PDF"><i class="fa fa-file-pdf-o text-danger icon-lg"></i></a>
							<a class="btn btn-outline float-right" href="#" title="Print Data" onclick='printExternal("/performance/print_kpi/{{ Hashids::encode($pm->id) }}/print")'><i class="ti-printer icon-lg"></i></a>
						</div>
					</div>
					<hr>
										
					<h5>{{ $pm->nama }}</h5>
					<div class="row m-t-20">
						<div class="col-sm-12">
							<table style="width: 100%;">
								<tr>
									<td style="width: 40%;">
										<table>
											<tr>
												<td> <span class="text-muted mr-2">{{ __('pm.emp_id') }}</span></td>
												<td> <span class="text-muted mr-2">:  {{  $pm->nik }}  </span></td>
											</tr>
											<tr>
												<td> <span class="text-muted mr-2">{{ __('pm.position') }} </span></td>
												<td> <span class="text-muted mr-2">: {{ $pm->jabatan }} </span></td>
											</tr>
											<tr>
												<td> <span class="text-muted mr-2">{{ __('pm.level') }}</span></td>
												<td> <span class="text-muted mr-2">:  {{  $pm->level }}</span></td>
											</tr>
											<tr>
												<td> <span class="text-muted mr-2">{{ __('pm.department') }}</span></td>
												<td> <span class="text-muted mr-2">:  {{  $pm->department }}</span></td>
											</tr>
										</table>
									</td>

									<td style="width: 40%;">
										<table>
											<tr>
												<td> <span class="text-muted mr-2">{{ __('pm.workarea') }} </span></td>
												<td> <span class="text-muted mr-2">: {{ $pm->workarea }} </span></td>
											</tr>
											<tr>
												<td> <span class="text-muted mr-2">{{ __('pm.company') }} </span></td>
												<td> <span class="text-muted mr-2">: {{ $pm->company }} </span></td>
											</tr>
											<tr>
												<td> <span class="text-muted mr-2">{{ __('pm.periode') }} </span></td>
												<td> <span class="text-muted mr-2">: {{ $pm->year }}</span></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</div>
					
					<div class="table-responsive">
						<table class="table table-bordered table-striped mt-5">
							<thead class="text-uppercase font-11">
								<th>No</th>
								<th>{{ __('pm.kpi') }}</th>
								<th class="text-center">{{ __('app.weight') }} (%)</th>
								<th class="text-center">{{ __('pm.target') }}</th>
								<th class="text-center">{{ __('pm.measure') }}</th>
								<th class="text-center">{{ __('pm.midterm_result') }}</th>
								<th class="text-center">{{ __('pm.midterm_kpi_score') }}</th>
								<th class="text-center">{{ __('pm.actual_annual_achievement') }}</th>
								<th class="text-center">{{ __('pm.kpi_score_achievement') }}</th>
								<th class="text-center">{{ __('pm.final_score') }} <br> <small>({{ __('app.weight') }} x {{ __('pm.kpi_score_achievement') }})</small></th>
							</thead>
							<tbody class="item_form">
								@php
									$kpi_score=0;
								@endphp
								@if (count($pm_items) > 0)
									@php
										$no = 1;
									@endphp
									@foreach ($pm_items as $item)
											<?php 
													$itemscore = ($item->weight/100) * $item->yearend_score;
													$kpi_score += $itemscore;
											?>

											<tr>
												<td>{{$no}}</td>
												<td>
													<span class="badge badge-info"> {{$item->category}} </span> <br><span class="text-uppercase font-11 font-weight-bold">{{$item->area}}</span>
													<p class="mt-1">{{ $item->kpi }}</p>
												</td>
												<td class="text-center">{{ $item->weight }}</td>
												<td>
													@if($item->target_revision == NULL)
														{{ $item->target}}
													@else
														<span class="text-muted">
															<strike>
																{{ $item->target}}
															</strike> 
														</span>
														<br>
														{{ $item->target_revision }}
													@endif
												</td>
												<td>
													@if($item->measure_id == NULL)
														{{$item->measure}}
													@else
														{{$item->measureTitle}}
													@endif	
												</td>
												<td  class="text-center">
													@if($item->midyear_achievement_revision == NULL)
														{{ $item->midyear_achievement }}
													@else
													<span class="text-muted">
														<strike>
															{{ $item->midyear_achievement}}
														</strike> 
													</span>
													<br>
													{{ $item->midyear_achievement_revision }}
													@endif
												</td>
												<td class="text-center">{{ $item->midyear_score}}</td>
												<td class="text-center" >
													@if($item->yearend_achievement_revision == NULL)
														{{ $item->yearend_achievement}}
													@else
													<span class="text-muted">
														<strike>
															{{ $item->yearend_achievement}}
														</strike> 
													</span>
													<br>
														{{ $item->yearend_achievement_revision }}
													@endif
												</td>
												<td class="text-center">{{ $item->yearend_score}}</td>
												<td class="text-center">{{ $itemscore}}</td>
											</tr>
											@php
												$no++
											@endphp
									@endforeach
								@endif
								<tr>
										<td colspan="9" class="text-right font-weight-bold">{{ __('pm.total_score') }}</td>
										<td class="text-center">{{ $kpi_score }}</td>
								</tr>
							</tbody>
						</table>
					</div>
												
				</div>
			</div>
		</div>
	</div>

@endsection

@section('js')
	<script>
		function printExternal(url) {
			var printWindow = window.open(url, 'Print', 'left=200, top=200, width=950, height=800, toolbar=0, resizable=0');
			printWindow.addEventListener('load', function() {
				printWindow.print();
			}, true);
		}
		$(document).ready(function(){
			
		});
	</script>
@endsection		
