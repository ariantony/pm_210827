
@extends('layouts.app')

@section('page-header')
    @lang('pm.performance')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('performance.data') }}">@lang('pm.performance')</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ __('pm.adjustment') }}</li>
    </ol>
@endsection


@section('content')

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body p-30">
				<h5>{{ $pm->nama}} </h5>	
					<hr>

					
					<div class="row m-t-20">
						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-3">@lang('pm.emp_id') </div>
								<div class="col-sm-5">: {{  $pm->nik }}  </div>
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.position') }}  </div>
								<div class="col-sm-5">: {{ $pm->jabatan }} </div>
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.department') }}</div>
								<div class="col-sm-5">: {{  $pm->department }}</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-3">{{ __('pm.workarea') }} </div>
								<div class="col-sm-5">: {{ $pm->workarea }}</div>
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.company') }} </div>
								<div class="col-sm-5">: {{ $pm->company }}</div> 
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.periode') }} </div>
								<div class="col-sm-5">:  {{ $pm->year }}</div> 
							</div>
						</div>
					</div>
					<hr>
					{!! Form::open(['method' => 'POST', 'route' => ['performance.adjust.store']]) !!}

						<input type="hidden" name="pm_id" class="form-control" value="{{ $pm->id }}">
						<input type="hidden" name="pm_year" class="form-control" value="{{ $pm->year }}">

						

						<div class="form-group row m-t-30">
							<label class="col-lg-2">{{ __('pm.adjustment') }} 1</label>
							<div class="col-lg-2">
								<small class="text-muted">Date</small>
								<input type="text" name="score_adjustment_1_date" class="form-control datepicker" required value="{{ date('d/m/Y',strtotime($pm->score_adjustment_1_date)) }}">
							</div>
							<div class="col-lg-2">
								<small class="text-muted">	{{ __('pm.score') }}</small>
								<input type="number" min="0" max="5" step="0.01" name="score_adjustment_1" class="form-control" required value="{{ $pm->score_adjustment_1}}">
							</div>
						</div>

						<div class="form-group row m-t-30">
							<label class="col-lg-2">{{ __('pm.adjustment') }} 2</label>
							<div class="col-lg-2">
								<small class="text-muted">Date</small>
								<input type="text" name="score_adjustment_2_date" class="form-control datepicker" required value="{{ date('d/m/Y',strtotime($pm->score_adjustment_2_date)) }}">
							</div>
							<div class="col-lg-2">
								<small class="text-muted">	{{ __('pm.score') }}</small>
								<input type="number" min="0" max="5" step="0.01" name="score_adjustment_2" class="form-control" required value="{{ $pm->score_adjustment_2}}">
							</div>
						</div>

						<hr>
						<div class="form-group mt-4">
							<div class="ml-auto float-right">
								<a class="btn btn-default" href="{{ route('performance.data') }}">{{ __('global.app_cancel') }}</a>
								<button type="submit" class="btn bg-gradient-success" id="btn-submit"><i class="ti-save m-r-10"></i> {{ __('global.app_submit') }}</button>
							</div>
						</div>
					</form>
					
				</div>
			</div>
		</div>
	</div>

@endsection

@section('js')
	<script>
		function printExternal(url) {
			var printWindow = window.open(url, 'Print', 'left=200, top=200, width=950, height=800, toolbar=0, resizable=0');
			printWindow.addEventListener('load', function() {
				printWindow.print();
				printWindow.close();
			}, true);
		}
		$(document).ready(function(){
			
		});
	</script>
@endsection		
