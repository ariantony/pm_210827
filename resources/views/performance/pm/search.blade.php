@extends('layouts.app')

@section('page-header')
    @lang('pm.performance')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('performance.data') }}">@lang('pm.performance')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('global.app_search')</li>
    </ol>
@endsection

@section('content')

    <div class="row">
    <div class="col-lg-12">
                    
        <div class="card">
            <div class="card-body p-30">
            
                <div class="pad-btm">
                    <div class="table-toolbar-right">
                        <div class="btn-group">
                            <a class="btn btn-default"  data-toggle="collapse" data-target="#export"><i class="fa fa-file-excel-o text-danger"></i> @lang('global.app_export')</a>
                            <a class="btn btn-default"  data-toggle="collapse" data-target="#filter"><i class="ti-filter"></i> @lang('global.app_filter')</a>
                            <a href="#" class="btn btn-danger float-right" id="btnDelete" style="display:none;"> <i class="ti-trash icon-lg"></i>  @lang('global.app_delete')</a>
                        </div>
                    </div>

                    <div class="collapse" id="filter">
                        <hr>
                            <form class="form-inline" action="{{ route('performance.search')}}" method='GET'>
                                {{ csrf_field() }}
                                <div class="form-group mb-2 col-sm-3">
                                    <label>@lang('pm.periode')</label>
                                    <input type="text" name="periode" value="{{ date('Y') }}" class="form-control yearpicker" required style="width:100%">
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.company') }}</label>
                                    {!! Form::select('company_id', $company, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.department') }}</label>
                                    {!! Form::select('department_id', $department, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.workarea') }}</label>
                                    {!! Form::select('workarea_id', $workarea, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.level') }}</label>
                                    {!! Form::select('level_id', $level, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group m-t-20 col-sm-12  ">
                                    <button type="submit" class="btn bg-gradient-blue float-right">@lang('global.app_submit')</button>
                                </div>
                            </form>
                        <hr>
                    </div>

                    <div class="collapse" id="export">
                            <hr>
                            <form class="form-inline" action="{{ route('performance.export')}}" method='GET'>
                                {{ csrf_field() }}
                                <div class="form-group mb-2 col-sm-3">
                                    <label>@lang('pm.periode')</label>
                                    <input type="text" name="periode" value="{{ date('Y') }}" class="form-control" id="yearpicker-export" required style="width:100%">
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.company') }}</label>
                                    {!! Form::select('company_id', $company, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.workarea') }}</label>
                                    {!! Form::select('workarea_id', $workarea, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.department') }}</label>
                                    {!! Form::select('department_id', $department, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.level') }}</label>
                                    {!! Form::select('level_id', $level, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group m-t-20 col-sm-12">
                                    <button type="submit" class="btn bg-gradient-blue float-right">@lang('global.app_export')</button>
                                </div>
                            </form>
                            <hr>
                        </div>

                </div>  

                <p>{!! $search !!}</p>
                <div class="table-responsive">
                    <div id="showSelected" style="display:none"></div>
                    <table id="dataTables" class="table table-striped " cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th style="width:50px" class="text-center">
                                    <input class="magic-checkbox" name="checkedAll" id="checkedAll" type="checkbox"> <label for="checkedAll"></label>
                                </th>
                                <th style="width:120px">{{ trans('pm.emp_id') }}</th>
                                <th>{{ trans('pm.emp_name') }}</th>
                                <th>{{ trans('pm.periode') }}</th>
                                <th>{{ trans('app.status') }}</th>
                                <th>{{ trans('app.action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($pm) > 0)
                                @foreach ($pm as $item)
                                    <tr>
                                        <td class="text-center">
                                            <input type='checkbox' name='pm_id[]' class='pm_id checkSingle magic-checkbox'  value='{{ $item->id }}' id="checkbox_{{ $item->id }}"><label for="checkbox_{{ $item->id }}"></label>
                                        </td>
                                        <td>{{  $item->emp_nik }}</td>
                                        <td>
                                            <span class='font-weight-bold text-uppercase'>{{ $item->emp_name }}</span><br>
                                            <small>{{ $item->emp_company }} | {{ $item->emp_workarea }} | {{ $item->emp_department }} | {{ $item->emp_level }}</small>
                                        </td>
                                        <td>{{  $item->period_start }} - {{  $item->period_end }}</td>
                                        <td>{!! getStatusPM($item->status) !!}</td>
                                        <td>
                                            <?php

                                                    $url_show    = "<a href='".route('performance.detail', Hashids::encode($item->id))."' class='dropdown-item'><span class='ti-eye icon-lg'></span> ".trans('global.app_show')." ".trans('pm.kpi')." </a>";  
                                                                
                                                    $achievement = $url_show;

                                                    if($item->is_score == 1 ){
                                                        $url_midyear  = "<a href='".route('performance.show', ['type' => 'midyear' , 'id' => Hashids::encode($item->id)])."'  class='dropdown-item'> <i class='ti-pencil'></i> ".trans('global.app_result')." MID YEAR</a>";  
                                                        $achievement .= $url_midyear;
                                                    }
                                                    if($item->is_score == 2){
                                                        $url_yearend  = "<a href='".route('performance.show', ['type' => 'yearend' , 'id' => Hashids::encode($item->id)])."'  class='dropdown-item'> <i class='ti-pencil'></i> ".trans('global.app_result')." YEAR END</a>";  
                                                        $achievement .= $url_yearend;
                                                    }
                                                    if($item->status == 3){
                                                        $url_score  = "<a href='".route('performance.adjust', ['id' => Hashids::encode($item->id)])."' class='dropdown-item'><span class='ti-pencil icon-lg'></span> </a>";  
                                                        $achievement .= $url_score;
                                                    }

                                                    echo 
                                                    '<div class="dropdown">
                                                        <a class="btn btn-md btn-default dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.trans("app.action").'</a>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">'
                                                            .$achievement.
                                                        '</div>
                                                     </div>';
                                            ?>
                                
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan="7" class="text-center"> @lang('global.app_no_entries_in_table')</td></tr>
                            @endif
                        </tbody>
                    </table>
                    {{ $pm->links() }}
                </div>

            </div>
        </div>
            
    </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {

            $('.select2').select2({
                placeholder: "Select an attribute",
                allowClear: true,
            });
            
            $(document).on("click", "#btnDelete", function(e) {
                var arr_id = [];
                $('.pm_id:checkbox:checked').each(function() {
                    var value = $(this).val();
                    arr_id.push(value);
                });
                var id = arr_id;

                if(arr_id.length === 0){
                    Swal.fire(
                        'Informasi',
                        'Minimal Checklist 1 Item',
                        'warning'
                    );
                    return false;
                }else{
                
                    var id = arr_id;

                    Swal.fire({
                        title: 'Confirmation', // Opération Dangereuse
                        text: 'Are you sure?', // Êtes-vous sûr de continuer ?
                        type: 'question',
                        showCancelButton: true,
                        confirmButtonColor: 'null',
                        cancelButtonColor: 'null',
                        confirmButtonClass: 'btn btn-danger',
                        cancelButtonClass: 'btn btn-primary',
                        confirmButtonText: 'Yes, Delete it', // Oui, sûr
                        cancelButtonText: 'Cancel', // Annuler
                    }).then(res => {
                        if (res.value) {
                            $.ajax({
                                type:'POST',
                                url:'/performance/destroy',
                                data:{
                                    'id':id,
                                    '_token': $('meta[name="csrf-token"]').attr('content')
                                },
                                success:function(data){
                                    $('#validation-message').append('<div class="alert dark alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button> Delete was successfull</div>');
                                    setTimeout(function () { document.location.reload(true); }, 300);
                                },
                                error: function(xhr)
                                {
                                    $('#validation-message').html('');
                                    $.each(xhr.responseJSON.errors, function(key,value) {
                                        $('#validation-message').append('<div class="alert dark alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>'+value+'</b></div>');
                                    });
                                }
                            });
                        }
                    });
                }
            });

            $("#checkedAll").change(function(){
                var rows_selected = [];
                if(this.checked){
                    $(".checkSingle").each(function(){
                        var value = $(this).val();
                        this.checked=true;
                        rows_selected.push(value);
                    });
                    $('#btnDelete').show();
                }else{
                    $(".checkSingle").each(function(){
                        this.checked=false;
                    });
                    $('#btnDelete').hide();
                }
            });

            $(".checkSingle").click(function () {
                var rows = [];
                if ($(this).is(":checked")){
                    var isAllChecked = 0;
                    $(".checkSingle").each(function(){
                        if(!this.checked){
                            isAllChecked = 1;
                        }else{
                            var value = $(this).val();
                            rows.push(value);
                        }
                    })              
                    if(isAllChecked == 0){
                        $("#checkedAll").prop("checked", true); 
                    } 
                    $('#btnDelete').show();
                }else {
                    $("#checkedAll").prop("checked", false);
                }
            });

            $('#yearpicker-export').datepicker({
                todayHighlight: 'TRUE',
                autoclose: true,
                format: " yyyy", // Notice the Extra space at the beginning
                viewMode: "years",
                minViewMode: "years",
                orientation: 'bottom'
            });

        });
    </script>
@stop