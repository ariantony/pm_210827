
@extends('layouts.app')

@section('page-header')
	@lang('global.app_preview')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('performance.data') }}">@lang('pm.performance')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('global.app_preview')</li>
    </ol>
@endsection


@section('content')
	<div class="row">

		<div class="col-lg-12">
			<div class="card">
				<div class="card-body p-30">
					
					<form action="{{ route('performance.preview.store') }}" method="POST">
							{{ csrf_field() }}	
							<input type="hidden" name="performance_id" value="{{ $pm->id }}">
								<div class="ml-auto float-right">
									<input type="hidden" value="0" name="status">
									<button type="submit" name="save" class="btn btn-default"><i class="m-r-10"></i> {{ __('global.app_cancel') }}</button>
									<button type="submit" class="btn bg-gradient-success" id="btn-submit"><i class="ti-save m-r-10"></i> {{ __('global.app_submit') }}</button>
								</div>
						</form>
					<h5>@lang('global.app_preview')</h5>
					<hr class="m-t-30">
					<h5>{{ $pm->nama}} </h5>
					<div class="row m-t-20">
						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-3">@lang('pm.emp_id') </div>
								<div class="col-sm-5">: {{  $pm->nik }}  </div>
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.position') }}  </div>
								<div class="col-sm-5">: {{ $pm->jabatan }} </div>
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.level') }}</div>
								<div class="col-sm-5">: {{  $pm->level }}</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-3">{{ __('pm.department') }}</div>
								<div class="col-sm-5">: {{  $pm->department }}</div>
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.workarea') }} </div>
								<div class="col-sm-5">: {{ $pm->workarea }}</div>
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.company') }} </div>
								<div class="col-sm-5">: {{ $pm->company }}</div> 
							</div>
							<div class="row">
								<div class="col-sm-3">{{ __('pm.periode') }} </div>
								<div class="col-sm-5">:  {{ $pm->year }}</div> 
							</div>
						</div>
					</div>
		

					<div class="table-responsive  m-t-40">
						<h5>I. @lang('pm.kpi')</h5>
						<hr>
						<table class="table table-bordered table-striped m-t-20">
							<thead>
								<th>No</th>
								<th style="width:300px">{{ __('pm.kpi') }}</th>
								<th>{{ __('app.weight') }} (%)</th>
								<th>{{ __('pm.target') }}</th>
								<th>{{ __('pm.measure') }}</th>
								<th>{{ __('pm.kpi_score_achievement') }}</th>
								<th>{{ __('pm.final_score') }} <br> <small>({{ __('app.weight') }} x {{ __('pm.kpi_score_achievement') }})</small></th>
							</thead>
							<tbody class="item_form">
								
								@if (count($kpi_items) > 0)
									@php
										$no = 1;
										$weight=0;
										$sum_final_score=0;
									@endphp
									@foreach ($kpi_items as $item)
											<?php 
												if ($pm->is_score== 1 || $pm->is_score == 2){
													$score = $item->score;
													$final_score = $item->final_score;
													$sum_final_score += $final_score;
												}
												else{
													$score = '-';
													$final_score = '-';
													$sum_final_score = '-';
												}
											?>

											<tr>
												<td>{{$no}}</td>
												<td><span class="badge badge-info"> {{$item->category}} </span> <br><span class="text-uppercase font-11 font-weight-bold">{{$item->area}}</span>
													<p class="mt-1">{{$item->kpi}}</p>
												</td>
												<td>{{$item->weight}}</td>
												<td>{{$item->target}}</td>
												<td>{{$item->measure}}</td>
												<td>{{ $score}}</td>
												<td>{{ $final_score}}</td>
											</tr>
											@php
												$weight +=$item->weight;
												$no++
											@endphp
									@endforeach
								@endif
								<tr>
									<td colspan="2" class="text-right font-weight-bold">{{ __('pm.total_weight') }}</td>
									<td>{{ $weight }}</td>
									<td></td>
									<td></td>
									<td></td>
									<td>{{ $sum_final_score }}</td>
								</tr>
							</tbody>
						</table>
						@if ($model == 'pa')

						<h5 class="m-t-40">II. @lang('pm.performance_appraisal')</h5>
						<hr>
						<table class="table table-bordered table-striped mt-4">
							<thead>
								<tr class="text-uppercase">
									<th rowspan="2" style="width:50px;border-bottom: 1px solid #e6ecf5;">No</th>
									<th rowspan="2" style="width:300px;border-bottom: 1px solid #e6ecf5;">{{ __('pm.competency') }}</th>
									<th rowspan="2" style="width:400px;border-bottom: 1px solid #e6ecf5;">{{ __('pm.comment') }}</th>
								
									@foreach ($group_raters as $value)
										@php
											$totalspan 	= 0;
										@endphp	
										<?php 
											foreach ($appraisal as $val){
												if(strtolower($value->name) == $val->group_rate ){
													if($val->group_rate == 'self'){
														$totalspan = 0;
													}else{
														$totalspan  +=1;
													}
												}
											}
										?>
										<th class='text-center' style="border-bottom: 1px solid #e6ecf5;" colspan="{{ $totalspan }}"> {{ $value->name }}</th>
									@endforeach
								</tr>
								<tr style="">
									@foreach ($group_raters as $value)
										<?php 
											$i = 1;
											foreach ($appraisal as $val){
												if(strtolower($value->name) == $val->group_rate ){
													if($val->group_rate == 'self'){
														echo "<th class='text-center bd'  style='max-width:50px;border-bottom: 1px solid #e6ecf5;'><small>1</small></th>";
													}else{
														echo "<th class='text-center bd'  style='max-width:50px;border-bottom: 1px solid #e6ecf5;'><small>".$i."</small></th>";
														$i++;
													}
												}
											}
										?>
									@endforeach
								</tr>
							</thead>
							<tbody>
								@php
									$no = 1;
									$totalscore = 0;

								@endphp	

									
								@foreach ($assessment as $key => $value)
									<tr>
										<td>{{ $no }}</td>
										<td>
											<h6>{{ $value->competencyName }}</h6> <span class="font-12 text-muted">{{ $value->competencyDesc }} </span>
										</td>
										<td>
											<?php 
												$comment = explode('-',$value->comment_all);
											?>
											@foreach($comment as $val)
												@if($val)
													- {{ $val }} <br>
												@endif
											@endforeach
										
										</td>
										
										<?php 
											$item_sum = 0;
											$items = 1;
											$rate = json_decode($value->rate,true);
											$i=1;
											$inc =0;
											?>

											@foreach ($group_raters as $value)
											
											<?php 
													$i = 1;
													foreach ($rate as $val){

														$group = explode('_',$val['key']);
														if(strtolower($value->name) == $group['0'] ){
															if(!is_null($val['nik'])){
																if (!is_null($val['value'])) {
																	echo "<td class='text-center bd'  style='max-width:50px;border-bottom: 1px solid #e6ecf5;'><small>".$val['value']."</small></th>";
																	$items += 1;
																	$item_sum += $val['value'];
																}else{
																	echo "<td class='text-center bd'  style='max-width:50px;border-bottom: 1px solid #e6ecf5;'><small>-</small></th>";
																}
																$inc +=1;
																$i++;
															}
														}
													}

											?>
										@endforeach


									</tr>
									@php
										$totalscore += ($item_sum/$items); 
										$no++
									@endphp
								@endforeach
							</tbody>
							<?php 
								$colspan = 3+$inc;
							?>
							<tfoot>
								<td colspan="{{ $colspan }}" class="text-right font-weight-bold">{{ __('pm.total_score') }}</td>
									<td>{{ round($totalscore/6,2) }}</td>
							</tfoot>
						</table>
						
						@endif

						
						<form action="{{ route('performance.preview.store') }}" method="POST">
							{{ csrf_field() }}	
							<input type="hidden" name="performance_id" value="{{ $pm->id }}">
							<div class="mt-4">
								<div class="ml-auto float-right">
									<input type="hidden" value="0" name="status">
									<button type="submit" name="save" class="btn btn-default"><i class="m-r-10"></i> {{ __('global.app_cancel') }}</button>
									<button type="submit" class="btn bg-gradient-success" id="btn-submit"><i class="ti-save m-r-10"></i> {{ __('global.app_submit') }}</button>
								</div>
							</div>
						</form>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

@endsection

@section('js')
	<script  type='text/javascript'>

		$(document).ready(function(){
			
			$(document).on("click", "#btn-submit", function(e) {
				$('input[name="status"]').val('1');
			});
			
		});

	</script>
@endsection		
