@extends('layouts.app')

@section('page-header')
    @lang('app.monitoring') KPI
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('app.monitoring') KPI</li>
    </ol>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
                        
            <div class="card">
                <div class="card-body p-30">
                
                    <div class="pad-btm">
                        <div class="table-toolbar-left">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text text-uppercase font-weight-bold" style="padding:0.3rem 1rem !important;">@lang('pm.periode')</span>
                                </div>
                                <input type="text" name="periode" value="{{ $year }}" class="form-control"  id="yearpicker" required>
                            </div>
                        </div>
                        <div class="table-toolbar-right">
                            <div class="btn-group">
                                <a class="btn btn-default"  data-toggle="collapse" data-target="#filter"><i class="ti-filter"></i> @lang('global.app_filter')</a>
                            </div>
                        </div>
                        <hr class="m-0">

                        <div class="collapse" id="filter">
                            <hr>
                            <form class="form-inline" action="{{ route('performance.monitoring.kpi.search')}}" method='GET'>
                                {{ csrf_field() }}
                                <input type="hidden" name="periode" value="{{ $year }}"  class="form-control" id="searchperiod" required style="width:100%">
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.company') }}</label>
                                    {!! Form::select('company_id', $company, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.department') }}</label>
                                    {!! Form::select('department_id', $department, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.workarea') }}</label>
                                    {!! Form::select('workarea_id', $workarea, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group m-t-20 col-sm-12">
                                    <button type="submit" class="btn bg-gradient-blue float-right">@lang('global.app_search')</button>
                                </div>
                            </form>
                            <hr>
                        </div>

                    </div>  
                    <p>{!! $search !!}</p>
                    <div class="table-responsive">
                        <table id="dataTables" class="table table-striped" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-uppercase">{{ trans('pm.emp_id') }}</th>
                                    <th class="text-uppercase">{{ trans('pm.emp_name') }}</th>
                                    <th class="text-uppercase">{{ trans('pm.company') }}</th>
                                    <th class="text-uppercase">{{ trans('pm.workarea') }}</th>
                                    <th class="text-uppercase">{{ trans('pm.department') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
                
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {

            $('.select2').select2({
                placeholder: "Select an attribute",
                allowClear: true,
            });

            $('#dataTables').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('performance.monitoring.kpi.datatables', [$year,$company_id,$workarea_id,$department_id]) }}',
                columns: [
                    {data: 'emp_nik', name: 'emp_nik'},
                    {data: 'emp_data', name: 'emp_name'},
                    {data: 'company', name: 'company'},
                    {data: 'workarea', name: 'workarea'},
                    {data: 'department', name: 'department'},
                ]
            });
           
            $('#yearpicker').datepicker({
                autoclose: true,
                container:'#page-content',
                format: " yyyy", // Notice the Extra space at the beginning
                viewMode: "years",
                minViewMode: "years",
                orientation: 'bottom'
            }).on('changeDate', function(e) {
                var date = encodeURIComponent(this.value.trim());
                var url = "/performance/monitoring_kpi/"+date;
                window.open(url, '_self');
            });


            $('.employee').select2({
                placeholder: 'Cari Employee dengan mengetik 3 huruf...',
				allowClear: true,
				minimumInputLength: 3,
                ajax: {
                    url: '/get_employee/',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.name + " [" + item.department + "/" + item.workarea + "]",
                                    id: item.nik
                                }
                            })
                        };
                    },
                    cache: true
                }
            });


        });
    </script>
@stop