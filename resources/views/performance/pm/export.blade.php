
<table>
    <thead>
        <tr>
            <th style="font-weight:bold;text-transform:uppercase">{{ trans('pm.emp_id') }}</th>
            <th style="font-weight:bold;text-transform:uppercase;width:35px">{{ trans('pm.emp_name') }}</th>
            <th style="font-weight:bold;text-transform:uppercase;width:30px">{{ trans('pm.position') }}</th>
            <th style="font-weight:bold;text-transform:uppercase;width:25px">{{ trans('pm.level') }}</th>
            <th style="font-weight:bold;text-transform:uppercase;width:25px">{{ trans('pm.department') }}</th>
            <th style="font-weight:bold;text-transform:uppercase;width:25px">{{ trans('pm.workarea') }}</th>
            <th style="font-weight:bold;text-transform:uppercase;">{{ trans('pm.company') }}</th>
            <th style="font-weight:bold;text-transform:uppercase;">{{ trans('pm.year') }}</th>
            <th style="font-weight:bold;text-transform:uppercase;">{{ trans('pm.competency') }}</th>
            <th style="font-weight:bold;text-transform:uppercase;">{{ trans('pm.kpi') }}</th>
            <th style="font-weight:bold;text-transform:uppercase;">{{ trans('pm.total_score') }}</th>
            <th style="font-weight:bold;text-transform:uppercase;">Date {{ trans('pm.adjusted') }} 1</th>
            <th style="font-weight:bold;text-transform:uppercase;">{{ trans('pm.adjusted') }} 1</th>
            <th style="font-weight:bold;text-transform:uppercase;">Date {{ trans('pm.adjusted') }} 2</th>
            <th style="font-weight:bold;text-transform:uppercase;">{{ trans('pm.adjusted') }} 2</th>
        </tr>
    </thead>
    <tbody>
        @if(count($pm) > 0)
            @foreach ($pm as $item)
                <tr>
                    <td>{{ $item['emp_nik'] }} </td>
                    <td>{{ $item['emp_name'] }}</td>
                    <td>{{ $item['emp_occupation'] }} </td>
                    <td>{{ $item['emp_level'] }} </td>
                    <td>{{ $item['emp_department'] }}</td>
                    <td>{{ $item['emp_workarea'] }}</td>
                    <td>{{ $item['emp_company'] }}</td>
                    <td>{{ $item['year'] }}</td>
                    <td>{{ $item['competency'] }}</td>
                    <td>{{ $item['kpi'] }}</td>
                    <td>{{ $item['score_pa'] }}</td>
                    <td>{{ $item['score_adjustment_1_date'] }}</td>
                    <td>{{ $item['score_adjustment_1'] }}</td>
                    <td>{{ $item['score_adjustment_2_date'] }}</td>
                    <td>{{ $item['score_adjustment_2'] }}</td>
                </tr>
            @endforeach
        @else
            <tr><td colspan="13" style="text-align:center"> @lang('global.app_no_entries_in_table')</td></tr>
        @endif
    </tbody>
</table>
                 