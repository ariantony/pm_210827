@extends('layouts.app')

@section('page-header')
    @lang('app.monitoring') @lang('pm.assessment')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('app.monitoring')  @lang('pm.assessment')</li>
    </ol>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
                        
            <div class="card">
                <div class="card-body p-30">
                
                    <div class="pad-btm">
                        <div class="table-toolbar-left">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text text-uppercase font-weight-bold" style="padding:0.3rem 1rem !important;">@lang('pm.periode')</span>
                                </div>
                                <input type="text" name="periode" value="{{ $year }}" class="form-control"  id="yearpicker" required>
                            </div>
                        </div>
                        <div class="table-toolbar-right">
                            <div class="btn-group">
                                <a class="btn btn-default"  data-toggle="collapse" data-target="#filter"><i class="ti-filter"></i> @lang('global.app_filter')</a>
                            </div>
                        </div>
                        <hr class="m-0">

                        <div class="collapse" id="filter">
                            <hr>
                            <form class="form-inline" action="{{ route('performance.monitoring.search')}}" method='GET'>
                                <input type="hidden" name="periode" value="{{ $year }}"  class="form-control" id="searchperiod" required style="width:100%">
                                {{ csrf_field() }}
                                <div class="form-group mb-2 col-sm-3">
                                    <label>@lang('pm.rater')</label>
                                    <select class="form-control employee" name="rater_id"></select>
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.company') }}</label>
                                    {!! Form::select('company_id', $company, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.department') }}</label>
                                    {!! Form::select('department_id', $department, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.workarea') }}</label>
                                    {!! Form::select('workarea_id', $workarea, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group m-t-20 col-sm-12">
                                    <button type="submit" class="btn bg-gradient-blue float-right">@lang('global.app_search')</button>
                                </div>
                            </form>
                            <hr>
                        </div>

                    </div>  
                    <p>{!! $search !!}</p>
                    <div class="table-responsive">
                        <table id="dataTables" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width:50px;">NO</th>
                                    <th class="text-uppercase">{{ trans('pm.rater') }}</th>
                                    <th class="text-uppercase">{{ trans('pm.emp_name') }}</th>
                                    <th class="text-uppercase">{{ trans('pm.rater_as') }}</th>
                                    <th class="text-uppercase">{{ trans('pm.periode') }}</th>
                                    <th class="text-uppercase">{{ trans('app.status') }}</th>
                                </tr>
                            </thead>
                            <tbody>

                            @php 
                                $no = 1
                            @endphp
                            @foreach($monitoring as $val)
                                <?php 
                                    $row      = count($val);
                                    $rowspan  = false;
                                    if($row > 1){
                                        $rowspan  = true;
                                    }
                                    $first = 1;
                                ?>
                                @foreach($val as $item)
                                    <tr>
                                    @if($first == 1 && $rowspan)
                                        <td rowspan="{{ $row }}" style="vertical-align:top;text-align:center">{{ $no }}</td>
                                        <td rowspan="{{ $row }}" style="vertical-align:top;">{{ $item->rater_id }} <br> 
                                            <span class='font-weight-bold text-uppercase'>{{ $item->rater_name }} </span> <br>
                                            <span class=''>{{ $item->company }} | {{ $item->emp_workarea }} | {{ $item->emp_department }} |  {{ $item->emp_level }} </span> 
                                        </td>
                                    @else
                                        @if($rowspan == false )
                                            <td style="vertical-align:top;text-align:center">{{ $no }}</td>
                                            <td style="vertical-align:top;">{{ $item->rater_id }} <br><span class='font-weight-bold text-uppercase'>{{ $item->rater_name }} </span></td>
                                        @endif
                                    @endif
                                    <td>{{ $item->emp_name }}</td>
                                    <td>{{ $item->group_rate }}</td>
                                    <td>{{ $item->year }}</td>
                                    <td>
                                        @if($item->date_midyear == NULL)
                                            <span class="badge badge-warning">Midyear</span>
                                        @endif
                                        @if($item->date_yearend == NULL)
                                            <span class="badge badge-danger">Yearend</span>
                                        @endif
                                    </td>
                                    </tr>
                                    @php 
                                        $first++;
                                    @endphp
                                @endforeach
                                @php 
                                    $no++
                                @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
                
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
           
            $('.select2').select2({
                placeholder: "Select an attribute",
                allowClear: true,
            });
            
            $('#yearpicker').datepicker({
                autoclose: true,
                container:'#page-content',
                format: " yyyy", // Notice the Extra space at the beginning
                viewMode: "years",
                minViewMode: "years",
                orientation: 'bottom'
            }).on('changeDate', function(e) {
                var date = encodeURIComponent(this.value.trim());
                var url = "/performance/monitoring/"+date;
                window.open(url, '_self');
            });

            $('.employee').select2({
                placeholder: 'Cari Employee dengan mengetik 3 huruf...',
				allowClear: true,
				minimumInputLength: 3,
                ajax: {
                    url: '/get_employee/',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.name + " [" + item.department + "/" + item.workarea + "]",
                                    id: item.nik
                                }
                            })
                        };
                    },
                    cache: true
                }
            });


        });
    </script>
@stop