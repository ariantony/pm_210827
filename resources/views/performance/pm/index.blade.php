@extends('layouts.app')

@section('page-header')
    @lang('pm.performance')
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('app.home')</a></li>
        <li class="breadcrumb-item active" aria-current="page">@lang('pm.performance')</li>
    </ol>
@endsection

@section('content')
    <div id="validation-message"></div>
    <div class="row">
        <div class="col-lg-12">
                        
            <div class="card">
                <div class="card-body p-30">
                
                    <div class="pad-btm">
                        <div class="table-toolbar-left">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text text-uppercase font-weight-bold" style="padding:0.3rem 1rem !important;">@lang('pm.periode')</span>
                                </div>
                                <input type="text" name="periode" value="{{ $year }}" class="form-control"  id="yearpicker" required>
                            </div>
                        </div>
                        <div class="table-toolbar-right">
                            <div class="btn-group">
                                <a href="{{ route('performance.upload') }}" class="btn btn-default" ><i class="fa fa-file-excel-o text-success"></i> @lang('global.app_import')</a>
                                <a class="btn btn-default"  data-toggle="collapse" data-target="#export"><i class="fa fa-file-excel-o text-danger"></i> @lang('global.app_export')</a>
                                <a class="btn btn-default"  data-toggle="collapse" data-target="#filter"><i class="ti-filter"></i> @lang('global.app_filter')</a>
                                <a href="#" class="btn btn-danger float-right" id="btnDelete" style="display:none;"> <i class="ti-trash icon-lg"></i>  @lang('global.app_delete')</a>
                            </div>
                        </div>
                        <hr class="m-0">

                        <div class="collapse" id="filter">
                            <hr>
                            <form class="form-inline" action="{{ route('performance.search')}}" method='GET'>
                                <input type="hidden" name="periode" value="{{ $year }}"  class="form-control" id="searchperiod" required style="width:100%">
                                {{ csrf_field() }}
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.company') }}</label>
                                    {!! Form::select('company_id', $company, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.workarea') }}</label>
                                    {!! Form::select('workarea_id', $workarea, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.department') }}</label>
                                    {!! Form::select('department_id', $department, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.level') }}</label>
                                    {!! Form::select('level_id', $level, null, ['class' => 'form-control select2']) !!}
                                </div>
                           
                                <div class="form-group m-t-20 col-sm-12">
                                    <button type="submit" class="btn bg-gradient-blue float-right">@lang('global.app_search')</button>
                                </div>
                            </form>
                            <hr>
                        </div>

                        <div class="collapse" id="export">
                            <hr>
                            <form class="form-inline" action="{{ route('performance.export')}}" method='GET'>
                                <input type="hidden" name="periode" value="{{ $year }}"  class="form-control" id="exportperiod" required style="width:100%">
                                {{ csrf_field() }}
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.company') }}</label>
                                    {!! Form::select('company_id', $company, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.workarea') }}</label>
                                    {!! Form::select('workarea_id', $workarea, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.department') }}</label>
                                    {!! Form::select('department_id', $department, null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.level') }}</label>
                                    {!! Form::select('level_id', $level, null, ['class' => 'form-control select2' ]) !!}
                                </div>
                                <div class="form-group mb-2 col-sm-3">
                                    <label>{{ trans('pm.assessment') }}</label>
                                    <select class="form-control select2" name="tipe">
                                        <option value="midyear">MID-YEAR</option>
                                        <option value="yearend">YEAREND</option>
                                    </select>
                                </div>
                                <div class="form-group m-t-20 col-sm-12">
                                    <button type="submit" class="btn bg-gradient-blue float-right">@lang('global.app_export')</button>
                                </div>
                            </form>
                            <hr>
                        </div>

                    </div>  

                    <div class="table-responsive">
                        <div id="showSelected" style="display:none"></div>
                        <table id="dataTables" class="table table-striped" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="width:50px" class="text-center">
                                        <input class="magic-checkbox" name="select_all" type="checkbox"> <label></label>
                                    </th>
                                    <th style="width:120px">{{ trans('pm.emp_id') }}</th>
                                    <th>{{ trans('pm.emp_name') }}</th>
                                    <th>{{ trans('pm.periode') }}</th>
                                    <th>{{ trans('app.status') }}</th>
                                    <th>{{ trans('app.date_input') }}</th>
                                    <th style="min-width:150px">{{ trans('app.action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
                
        </div>
    </div>

@endsection

@section('js')
    <script>
        function updateDataTableSelectAllCtrl(table){
            var $table             = table.table().node();
            var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
            var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
            var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

            // If none of the checkboxes are checked
            if($chkbox_checked.length === 0){
                chkbox_select_all.checked = false;
                if('indeterminate' in chkbox_select_all){
                    chkbox_select_all.indeterminate = false;
                }

            // If all of the checkboxes are checked
            } else if ($chkbox_checked.length === $chkbox_all.length){
                chkbox_select_all.checked = true;
                if('indeterminate' in chkbox_select_all){
                    chkbox_select_all.indeterminate = false;
                }

            // If some of the checkboxes are checked
            } else {
                chkbox_select_all.checked = true;
                if('indeterminate' in chkbox_select_all){
                    chkbox_select_all.indeterminate = true;
                }
            }
        }

        $(document).ready(function() {

            $('.select2').select2({
                placeholder: "Select an attribute",
                allowClear: true,
            });

            var rows_selected = [];
            var table =  $('#dataTables').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('performance.datatables', $year) }}',
                "pageLength": 50,
                'columnDefs': [{
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'width': '1%',
                    'className': 'text-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox" class="pr_id magic-checkbox" value="'+ data +'"><label></label>';
                    }
                }],
                columns: [
                    {data: 'id'},
                    {data: 'nik', name: 'nik'},
                    {data: 'emp_data', name: 'nama'},
                    {data: 'period', name: 'period',orderable: false, searchable: false},
                    {data: 'status', name: 'status'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                'rowCallback': function(row, data, dataIndex){
                    var rowId = data['id'];
                    if($.inArray(rowId, rows_selected) !== -1){
                        $(row).find('input[type="checkbox"]').prop('checked', true);
                        $(row).addClass('selected');
                    }
                }
            });


            $('#dataTables tbody').on('click', 'input[type="checkbox"]', function(e){
                var $row = $(this).closest('tr');
                var data = table.row($row).data();
                var rowId = data['id'];
                var index = $.inArray(rowId, rows_selected);
                if(this.checked && index === -1){
                    rows_selected.push(rowId);
                } else if (!this.checked && index !== -1){
                    rows_selected.splice(index, 1);
                }
                if(this.checked){
                    $row.addClass('selected');
                } else {
                    $row.removeClass('selected');
                }
                updateDataTableSelectAllCtrl(table);

                if( rows_selected.length > 0){
                    $('#btnDelete').show();
                    $('#showSelected').show();
                    $('#showSelected').text('Data Selected:'+ rows_selected.length);
                }else{
                    $('#showSelected').hide();
                    $('#btnDelete').hide();
                }

                e.stopPropagation();
            });

            $('#dataTables').on('click', 'tbody td, thead th:first-child', function(e){
                $(this).parent().find('input[type="checkbox"]').trigger('click');
            });

            $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
                if(this.checked){
                    $('#dataTables tbody input[type="checkbox"]:not(:checked)').trigger('click');
                } else {
                    $('#dataTables tbody input[type="checkbox"]:checked').trigger('click');
                }

                e.stopPropagation();
            });

            table.on('draw', function(){
                updateDataTableSelectAllCtrl(table);
            });


            $(document).on("click", "#btnDelete", function(e) {
                var form = this;
                var arr_id = [];

                $.each(rows_selected, function(index, rowId){
                    arr_id.push(rowId);
                });

                if(arr_id.length === 0){
                    Swal.fire(
                        'Informasi',
                        'Minimal Checklist 1 Item PO',
                        'warning'
                    );
                    return false;
                }else{
                
                    var id = arr_id;

                    Swal.fire({
                        title: 'Confirmation', // Opération Dangereuse
                        text: 'Are you sure?', // Êtes-vous sûr de continuer ?
                        type: 'question',
                        showCancelButton: true,
                        confirmButtonColor: 'null',
                        cancelButtonColor: 'null',
                        confirmButtonClass: 'btn btn-danger',
                        cancelButtonClass: 'btn btn-primary',
                        confirmButtonText: 'Yes, Delete it', // Oui, sûr
                        cancelButtonText: 'Cancel', // Annuler
                    }).then(res => {
                        if (res.value) {
                            $.ajax({
                                type:'POST',
                                url:'/performance/destroy',
                                data:{
                                    'id':id,
                                    '_token': $('meta[name="csrf-token"]').attr('content')
                                },
                                success:function(data){
                                    $('#validation-message').append('<div class="alert dark alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button> Delete was successfull</div>');
                                    setTimeout(function () { document.location.reload(true); }, 300);
                                },
                                error: function(xhr)
                                {
                                    $('#validation-message').html('');
                                    $.each(xhr.responseJSON.errors, function(key,value) {
                                        $('#validation-message').append('<div class="alert dark alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>'+value+'</b></div>');
                                    });
                                }
                            });
                        }
                    });
                }
            });

            $('#yearpicker').datepicker({
                autoclose: true,
                container:'#page-content',
                format: " yyyy", // Notice the Extra space at the beginning
                viewMode: "years",
                minViewMode: "years",
                orientation: 'bottom'
            }).on('changeDate', function(e) {
                var date = encodeURIComponent(this.value.trim());
                var url = "/performance/data/"+date;
                window.open(url, '_self');
            });



        });
    </script>
@stop