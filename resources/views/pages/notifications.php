<?php include "../partials/header.php";?>

		<!--CONTENT CONTAINER-->
		<div id="content-container">
			
      <div id="page-head">
				<div class="page-title">
					<h4 class="page-header text-overflow">Pemberitahuan</h4>
				</div>
				<ol class="breadcrumb">
					<li><a href="../dashboard/index.php"><i class="ti-home"></i></a></li>
					<li class="active"><a href="#">Pemberitahuan</a></li>
				</ol>
			</div>


			<div id="page-content">

				<div class="card">
					<div class="card-body p-30">
            <div class="row tabs-vertical-env justify-content-between">
              <div class="col-lg-3">
                <hr>
                <a href="" class="">Hapus semua pemberitahuan <i class="fa fa-external-link font-16 m-l-10 text-right"></i> </a>
              </div>
              <div class="col-lg-9">
                <div class="kl-widget m-b-10 bg-white">
                  <div class="kl-widget-content p-25">
                    <form role="search" class="text-right" action="../konfigurasi/list-module.html">
                        <input type="text" placeholder="Cari pemberitahuan..." class="form-control">
                    </form>
                    <div class="tabel-responsive m-t-20">
                      <table class="table table-hover mails m-0">
                        <tbody>
                          <tr>
                            <td colspan="4"><strong>Jumat, 27 November 2017<strong></td>
                          </tr>
                          <tr>
                            <td style="width: 50px;">
                                    <input id="checkbox1" type="checkbox"  class="magic-checkbox magic-checkbox-primary">
                                    <label for="checkbox1"></label>
                            </td>
                            <td class="text-left"> 07:23 AM </td>
                            <td><a href="mail-read.html">Pengajuan Cuti oleh Vinsensiu Agung S. </a></td>
                            <td style="width: 20px;"><i class="icon-options-vertical"></i></td>
                          </tr>
                          <tr>
                            <td style="width: 50px;">
                                    <input id="checkbox2" type="checkbox"  class="magic-checkbox magic-checkbox-primary">
                                    <label for="checkbox2"></label>
                            </td>
                            <td class="text-left"> 07:23 AM </td>
                            <td><a href="mail-read.html">Pengajuan Cuti oleh Vinsensiu Agung S. </a></td>
                            <td style="width: 20px;"><i class="icon-options-vertical"></i></td>
                          </tr>
                          <tr>
                            <td style="width: 50px;">
                                    <input id="checkbox3" type="checkbox"  class="magic-checkbox magic-checkbox-primary">
                                    <label for="checkbox3"></label>
                            </td>
                            <td class="text-left"> 07:23 AM </td>
                            <td><a href="mail-read.html">Pengajuan Cuti oleh Vinsensiu Agung S. </a></td>
                            <td style="width: 20px;"><i class="icon-options-vertical"></i></td>
                          </tr>
                            <tr>
                            <td colspan="4"><strong>Kamis, 26 November 2017<strong></td>
                          </tr>
                          <tr>
                            <td style="width: 50px;">
                                    <input id="checkbox4" type="checkbox"  class="magic-checkbox magic-checkbox-primary">
                                    <label for="checkbox4"></label>
                            </td>
                            <td class="text-left"> 07:23 AM </td>
                            <td><a href="mail-read.html">Pengajuan Cuti oleh Vinsensiu Agung S. </a></td>
                            <td style="width: 20px;"><i class="icon-options-vertical"></i></td>
                          </tr>
                          <tr>
                            <td style="width: 50px;">
                                    <input id="checkbox5" type="checkbox" class="magic-checkbox magic-checkbox-primary">
                                    <label for="checkbox5"></label>
                            </td>
                            <td class="text-left"> 07:23 AM </td>
                            <td><a href="mail-read.html">Pengajuan Cuti oleh Vinsensiu Agung S. </a></td>
                            <td style="width: 20px;"><i class="icon-options-vertical"></i></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>

					</div>
				</div>

			</div>

		</div>
		<!--END CONTENT CONTAINER-->

<?php include "../partials/footer.php";?>