<?php include "../partials/header.php";?>

	<!--CONTENT CONTAINER-->
	<div id="content-container">
		<div id="page-head">
			<div class="page-title">
				<h4 class="page-header text-overflow">Pengumuman</h4>
			</div>
			<ol class="breadcrumb">
				<li><a href="../dashboard/index.php"><i class="ti-home"></i></a></li>
				<li class="active">Pengumuman</li>
			</ol>

		</div>

		<div id="page-content">
			<div class="row justify-content-center">
				<div  class="col-9">
					<div class="card">
						<div class="card-body">
							<form action="#" method="post">
								<label class="text-muted mb-2">Cari Pengumuman Berdasarkan Judul: </label>
								<div class="input-group">
									<input type="text" placeholder="Ketik kata kunci..." class="form-control">
									<span class="input-group-btn">
										<button class="btn bg-gradient-success" type="button">Search</button>
									</span>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
	
			<div class="blog">
				<div class="blog-item col-sm-4">
					<div class="card">
						<div class="blog-content">
							<div class="blog-title media-block">
								<div class="media-body">
									<a href="#" class="btn-link">
										<h2>Libur Natal dan Tahun Baru</h2>
									</a>
								</div>
							</div>
							<div class="blog-body">
								<p>Diberitahukan kepada Karwayan bahwa jadwal libur Natal dan Tahun Baru tanggal adalah 25 Desember 2017 - 4 Januari 2018! </p>
							</div>
						</div>
						<div class="blog-footer">
							<div class="media-left">
								<span class="text-muted">5 days ago</span>
							</div>
							<div class="media-body text-right">
								<span class="mar-rgt"><i class="ti-heart icon-fw"></i>519</span>
								<i class="ti-eye icon-fw"></i>23
							</div>
						</div>
					</div>
				</div>

				<div class="blog-item col-sm-4">
					<div class="card">
						<div class="blog-header">
							<img class="img-responsive" src="https://images.unsplash.com/photo-1509629555017-fb1950574d52?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80">
						</div>
						<div class="blog-content">
							<div class="blog-title media-block">
								<div class="media-body">
									<a href="#" class="btn-link">
										<h2>Libur Natal dan Tahun Baru</h2>
									</a>
								</div>
							</div>
							<div class="blog-body">
								<p>Diberitahukan kepada Karwayan bahwa jadwal libur Natal dan Tahun Baru tanggal adalah 25 Desember 2017 - 4 Januari 2018! </p>
							</div>
						</div>
						<div class="blog-footer">
							<div class="media-left">
								<span class="text-muted">5 days ago</span>
							</div>
							<div class="media-body text-right">
								<span class="mar-rgt"><i class="ti-heart icon-fw"></i>519</span>
								<i class="ti-eye icon-fw"></i>23
							</div>
						</div>
					</div>
				</div>

				<div class="blog-item col-sm-4">
					<div class="card">
						<div class="blog-content">
							<div class="blog-title media-block">
								<div class="media-body">
									<a href="#" class="btn-link">
										<h2>Libur Natal dan Tahun Baru</h2>
									</a>
								</div>
							</div>
							<div class="blog-body">
								<p>Diberitahukan kepada Karwayan bahwa jadwal libur Natal dan Tahun Baru tanggal adalah 25 Desember 2017 - 4 Januari 2018! </p>
							</div>
						</div>
						<div class="blog-footer">
							<div class="media-left">
								<span class="text-muted">5 days ago</span>
							</div>
							<div class="media-body text-right">
								<span class="mar-rgt"><i class="ti-heart icon-fw"></i>519</span>
								<i class="ti-eye icon-fw"></i>23
							</div>
						</div>
					</div>
				</div>

				<div class="blog-item col-sm-4">
					<div class="card">
						<div class="blog-header">
							<img class="img-responsive" src="https://images.unsplash.com/photo-1559809928-bd1be8ea204b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80">
						</div>
						<div class="blog-content">
							<div class="blog-title media-block">
								<div class="media-body">
									<a href="#" class="btn-link">
										<h2>Libur Idul Fitri 2019</h2>
									</a>
								</div>
							</div>
							<div class="blog-body">
								<p>Diberitahukan kepada Karwayan bahwa jadwal libur Natal dan Tahun Baru tanggal adalah 25 Desember 2017 - 4 Januari 2018! </p>
							</div>
						</div>
						<div class="blog-footer">
							<div class="media-left">
								<span class="text-muted">5 days ago</span>
							</div>
							<div class="media-body text-right">
								<span class="mar-rgt"><i class="ti-heart icon-fw"></i>519</span>
								<i class="ti-eye icon-fw"></i>23
							</div>
						</div>
					</div>
				</div>
				
				<div class="blog-item col-sm-4">
					<div class="card">
						<div class="blog-header">
							<img class="img-responsive" src="https://images.unsplash.com/photo-1556761175-b413da4baf72?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=967&q=80">
						</div>
						<div class="blog-content">
							<div class="blog-title media-block">
								<div class="media-body">
									<a href="#" class="btn-link">
										<h2>Seminar: Employee Engagement </h2>
									</a>
								</div>
							</div>
							<div class="blog-body">
								<p>Diberitahukan kepada Karwayan bahwa jadwal libur Natal dan Tahun Baru tanggal adalah 25 Desember 2017 - 4 Januari 2018! </p>
							</div>
						</div>
						<div class="blog-footer">
							<div class="media-left">
								<span class="text-muted">5 days ago</span>
							</div>
							<div class="media-body text-right">
								<span class="mar-rgt"><i class="ti-heart icon-fw"></i>519</span>
								<i class="ti-eye icon-fw"></i>23
							</div>
						</div>
					</div>
				</div>
			
				<div class="blog-item col-sm-4">
					<div class="card">
						<div class="blog-content">
							<div class="blog-title media-block">
								<div class="media-body">
									<a href="#" class="btn-link">
										<h2>Libur Natal dan Tahun Baru</h2>
									</a>
								</div>
							</div>
							<div class="blog-body">
								<p>Diberitahukan kepada Karwayan bahwa jadwal libur Natal dan Tahun Baru tanggal adalah 25 Desember 2017 - 4 Januari 2018! </p>
							</div>
						</div>
						<div class="blog-footer">
							<div class="media-left">
								<span class="text-muted">5 days ago</span>
							</div>
							<div class="media-body text-right">
								<span class="mar-rgt"><i class="ti-heart icon-fw"></i>519</span>
								<i class="ti-eye icon-fw"></i>23
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<ul class="pager pager-rounded">
				<li class="previous"><a href="#fakelink">← Older</a></li>
				<li class="next disabled"><a href="#fakelink">Newer →</a></li>
			</ul>
		        
        </div>
		
	</div>
	<!--END CONTENT CONTAINER-->

<?php include "../partials/footer.php";?>
<script>
	$(document).ready(function() {
		$('.blog').masonry({
			// options...
			itemSelector: '.blog-item',
			columnWidth: 200
		});
	})
</script>