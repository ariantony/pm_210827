
@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body p-30">
			<div class="row">
				<div class="col-lg-9">
					<h4>
						Selamat Datang di Klola Version 2.0
					</h4>	
					<p class="font-14"> Terima Kasih telah menggunakan Klola version 2.0. Aplikasi Klola dibangun menggunakan
						teknologi terkini dan didukung dengan jaringan yang aman.</p>
				</div>
				<div class="col-lg-3">
					<img class="rounded float-right" src="../../assets/images/badge.png" alt="">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<ul class="nav nav-tabs tabs-bordered m-b-20">
						<li class="nav-item"><a href="#ps1" data-toggle="tab" aria-expanded="true" class="nav-link active">Perubahan</a></li>
						<li class="nav-item"><a href="#ps3" data-toggle="tab" aria-expanded="false" class="nav-link">Kebijakan Privasi</a></li>
						<li class="nav-item"><a href="#ps4" data-toggle="tab" aria-expanded="false" class="nav-link">Bantuan</a></li>
					</ul>
					<div class="tab-content">

						<div role="tabpanel" class="tab-pane active in" id="ps1">
							<p><strong>Version 2.0.1</strong> menambahkan konfigurasi templates </p>
							<p><strong>Version 2.0.0</strong> mengubah core Aplikasi dengan memisah sistem backend dan frontend. </p>
						</div>

						<div role="tabpanel" class="tab-pane" id="ps2">

						</div>

						<div role="tabpanel" class="tab-pane" id="ps3">

						</div>

					</div>
				</div>
			</div>
		</div>

	</div>
@endsection
