<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Aplication Language Lines
    |--------------------------------------------------------------------------
    |
    */
    'klola_tagline' => 'Managing People. Anywhere. Anytime.',
    'klola_description' => 'Klola helps companies of various sizes, industries and employment situations improve their work-efficiency and effectiveness of people management in companies', 
    'forgot_password' => 'Forgot Password?',
    'login_title' => 'Please input your Username & Password',
    'remember_me' => 'Remember Me',
    'logout' => 'Logout',
    'login' => 'Login',
    'username' => 'Username',
    'email' => 'Email',
    'password' => 'Password',
    'lock_screen' => 'Lock Screen',
    'profile' => 'Profile',
    'about_us' => 'About Klola PM',
    'show_all_notifications' => 'Show All Notifications',
    'view_all'     => 'View All',
    'copyright'    => 'Made with in Indonesia',
    'submit' => 'Submit',
    'continue' => 'Continue',
    'dashboard' => 'Dashboard',
    'home' => 'Home',
    'input_nik'    => 'Please Input Your Employee Number',
    'search_by_title' => 'Search Announcement By Title',
   
    /*
   |--------------------------------------------------------------------------
   | Setting
   |--------------------------------------------------------------------------
   |
   */
   'master' => 'Master',
   'setting'  => 'Setting',
   'configuration'  => 'Configuration',
   'general'  => 'General',
   'announcement'  => 'Announcement',
   'features'  => 'Features',
   'setting_field_1' => 'Weight KPI vs Competency',
   'group_raters' => 'Group Raters',
   'change_password' => 'Change Password',
   'change_profile' => 'Change Profile',
   'monitoring' => 'Monitoring',

    /*
   |--------------------------------------------------------------------------
   | Table General
   |--------------------------------------------------------------------------
   |
   */
   'name'     => 'Name',
   'status'  => 'Status',
   'updated' => 'Updated',
   'created_at' => 'Created At',
   'action' => 'Action',
   'description' => 'Description',
   'content' => 'Content',
   'expired' => 'Expired',
   'title' => 'Title',
   'weight' => 'Weight',
   'form' => 'Formulir',
   'config' => 'Config',
   'history' => 'History',
   'import' => 'Import',
   'date_input' => 'Input Date',

    /*
   |--------------------------------------------------------------------------
   | Message
   |--------------------------------------------------------------------------
   |
   */
  'message_success_input' => 'Data was successful input',
  'message_failed_input' => 'Data was unsuccessful input',
  'message_success_delete' => 'Delete was successful!',
  'notice' => 'Notice',
  'not_found' => 'Data not found',
  'model' => 'Upload Model',
  
];
