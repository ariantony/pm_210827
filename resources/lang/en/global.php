<?php

return [
	
	'user-management' => [
		'title' => 'User Management',
		'created_at' => 'Time',
		'fields' => [
		],
	],
	
	'permissions' => [
		'title' => 'Permissions',
		'created_at' => 'Time',
		'fields' => [
			'name' => 'Name',
		],
	],
	
	'roles' => [
		'title' => 'Roles',
		'created_at' => 'Time',
		'fields' => [
			'name' => 'Name',
			'permission' => 'Permissions',
		],
	],
	
	'users' => [
		'title' => 'Users',
		'created_at' => 'Time',
		'fields' => [
			'name' => 'Name',
			'email' => 'Email',
			'password' => 'Password',
			'roles' => 'Roles',
			'remember-token' => 'Remember token',
		],
	],
	'app_next' => 'Next',
	'app_previous' => 'Previous',
	'app_add' => 'Add',
	'app_create' => 'Create',
	'app_save' => 'Save',
	'app_save_draft' => 'Save As Draft',
	'app_yes' => 'Yes',
	'app_result' => 'Result',
	'app_submit' => 'Submit',
	'app_cancel' => 'Cancel',
	'app_search' => 'Search',
	'app_search_by' => 'Search By',
	'app_show' => 'Show',
	'app_showall' => 'Show All',
	'app_choose' => 'Choose Something',
	'app_download' => 'Download',
	'app_clearall' => 'Clear All',
	'app_edit' => 'Edit',
	'app_view' => 'View',
	'app_preview' => 'Preview',
	'app_filter' => 'Filter',
	'app_export' => 'Export',
	'app_import' => 'Import',
	'app_detail' => 'Detail',
	'app_update' => 'Update',
	'app_list' => 'List',
	'app_no_entries_in_table' => 'No entries in table',
	'app_no_data' => 'Data not available',
	'custom_controller_index' => 'Custom controller index.',
	'app_logout' => 'Logout',
	'app_add_new' => 'Add new',
	'app_confirmation' => 'Confirmation?',
	'app_information' => 'Information',
	'app_are_you_sure' => 'Are you sure?',
	'app_back_to_list' => 'Back to list',
	'app_dashboard' => 'Dashboard',
	'app_delete' => 'Delete',
	'global_title' => 'Roles-Permissions Manager',
	'app_back' => 'Back',
	'upload_data' => 'Upload Data',
	'data' => 'Data',
	'file_caption' => 'Choose your xls/csv File',

];