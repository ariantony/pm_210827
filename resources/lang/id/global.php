<?php

return [
	
	'user-management' => [
		'title' => 'Manajemen Pengguna',
		'created_at' => 'Time',
		'fields' => [
		],
	],
	
	'permissions' => [
		'title' => 'Hak Akses',
		'created_at' => 'Time',
		'fields' => [
			'name' => 'Name',
		],
	],
	
	'roles' => [
		'title' => 'Roles',
		'created_at' => 'Time',
		'fields' => [
			'name' => 'Name',
			'permission' => 'Permissions',
		],
	],
	
	'users' => [
		'title' => 'Pengguna',
		'created_at' => 'Time',
		'fields' => [
			'name' => 'Name',
			'email' => 'Email',
			'password' => 'Password',
			'roles' => 'Roles',
			'remember-token' => 'Remember token',
		],
	],
	'app_next' => 'Lanjutkan',
	'app_previous' => 'Sebelumnya',
	'app_add' => 'Tambah',
	'app_create' => 'Tambah',
	'app_save' => 'Simpan',
	'app_save_draft' => 'Simpan Sebagai Draft',
	'app_yes' => 'Ya',
	'app_submit' => 'Kirim',
	'app_cancel' => 'Batal',
	'app_search' => 'Cari',
	'app_search_by' => 'Cari Berdasarkan',
	'app_show' => 'Lihat',
	'app_result' => 'Hasil',
	'app_showall' => 'Tampilkan Semua',
	'app_choose' => 'Silahkan Pilih',
	'app_download' => 'Unduh',
	'app_clearall' => 'Hapus Semua',
	'app_edit' => 'Edit',
	'app_view' => 'Lihat',
	'app_preview' => 'Pratinjau',
	'app_filter' => 'Saring',
	'app_export' => 'Unduh',
	'app_import' => 'Unggah',
	'app_detail' => 'Detail',
	'app_update' => 'Update',
	'app_list' => 'List',
	'app_no_entries_in_table' => 'Tidak ditemukan Data',
	'app_no_data' => 'Data tidak ditemukan',
	'custom_controller_index' => 'Custom controller index.',
	'app_logout' => 'Keluar',
	'app_add_new' => 'Tambah Baru',
	'app_confirmation' => 'Konfirmasi',
	'app_information' => 'Informasi',
	'app_are_you_sure' => 'Apakah Anda Yakin?',
	'app_back_to_list' => 'Kembali ke table',
	'app_dashboard' => 'Dashboard',
	'app_delete' => 'Hapus',
	'global_title' => 'Roles-Permissions Manager',
	'app_back' => 'Back',
	'upload_data' => 'Unggah Data',
	'data' => 'Data',
	'file_caption' => 'Pilih File xls/csv Anda',

];