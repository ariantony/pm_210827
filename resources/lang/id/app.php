<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Aplication Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'klola_tagline' => 'Manajemen SDM. Darimana saja. Kapan saja.',
    'klola_description' => 'Klola telah membantu meningkatkan efisiensi dan efektifitas pengelolaan SDM di berbagai perusahaan di berbagai industri dan berbagai ukuran dan kondisi kepegawaian di Indonesia', 
    'forgot_password' => 'Lupa Password?',
    'login_title' => 'Masukan Username dan Password anda.',
    'remember_me' => 'Ingat Saya',
    'logout' => 'Keluar',
    'login' => 'Masuk',
    'username' => 'Username',
    'email' => 'Email',
    'password' => 'Kata Sandi',
    'lock_screen' => 'Kunci Layar',
    'profile' => 'Profile',
    'about_us' => 'Tentang Klola PM',
    'show_all_notifications' => 'Tampilkan Semua Notification',
    'view_all'     => 'Tampilkan Semua',
    'copyright'    => 'Made with  in Indonesia',
    'submit' => 'Simpan',
    'lanjut' => 'Lanjutkan',
    'dashboard' => 'Dashboard',
    'home' => 'Beranda',
    'input_nik'    => 'Silahkan masukan Nomor Induk Karyawan (NIK) anda.',
    'search_by_title' => 'Cari Pengumuman Berdasarkan Judul',
   
     /*
    |--------------------------------------------------------------------------
    | Menu
    |--------------------------------------------------------------------------
    |
    */
    'master' => 'Master',
    'setting'  => 'Pengaturan',
    'configuration'  => 'Konfigurasi Umum',
    'announcement'  => 'Pengumuman',
    'general'  => 'Umum',
    'features'  => 'Fitur',
    'setting_field_1' => 'Bobot KPI vs Kompetensi ',
    'group_raters' => 'Group Penilai',
    'change_password' => 'Ganti Kata Sandi',
    'change_profile' => 'Ganti Profile',
    'monitoring' => 'Monitoring',
     /*
   |--------------------------------------------------------------------------
   | Table General
   |--------------------------------------------------------------------------
   |
   */
   'name'     => 'Nama',
   'status'  => 'Status',
   'updated' => 'Diperbaiki',
   'created_at' => 'Dibuat',
   'action' => 'Aksi',
   'description' => 'Description',
   'content' => 'Content',
   'expired' => 'Expired',
   'title' => 'Judul',
   'weight' => 'Bobot',
   'form' => 'Formulir',
   'config' => 'Atur',
   'history' => 'Riwayat',
   'import' => 'Unggah',
   'date_input' => 'Tanggal Input',

   /*
   |--------------------------------------------------------------------------
   | Messages
   |--------------------------------------------------------------------------
   |
   */
   'message_success_input' => 'Input data berhasil',
   'message_failed_input' => 'Input data tidak berhasil',
   'message_success_delete' => 'Hapus data berhasil',
   'notice' => 'Peringatan',
   'not_found' => 'Data tidak ditemukan',
   'model' => 'Model Unggahan',

];  
