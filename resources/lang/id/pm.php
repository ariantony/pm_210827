<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Kinerjance Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'performance' => 'Kinerja',
    'input_performance' => 'Isi Kinerja',
    'my_performance' => 'Kinerja Saya',
    'my_approval' => 'Approval',
    'overall_performance' => 'Kinerja Keseluruhan',
    'performance_appraisal' => 'Penilaian Kinerja',
    'performance_aspect' => 'Aspek Kinerja',
    'area_of_performance' => 'Area Kinerja',
    'emp_name' => 'Nama Pegawai',
    'emp_id' => 'Pegawai ID',
    'id' => 'NIK',
    'emp_data' => 'Employee Data',
    'gender' => 'Jenis Kelamin',
    'position' => 'Posisi',
    'division' => 'Divisi',
    'department' => 'Departemen',
    'workarea' => 'Lokasi Kerja',
    'level'     => 'Pangkat',
    'company'   => 'Perusahaan',
    'rater' => 'Penilai',
    'rater_as' => 'Penilai sebagai',
    'raters' => 'Penilai',
    'relationship' => 'Hubungan',
    'pa_before' => 'Hasil Penilaian Kinerja Sebelumnya',
    'score_range' => 'Nilai Kompetensi (skala 1 sampai 5)',
    'adjusted_score' => 'Unggah Nilai Banding',

    'personal'      => 'Personal',
    'team'          => 'Tim',
    'goals'         => 'Goals',
    'competency'    => 'Kompetensi',
    'periode' => 'Periode',
    'year_assessment' => 'Periode Penilaian',
    'year' => 'Tahun',
    'kpi' => 'KPI',
    'score' => 'Nilai',
    'kpi_score' => 'Nilai KPI',
    'competency_score' => 'Nilai Kompetensi',
    'objective'    => 'Sasaran',
    'objective_category'    => 'Kategori Sasaran',
    'highest'   => 'Tertinggi',
    'lowest'    => 'Terendah',
    'employee_total'   => 'Total Pegawai',
    'assessment'   => 'Penilaian',
    'my_assessment'   => 'Penilaian Saya',
    'target'   => 'Target',
    'target_revision'   => 'Revisi Target',
    'measure'   => 'Satuan',
    'total'   => 'Total',
    'history'   => 'Histori',
    'total_score'   => 'Total Nilai',
    'total_weight'   => 'Total Bobot',
    'kpi_score_achievement'   => 'Nilai KPI',
    'midterm_kpi_score' => 'Nilai KPI Tengah Tahun',
    'achievement'   => 'Pencapaian',
    'actual_annual_achievement' => 'Actual Annual Achievement',
    'midterm_result'   => 'Hasil Jangka Menengah',
    'midyear_achievement'   => 'Pencapaian Tengah Tahun',
    'yearend_achievement'   => 'Pencapaian Akhir Tahun',
    'set_midyear_achievement'   => 'Isi Pencapaian Tengah Tahun',
    'set_yearend_achievement'   => 'Isi Pencapaian Akhir Tahun',
    'midyear'   => 'Tengah Tahun',
    'yearend'   => 'Akhir Tahun',
    'final_score'   => 'Nilai Akhir',
    'total_weight_information' => 'Total Bobot dari KPI harus 100%',
    'comment' => 'Komentar Penilai',
    'midterm_kpi_score_information' => 'Nilai KPI Tengah Tahun antara 1-5',

    'form_setting_raters' => 'Form Pengaturan Penilai',
    'total_raters' => 'Jumlah Penilai',
    'setting_raters_info' => 'Untuk Pengaturan Penilai, Silahkan pilih nama pegawai per group penilai pada form dibawah ini.',

    'message_failed_input' => 'Input data tidak berhasil, karena ada data dengan periode yang sama',
    'message_failed_2_input' => "Input data tidak berhasil, karena kamu tidak memiliki Penilai",
    'message_failed_weight' => 'Total Bobot tidak boleh lebih dari 100%',

    'email_title' =>'Form Performa Management System',
    'email_content' =>'Telah melakukan pengisiian Form Performa Management System.',
    'total_weighted_score' =>'Total Bobot Skor',
    'check_assessment_guide' =>'Cek Pedomanan Penilaian',
    'assessment_guide' =>'Pedomanan Penilaian',
    'adjustment_1' =>'Nilai Banding 1',
    'adjustment_2' =>'Nilai Banding 2',
    'adjusted' =>'Nilai Banding',
    'message_detail_failed' => 'Belum ada penilaian',

    'dashoard_title_2' => 'Daftar pegawai yang akan anda nilai',
    'form_input_caption' => 'Anda dapat mengisi KPI dengan formulir di bawah ini atau mengunggahnya. Jika Anda memilih untuk mengunggah, silakan klik tombol unggah data.',
    'form_upload_caption' => 'Silakan gunakan template ini untuk mengunggah KPI',
    'message_upload_failed' => 'Template KPI tidak sesuai, mohon diperhatikan kembali',
    'validation_rate' => 'Mohon berikan penilaian Anda',
    'info_validation_rate' => 'Ada kompetensi yang belum diberikan penilaian',
    'info_validation_rate_kpi' => 'Ada KPI yang belum diberikan penilaian',
    'info_validation_achievement' => 'Ada Achievements yang belum anda isi',
    'date_input' => 'Tanggal Input',
    'adj_score' =>'Nilai Banding',
    'data_not_found' => 'Data tidak ditemukan',

];
