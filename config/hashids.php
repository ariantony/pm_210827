<?php

return [

    'connections' => [

        'main' => [
           'salt' => 'RBLSxlv1bv84D2FWOizGxyaXojAr4urW',
           'length' => '6',
           'alphabet' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789',
        ],
     
        'numbers' => [
           'salt' => 'RBLSxlv1bv84D2FWOizGxyaXojAr4urW',
           'length' => '6',
           'alphabet' => '1234567890',
         ],
      ],
     
];
