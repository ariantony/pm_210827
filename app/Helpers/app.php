<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\User;


function getAspect($aspect)
{
	$query = DB::table('categories')
	->select('*')
	->where('name', $aspect)
	->where('status', 1)
	->first();
	return $query;
}

function getStatusData($status, $raw = null)
{
	if($status == 1){
		if($raw==null){
			return "<span class='badge badge-success'>Active</span>";
		}else{
			return "Active";
		}
	}
	else{
		if($raw==null){
			return "<span class='badge badge-warning'>Draft</span>";
		}else{
			return "Draft";
		}
	}
}

function getStatusPM($status, $raw = null)
{
	if($status == 1){
		if($raw==null){
			return "<span class='badge badge-warning'>Waiting to Approve</span>";
		}else{
			return "Waiting to Approve";
		}
	}elseif($status == 2){
		if($raw==null){
			return "<span class='badge badge-success'>On Process</span>";
		}else{
			return "Draft";
		}
	}
	elseif($status == 3){
		if($raw==null){
			return "<span class='badge badge-danger'>Done</span>";
		}else{
			return "Done";
		}
	}
	else{
		if($raw==null){
			return "<span class='badge badge-info'>Draft</span>";
		}else{
			return "Draft";
		}
	}
}


function getAvatar($user_id)
{
	$query = "https://hes.harvestgroup.id:60443/api/dashboardpegawai/browse/image/image:image:jpg:1:1:97/large_".$user_id."_source_200x200?token=hKubMxdn2B71XGPuOqqjRR09ixvZgmjqH1iGFk7UTHjDZIE3lc70luwkOsG1W5sJ9LZLUJX8T9XPjm5OyvohcZEeaTMNgZnx07VVN6JNcNncuPRkO5tH3+GQIja+XWif";
	return $query;
}


function getNotifications($user_id)
{	
	$query = DB::table('notifications')
	->select('*')
	->where('user_id', $user_id)
	->where('status', 0)
	->get();
	return $query;
}

function getRate($emp_id,$competency_id,$pm_id)
{	
	$query = DB::table('competency_assessments')
	->select('*')
	->where('emp_id', $emp_id)
	->where('competency_id', $competency_id)
	->where('performance_id', $pm_id)
	->first();
	return $query;
}

function getUserByID($id)
{	
	$users = DB::table('users')
	->select('users.*')
	->where('id', $id)
	->first();
    if ($users) {
        return $users->name;
    }else{
		return "-";
	}
}

function getDataByID($table, $id)
{	
	$data= DB::table($table)
	->select('*')
	->where('id', $id)
	->first();
   return $data;
    
}

function getDataByParam($table, $param, $value)
{	
	$data= DB::table($table)
	->select('*')
	->where($param, $value)
	->first();
   return $data;
    
}


function totalScore()
{	
	$data= DB::table($table)
	->select('*')
	->where($param, $value)
	->first();
   return $data;
    
}


function isAdministrator() {
	if(Auth::user()->type==1){
		return true;
	}else{
		return false;
	}
} 

function isEmployee () {
    if (Auth::user()->type==4) {
        return true;
    } else {
        return false;
    }
}


function getSetting($key)
{	
	$query = DB::table('settings')
	->select('*')
	->where('key', $key)
	->first();
	return $query->value;
}

function isStaff($id){
	$staff = explode(",",config('app.staff_id'));

	if(in_array($id,$staff)){
		return true;
	}else{
		return false;
	}
}


function getAssessment($pm_id){
	$appraisal    = DB::table('assessments')->where('performance_id',$pm_id)->get();
	$status = [];
	foreach($appraisal  as $val){
		$status[]= $val->status;
	}
	if(in_array(0,$status)){
		return false;
	}else{
		return true;
	}
}

function scoreCompetency($id,$rate){

	$competency = DB::table('performance_appraisal')
	->select('performance_appraisal.*')
	->where('performance_id', $id)
	->get();

	$scorePA = 0;
	foreach($competency as $val){
		$score = json_encode($val->rate_yearend);
		if($score != null){
			foreach ($score as $item){
				$scorePA = 0;
			}
		}
		
	}
	print_r($scorePA).exit();

}

function getBobot($group,$json){
	$weight = '';
	$arr = json_decode($json,true);
	if(array_key_exists($group, $arr)){
		$weight = $arr[$group];
	}
	return (int) $weight;
}

function getPAScore($rater_weight,$competency_weight, $pm_id, $type ){
	$score = '';

	$competency = DB::table('performance_appraisal')
	->select('performance_appraisal.*')
	->where('performance_id', $pm_id)
	->get();

	$group_raters = DB::table('group_raters')->get();
	$appraisal    = DB::table('assessments')->where('performance_id',$pm_id)->get();

	$scorePA = 0;
	$nilaiGroup = [];

	foreach($competency as $val){
		if($type == "midyear"){
			$rate = json_decode($val->rate,true);
		}else{
			$rate = json_decode($val->rate_yearend,true);
		}
		$i=1;
		$inc =0;

		if($rate != NULL){
			foreach ($group_raters as $value) {
				$i = 1;
				$nilai = [];

				foreach ($rate as $val) {
					$nilai[$val['nik']] = $val['value'];
				}

				foreach ($appraisal as $val){
					if(strtolower($value->name) == $val->group_rate ){
						if(array_key_exists($val->rater_id,$nilai)){
							$nilaiGroup[$val->group_rate.'_'.$inc][] = $nilai[$val->rater_id];
						}else{
							$nilaiGroup[$val->group_rate.'_'.$inc][] = 0;
						}
						$inc +=1;
					}
				}
			
			}
		}
		
	}

	$totalNilaiPA = 0;

	if($nilaiGroup){

		$key = array_keys($nilaiGroup);
		$NilaiSubordinates = $NilaiPeers = $NilaiSuperior = $NilaiSelf= $nilaiGroupSumSubordinates = $nilaiGroupSumPeers  = 0;
		$count_peers = $count_subordinates = 0;

		for($i = 0;$i<count($key);$i++){
			$groupRater = explode("_",$key[$i]);
			$nilaiGroupSum = 0;
			foreach($nilaiGroup[$key[$i]] as $item){
				$nilaiGroupSum += $item;
			}

			$bobotRater = getBobot($groupRater[0],$rater_weight);

			if(is_numeric($bobotRater)){
				$nilaiGroupRaterSum = $bobotRater /100 * $nilaiGroupSum;
			}else{
				$nilaiGroupRaterSum = 0;
			}

			if($groupRater[0] == 'peers'){
				if($nilaiGroupSum != 0){
					$count_peers ++;
					$nilaiGroupSumPeers += $nilaiGroupSum ;
				}
			}

			if($groupRater[0] == 'subordinates'){
				if($nilaiGroupSum != 0){
					$count_subordinates ++;
					$nilaiGroupSumSubordinates += $nilaiGroupSum ;
				}
			}
			if($groupRater[0] == 'self'){
				$NilaiSelf  = $nilaiGroupRaterSum;
			}
			if($groupRater[0] == 'superior' ){
				$NilaiSuperior = $nilaiGroupRaterSum;
			}
		}


		if($nilaiGroupSumPeers != 0 && is_numeric($nilaiGroupSumPeers)  && is_numeric($count_peers) ){
			$NilaiPeers =  round((getBobot('peers',$rater_weight)/100) * $nilaiGroupSumPeers/$count_peers,2);
		}
		if($nilaiGroupSumSubordinates !=0 && is_numeric($nilaiGroupSumSubordinates)  && is_numeric($count_subordinates) ){
			$NilaiSubordinates  =  round((getBobot('subordinates',$rater_weight)/100) * $nilaiGroupSumSubordinates/$count_subordinates,2);
		}
		$totalNilaiPA = ($NilaiSelf  + $NilaiPeers + $NilaiSubordinates + $NilaiSuperior)/6;
		return $totalNilaiPA;
	}else{
		return $totalNilaiPA;
	}

}

function getRaterByEmployeeID($id)
{		
	$idstring = '"'.$id.'"';
	$sql = "
		SELECT employee_id, employee.firstname AS emp_name
		FROM raters
		LEFT JOIN db_klola_hes.sys_ttrs_employee AS employee ON raters.employee_id =  employee.nip
	 ";
	 return DB::select( DB::raw($sql));
}


function getEmployee($emp_id)
{	
	$query = DB::table('db_klola_hes.sys_ttrs_employee AS employee')
	->select(
	'employee.firstname AS emp_name','employee.nip AS  emp_nik','employee.image AS avatar',
	'occupation.alias AS emp_occupation')
	->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'employee.sys_tmst_occupation_id')
	->whereIn('employee.nip', $emp_id)
	->get();
	return $query;
}










