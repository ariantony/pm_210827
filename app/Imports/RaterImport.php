<?php

namespace App\Imports;


use App\Models\Rater;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\HeadingRowImport;

class RaterImport implements ToCollection
{
    public function __construct()
    {

    }

    public function headingRow(): int
    {
        return 1;
    }
    

    public function collection(Collection $rows)
    {   
        $heading = $rows[0];
        unset($rows[0]) ;
        
        $data = [];
        foreach ($rows as $val => $row) {
            $data[$val]= $row;
        }

        $rate_parent = [];

        for($i=1;$i<=count($data);$i++){
            $rate = [];
            for ($d=2;$d<count($data[$i]);$d++){
                $rate[] = 
                    array(
                        "key"   => $heading[$d],
                        "value" => $data[$i][$d]
                    );
            }
            $rateJson = json_encode($rate);
            $rate_parent[] = $rateJson;
        }

        foreach ($rows as $val => $row) 
        {   
            $index = $val-1;
            $rate = json_decode($rate_parent[$index]);
            if($rate[0]->value !=null){
                Rater::create([
                    'employee_id'   => $row[2],
                    'rate'          => $rate_parent[$index],
                ]);
            }
           
        }
    }
}
