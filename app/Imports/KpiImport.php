<?php

namespace App\Imports;


use App\Models\PerformanceItem;
use App\Models\Performance;
use App\Models\ObjectiveCategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class KpiImport implements ToCollection, WithHeadingRow
{
    public function __construct($performance_id = null,  $emp_level = null, $year= null)
    {
        $this->performance_id  = $performance_id;
        $this->emp_level       = $emp_level;
        $this->year            = $year;
    } 
    
    public function headingRow(): int
    {
        return 1;
    }

    public function collection(Collection $rows)
    {   
        $staff = explode(",",config('app.staff_id'));

        if (in_array($this->emp_level, $staff)) {
            $statusItemKPI     = 1;
        }else{
            if( $this->year <= 2019){
                $statusItemKPI     = 1;
            }else{
                $statusItemKPI     = 0;
            }
        }

        foreach ($rows as $row) 
        {   
           
            PerformanceItem::create([
                'performance_id' => $this->performance_id,
                'aspect'        => ucwords($row['performa_aspect']),
                'area'          => ucwords($row['performa_area']),
                'kpi'           => preg_replace('/[^a-zA-Z0-9\s]/', '', $row['kpi']),
                'weight'        => $row['weight'],
                'target'        => preg_replace('/[^a-zA-Z0-9\s]/', '', $row['target']),
                'measure'       => preg_replace('/[^a-zA-Z0-9\s]/', '', $row['uom']),
                'notes'         => $row['notes'],
                'status'        => $statusItemKPI,
            ]);
        }
    }
}
