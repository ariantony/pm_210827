<?php

namespace App\Imports;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class PerformanceImport implements WithMultipleSheets
{
    public function __construct($performance_id = null, $emp_nik = null,$emp_level = null,$year = null,$model = null)
    {
        $this->performance_id  = $performance_id;
        $this->emp_nik         = $emp_nik;
        $this->emp_level       = $emp_level;
        $this->year            = $year;
        $this->model           = $model;
    } 

    public function sheets(): array
    {   
        if( $this->model == 'kpi'){
            return [
                'KPI'  => new KpiImport($this->performance_id,$this->emp_level, $this->year),
           ];
        }else{
            return [
                'KPI'  => new KpiImport($this->performance_id,$this->emp_level, $this->year),
                'PA'   => new AppraisalImport($this->performance_id, $this->emp_nik),
           ];
        }
    }
}
