<?php

namespace App\Imports;


use App\Models\PerformanceItem;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon\Carbon;

class MyPerformanceImport implements ToCollection, WithHeadingRow
{
    public function __construct($performance_id = null, $emp_nik = null, $year= null)
    {
        $this->performance_id  = $performance_id;
        $this->emp_nik         = $emp_nik;
        $this->year            = $year;
    } 
    
    public function headingRow(): int
    {
        return 1;
    }

    public function collection(Collection $rows)
    {   
        $staff = explode(",",config('app.staff_id'));

        if (in_array(Session::get('emp_level_id'), $staff)) {
            $statusItemKPI     = 1;
        }else{
            if( $this->year <= 2019){
                $statusItemKPI     = 1;
            }else{
                $statusItemKPI     = 0;
            }
        }

        
        foreach ($rows as $row) 
        {   
            PerformanceItem::create([
                'performance_id' => $this->performance_id,
                'aspect'        => ucwords($row['performa_aspect']),
                'area'          => ucwords($row['performa_area']),
                'kpi'           => $row['kpi'],
                'weight'        => $row['weight'],
                'target'        => $row['target'],
                'measure'       => $row['uom'],
                'notes'         => $row['notes'],
                'status'        => $statusItemKPI,
            ]);
        }

    }
}
