<?php

namespace App\Imports;


use App\Models\PerformanceAppraisal;
use App\Models\Performance;
use App\Models\Assessment;
use App\Models\Rater;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Carbon\Carbon;

class AppraisalImport implements  ToCollection, WithHeadingRow, WithCalculatedFormulas
{
    public function __construct($performance_id = null, $emp_nik = null)
    {
        $this->performance_id  = $performance_id;
        $this->emp_nik         = $emp_nik;
    } 

    public function headingRow(): int
    {
        return 1;
    }
    

    public function collection(Collection $rows)
    {   
        
        $rater   =  Rater::where('employee_id',$this->emp_nik)->first();
        $raterID = json_decode($rater->rate,true);
        $totalScore = 0;

        foreach ($rows as $row) 
        {   

            $rate = [];
            foreach ($raterID as $item){
                $group = $item['key'];
                if ($group != 'null') {
                    $rate[] =
                    array(
                        "key"   => $item['key'],
                        "nik"   => $item['value'],
                        "value" => $row[$group]
                    );
                }
            }

            $rateJson = json_encode($rate);

            PerformanceAppraisal::create([
                'performance_id'=> $this->performance_id,
                'rate'          => $rateJson,
                'comment_all'   => $row['comment'],
                'competency_id' => $row['no']
            ]);
           
        }

        $dataRate = [];

        foreach ($raterID as $val ){
            $key = explode('_',$val['key']);
            if($val['value'] != NULL){
                $dataRate[] = [
                    'performance_id'=> $this->performance_id,
                    'group_rate'    => $key['0'],
                    'rater_id'      => $val['value'],
                    'status'        => 0,
                    'created_at'    => Carbon::now()->format('Y/m/d H:i:s'),
                ];
            }
        }
        Assessment::insert($dataRate);
    }
}
