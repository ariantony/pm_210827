<?php

namespace App\Imports;

use App\User;
use App\Employee;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class EmployeeImport implements ToCollection
{
    public function collection(Collection $rows)
    {   
        $id = User::latest('id')->first();
        $uuid = $id + 1;
        foreach ($rows as $row) 
        {
            EmployeeType::create([
                'user_id'       => $uuid,
                'name'          => $row[1],
            ]);

            User::create([
                'id'            => $uuid,
                'name'          => $row[4],
                'email'         => $row[5],
                'password'      => Hash::make('123456'),
            ]);
        }
    }
}
