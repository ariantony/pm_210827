<?php

namespace App\Imports;


use App\Models\Performance;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon\Carbon;

class AdjustedImport implements ToCollection, WithHeadingRow
{
    public function __construct( $year= null)
    {
        $this->year            = $year;
    } 
    
    public function headingRow(): int
    {
        return 1;
    }

    public function collection(Collection $rows)
    {   

        foreach ($rows as $row) 
        {   
           $pm = Performance::where('emp_id', $row['nip'])->where('year',$this->year);

           $data['score_adjustment_1']   = $row['adjusted_1'];
           $data['score_adjustment_2']   = $row['adjusted_2'];
           $data['status']   = 3;
           $data['score_adjustment_1_date']   =  Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['date_adjusted_1']));
           $data['score_adjustment_2_date']   =  Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['date_adjusted_2']));

           $pm->update($data);
        }
    }
}
