<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Dashboard extends Model
{
  
    public static function getTopPA(){
        $year = date('Y');
        $sql = "SELECT t1.firstname AS emp_name, t1.image AS emp_avatar, department.name AS emp_department,
        t2.score
        FROM db_klola_hes.sys_ttrs_employee t1
        LEFT OUTER JOIN 
            (SELECT performances.emp_id, IFNULL(SUM(performance_items.yearend_score),0) AS score
            FROM performances  
            LEFT JOIN performance_items ON performances.id = performance_items.performance_id
            WHERE performances.year = $year
            GROUP BY performances.emp_id
            ) t2
        ON t1.nip = t2.emp_id
        LEFT JOIN db_klola_hes.sys_tmst_department AS department ON department.id = t1.sys_tmst_department_id
        ORDER BY t2.score ASC
        LIMIT 5
        ";

        return DB::select( DB::raw($sql));
    }



    
    public static function getAvgPaByDepartment($year)
    {  
        $sql="SELECT SUBSTRING(db_klola_hes.sys_tmst_department.departement_id,1,2) AS department_id, IFNULL(SUM(performance_items.yearend_score),0)  AS score, COUNT(performances.emp_id) AS employee_count
        FROM db_klola_hes.sys_ttrs_employee AS employee
        LEFT JOIN performances ON performances.emp_id = employee.nip
        LEFT JOIN performance_items ON performances.id = performance_items.performance_id
        LEFT JOIN db_klola_hes.sys_tmst_department ON db_klola_hes.sys_tmst_department.id = employee.sys_tmst_department_id
        WHERE performances.year = $year
        GROUP BY db_klola_hes.sys_tmst_department.departement_id";

        return DB::select( DB::raw($sql));
    }

    public static function getPmByStatus($year)
    {  
        $sql = "
        SELECT
            COUNT(case when status = 0 then 1 end) as draft,
            COUNT(case when status = 1 then 1 end) as waiting,
            COUNT(case when status = 2 then 1 end) as process,
            COUNT(case when status = 3 then 1 end) as done
        FROM performances
        WHERE performances.year = $year
        ";
        return DB::select( DB::raw($sql));
    }




    

}
