<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class PerformanceItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'performance_items';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['performance_id','aspect','category_id','area', 'kpi','weight','target','target_revision','status','midyear_achievement','yearend_achievement','measure','notes','created_at'];


    public static function getPerformanceItem($id){

        $query = DB::table('performance_items')
          ->select('performance_items.*','measures.title AS measureTitle','performance_items.aspect AS category')
          ->leftJoin('measures', 'measures.id', '=', 'performance_items.measure_id')
          ->where('performance_items.performance_id', $id)
          ->get();
        return $query;
  
     }

}
