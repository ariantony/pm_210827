<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupRater extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'group_raters';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'weight','updated_by','created_by','created_at','status','raters'];


}
