<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Performance extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'performances';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['competency_weight','rater_weight','kpi_weight','emp_id','period_start','period_end', 'year','is_score','created_at','status','score_adjustment_1','score_adjustment_2','score_adjustment_1_date','score_adjustment_2_date','is_achievement_midyear','is_achievement_yearend','company_id','unit_id','department_id','level_id','occupation_id'];

    public static function cekData($period_start, $period_end, $year, $emp_id = null, $id = null){
        $query = DB::table('performances')
          ->select('*')
          ->where('year', $year)
          ->where('period_start', $period_start)
          ->where('period_end', $period_end)
          ->when(!empty($emp_id), function ($query) use ($emp_id) {
            $query->where('emp_id',$emp_id);
          })
          ->when(!empty($id), function ($query) use ($id) {
              $query->where('id','!=',$id);
          })
          ->first();
        return $query;
    }

    public static function getData($year){

        $query = DB::table('performances')
        ->select('performances.*',
        'employee.firstname AS nama','employee.nip AS nik',
        'occupation.alias AS jabatan', 
        'department.name AS department', 
        'level.name AS level', 
        'group.name AS company', 
        'unit.name AS workarea')
        ->leftjoin('db_klola_hes.sys_ttrs_employee AS employee', 'employee.nip', '=', 'performances.emp_id')
        ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'performances.department_id')
        ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'performances.occupation_id')
        ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'performances.unit_id')
        ->leftjoin('db_klola_hes.sys_tmst_level AS level', 'level.id', '=', 'performances.level_id')
        ->leftjoin('db_klola_hes.sys_ttrs_employee_group AS group', 'group.id', '=', 'performances.company_id')
        ->where('performances.year',$year)
        ->where('performances.status','!=',0)
        ->get();
        return $query;
    }


    public static function getPerformanceById($id){

        $query = DB::table('performances')
        ->select('performances.*',
        'employee.firstname AS nama','employee.nip AS nik',
        'occupation.alias AS jabatan', 
        'level.name AS level', 
        'level.id AS level_id', 
        'department.name AS department', 
        'group.name AS company', 
        'unit.name AS workarea')
        ->leftjoin('db_klola_hes.sys_ttrs_employee AS employee', 'employee.nip', '=', 'performances.emp_id')
        ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'performances.department_id')
        ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'performances.occupation_id')
        ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'performances.unit_id')
        ->leftjoin('db_klola_hes.sys_tmst_level AS level', 'level.id', '=', 'performances.level_id')
        ->leftjoin('db_klola_hes.sys_ttrs_employee_group AS group', 'group.id', '=', 'performances.company_id')
        ->where('performances.id',$id)
        ->first();
        return $query;
    }


    public static function getPerformanceByNikYear($empID, $year = null){

        if( $year == null){
            $year = date('Y');
        }else{
            $year = $year;
        }

        $query = DB::table('performances')
        ->select('performances.*',
        'employee.firstname AS nama','employee.nip AS nik',
        'occupation.alias AS jabatan', 
        'level.name AS level', 
        'level.id AS level_id', 
        'department.name AS department', 
        'group.name AS company', 
        'unit.name AS workarea')
        ->leftjoin('db_klola_hes.sys_ttrs_employee AS employee', 'employee.nip', '=', 'performances.emp_id')
        ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'performances.department_id')
        ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'performances.occupation_id')
        ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'performances.unit_id')
        ->leftjoin('db_klola_hes.sys_tmst_level AS level', 'level.id', '=', 'performances.level_id')
        ->leftjoin('db_klola_hes.sys_ttrs_employee_group AS group', 'group.id', '=', 'performances.company_id')
        ->where('performances.emp_id',$empID)
        ->where('performances.year',$year)
        ->first();
        return $query;
    }

    public static function getAppraisalItemById($id){

        $query = DB::table('performance_appraisal')
        ->select('performance_appraisal.*','competencies.name AS competencyName','competencies.description AS competencyDesc')
        ->leftjoin('performances', 'performances.id', '=', 'performance_appraisal.performance_id')
        ->leftjoin('competencies', 'competencies.id', '=', 'performance_appraisal.competency_id')
        ->where('performances.id',$id)
        ->get();
        return $query;
    }
    

    public static function getPerformanceItemById($id,$status = null){

        $query = DB::table('performance_items')
        ->select('performance_items.*','performance_items.aspect AS category','measures.title AS measureTitle')
        ->leftjoin('measures', 'measures.id', '=', 'performance_items.measure_id')
        ->leftjoin('performances', 'performances.id', '=', 'performance_items.performance_id')
        ->where('performances.id',$id)
        ->when(!empty($status), function ($query) use ($status) {
            return $query->where('performance_items.status',$status);
        })
        ->get();
        return $query;

    }


    public static function getRaterByEmployee($emp_id){
        $query = DB::table('raters')
        ->select('raters.*')
        ->where('raters.employee_id', $emp_id)
        ->get();
        return $query;

    }


    public static function getApprovalList($id){

        $query = DB::table('performances')
            ->select(
            'employee.firstname AS emp_name','employee.nip AS  emp_nik','employee.image AS avatar',
            'occupation.alias AS emp_occupation', 
            'department.name AS emp_department', 
            'unit.name AS emp_workarea',
            'group.name AS company', 
            'assessments.rater_id',
            'performances.*')
            ->leftJoin('assessments', 'assessments.performance_id', '=', 'performances.id')
            ->leftjoin('db_klola_hes.sys_ttrs_employee AS employee', 'employee.nip', '=', 'performances.emp_id')
            ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'performances.department_id')
            ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'performances.occupation_id')
            ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'performances.unit_id')
            ->leftjoin('db_klola_hes.sys_tmst_level AS level', 'level.id', '=', 'performances.level_id')
            ->leftjoin('db_klola_hes.sys_ttrs_employee_group AS group', 'group.id', '=', 'performances.company_id')
            ->where('performances.status', 1)
            ->where('assessments.rater_id', $id)
            ->where('assessments.group_rate','superior')
            ->get();
        
        return $query;
    }

    public static function getMonitoring($year, $company_id = null,  $workarea_id = null, $department_id = null){

        $where = "";
        if($company_id != 'null'){
            $where .= " AND sys_ttrs_employee.sys_ttrs_employee_group_id = $company_id";
        }
        if($workarea_id != 'null'){
            $where .= " AND sys_ttrs_employee.sys_tmst_unit_id = $workarea_id";
        }
        if($department_id  != 'null'){
            $where .= " AND sys_ttrs_employee.sys_tmst_department_id = $department_id";
        }

        $sql = "
            SELECT sys_ttrs_employee.nip AS emp_nik, sys_ttrs_employee.firstname AS emp_name, sys_tmst_level.name as level,  sys_tmst_department.name as department,
            sys_tmst_unit.name AS workarea, sys_ttrs_employee_group.name AS company
            FROM raters t1 
            LEFT JOIN db_klola_hes.sys_ttrs_employee ON t1.employee_id = sys_ttrs_employee.nip
            LEFT JOIN db_klola_hes.sys_tmst_department ON sys_ttrs_employee.sys_tmst_department_id = sys_tmst_department.id
            LEFT JOIN db_klola_hes.sys_tmst_unit ON sys_ttrs_employee.sys_tmst_unit_id = sys_tmst_unit.id
            LEFT JOIN db_klola_hes.sys_tmst_level ON sys_ttrs_employee.sys_tmst_level_id = sys_tmst_level.id
            LEFT JOIN db_klola_hes.sys_ttrs_employee_group ON sys_ttrs_employee.sys_ttrs_employee_group_id = sys_ttrs_employee_group.id
            WHERE NOT EXISTS (SELECT * FROM performances WHERE t1.employee_id = performances.emp_id AND performances.year = $year AND performances.status != 0) 
            ".$where."
            ORDER BY sys_ttrs_employee.firstname ASC
        ";

        return DB::select( DB::raw($sql));
       
    }

}
