<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Laravel\Scout\Searchable;

class Employee extends Model
{
  use Searchable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'db_klola_hes.sys_ttrs_employee';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'uuid';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

      /*
    |------------------------------------------------------------------------------------
    | Attributes
    |------------------------------------------------------------------------------------
    */

    public function searchableAs()
    {
        return 'employees';
    }


    public static function login($attemps){

      $query = DB::table('db_klola_hes.sys_ttrs_employee AS employee')
      ->select('employee.*', 
        'occupation.alias AS jabatan', 
        'department.name AS department', 
        'level.name AS level', 
        'unit.name AS workarea',
        'group.name AS company')
      ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'employee.sys_tmst_department_id')
      ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'employee.sys_tmst_occupation_id')
      ->leftjoin('db_klola_hes.sys_tmst_level AS level', 'level.id', '=', 'employee.sys_tmst_level_id')
      ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'employee.sys_tmst_unit_id')
      ->leftjoin('db_klola_hes.sys_ttrs_employee_group AS group', 'group.id', '=', 'employee.sys_ttrs_employee_group_id')
      ->where($attemps)
      ->get();
      return $query;
    }


    public static function getById($id){

      $query = DB::table('db_klola_hes.sys_ttrs_employee AS employee')
      ->select('employee.*', 
        'occupation.alias AS jabatan', 
        'department.name AS department', 
        'level.name AS level', 
        'unit.name AS workarea',
        'group.name AS company')
      ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'employee.sys_tmst_department_id')
      ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'employee.sys_tmst_occupation_id')
      ->leftjoin('db_klola_hes.sys_tmst_level AS level', 'level.id', '=', 'employee.sys_tmst_level_id')
      ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'employee.sys_tmst_unit_id')
      ->leftjoin('db_klola_hes.sys_ttrs_employee_group AS group', 'group.id', '=', 'employee.sys_ttrs_employee_group_id')
      ->where('employee.nip',$id)
      ->first();
      return $query;


    }
    
    
}
