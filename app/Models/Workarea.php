<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workarea extends Model
{
    // protected $connection = 'mysql2';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'db_klola_hes.sys_tmst_unit';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'unit_id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

      /*
    |------------------------------------------------------------------------------------
    | Attributes
    |------------------------------------------------------------------------------------
    */

    
}
