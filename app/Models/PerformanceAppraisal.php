<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class PerformanceAppraisal extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'performance_appraisal';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['performance_id','rate','rate_yearend','comment_yearend','comment_midyear','total_score','comment_all','competency_id','created_at'];


    public static function getAppraisalItem($id){

        $query = DB::table('performance_appraisal')
          ->select('performance_appraisal.*','competency.name AS competencyName','competency.description AS competencyDesc')
          ->leftjoin('competencies', 'competencies.id', '=', 'performance_appraisal.competency_id')
          ->where('performance_appraisal.appraisal_id', $id)
          ->get();
        return $query;
  
     }

}
