<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Assessment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'assessments';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['rater_id','group_rate','performance_id','created_at','status','date_yearend','date_midyear'];

    
    public static function getMonitoring($year, $company_id = null, $department_id = null, $workarea_id = null, $rater_id = null){

        $query = DB::table('assessments')
            ->select('assessments.*',
            'rater.firstname AS rater_name',
            'employee.firstname AS emp_name','employee.nip AS emp_nik',
            'occupation.alias AS emp_occupation', 
            'department.name AS emp_department', 
            'unit.name AS emp_workarea',
            'level.name AS emp_level',
            'group.name AS company', 
            'performances.id AS pm_id','performances.year','performances.period_start','performances.period_end','performances.is_score','performances.is_achievement_midyear','performances.is_achievement_yearend')
            ->leftJoin('performances', 'assessments.performance_id', '=', 'performances.id')
            ->leftjoin('db_klola_hes.sys_ttrs_employee AS rater', 'rater.nip', '=', 'assessments.rater_id')
            ->leftjoin('db_klola_hes.sys_ttrs_employee AS employee', 'employee.nip', '=', 'performances.emp_id')
            ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'rater.sys_tmst_department_id')
            ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'rater.sys_tmst_occupation_id')
            ->leftjoin('db_klola_hes.sys_tmst_level AS level', 'level.id', '=', 'employee.sys_tmst_level_id')
            ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'rater.sys_tmst_unit_id')
            ->leftjoin('db_klola_hes.sys_ttrs_employee_group AS group', 'group.id', '=', 'rater.sys_ttrs_employee_group_id')
            ->when(!empty($company_id), function ($query) use ($company_id) {
                return $query->where('rater.sys_ttrs_employee_group_id',$company_id);
            })
            ->when(!empty($department_id), function ($query) use ($department_id) {
                return $query->where('rater.sys_tmst_department_id',$department_id);
            })
            ->when(!empty($workarea_id), function ($query) use ($workarea_id) {
                return $query->where('rater.sys_tmst_unit_id',$workarea_id);
            })
            ->when(!empty($rater_id), function ($query) use ($rater_id) {
                return $query->where('assessments.rater_id',$rater_id);
            })
            ->whereIn('assessments.status', ['0','1'])
            ->where('performances.status', 2)
            ->whereIn('employee.employee_status',['1','2','3'])
            ->where('employee.sys_tmst_employee_type_id',1)
            ->where('performances.year', $year)
            ->get();

        $data = [];
        foreach ($query as $item) {
            $data[$item->rater_id][] =$item;
        }
       
        return $data;
    }


    public static function getAssessmentList($id){

        $query = DB::table('assessments')
            ->select('assessments.*',
            'group_raters.name AS group',
            'employee.firstname AS emp_name','employee.nip AS  emp_nik','employee.image AS avatar',
            'occupation.alias AS emp_occupation', 
            'department.name AS emp_department', 
            'level.name AS level', 
            'level.id AS emp_level_id', 
            'unit.name AS emp_workarea',
            'performances.id AS pm_id','performances.level_id AS level_id','performances.year','performances.period_start','performances.period_end','performances.is_score','performances.is_achievement_midyear','performances.is_achievement_yearend')
            ->leftJoin('performances', 'assessments.performance_id', '=', 'performances.id')
            ->leftJoin('group_raters', 'group_raters.code', '=', 'assessments.group_rate')
            ->leftjoin('db_klola_hes.sys_ttrs_employee AS employee', 'employee.nip', '=', 'performances.emp_id')
            ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'performances.department_id')
            ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'performances.occupation_id')
            ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'performances.unit_id')
            ->leftjoin('db_klola_hes.sys_tmst_level AS level', 'level.id', '=', 'performances.level_id')
            ->leftjoin('db_klola_hes.sys_ttrs_employee_group AS group', 'group.id', '=', 'performances.company_id')
            ->where('performances.status', 2)
            ->where('assessments.rater_id', $id)
            ->whereIn('assessments.status', ['0','1'])
            ->get();
        $data = [];
        foreach ($query as $item){
            if(($item->is_score == 1 && $item->status == 0) || ($item->is_score == 2 && $item->status == 1) ){
                $data[]=$item;
            }
        }
        return $data;
    }


    public static function getPerformance($id){
        $query = DB::table('assessments')
            ->select('assessments.*','group_raters.name AS group','group_raters.weight AS groupWeight', 'assessments.group_rate AS group_rater',
            'performances.id AS pm_id','performances.year','performances.period_start','performances.period_end','performances.is_score','performances.id AS pm_id','performances.year','performances.period_start','performances.period_end',
            'performances.is_score', 'performances.rater_weight', 'performances.kpi_weight', 'performances.competency_weight',
            'employee.firstname AS emp_name','employee.nip AS  emp_nik',
            'occupation.alias AS emp_occupation', 
            'group.name AS emp_company', 
            'level.name AS emp_level', 
            'level.id AS emp_level_id', 
            'department.name AS emp_department', 
            'unit.name AS emp_workarea')
            ->leftjoin('performances', 'assessments.performance_id', '=', 'performances.id')
            ->leftjoin('db_klola_hes.sys_ttrs_employee AS employee', 'employee.nip', '=', 'performances.emp_id')
            ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'performances.department_id')
            ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'performances.occupation_id')
            ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'performances.unit_id')
            ->leftjoin('db_klola_hes.sys_tmst_level AS level', 'level.id', '=', 'performances.level_id')
            ->leftjoin('db_klola_hes.sys_ttrs_employee_group AS group', 'group.id', '=', 'performances.company_id')
            ->leftjoin('group_raters', 'group_raters.code', '=', 'assessments.group_rate')
            ->where('assessments.id', $id)
            ->first();
        return $query;
    }


    public static function getAssessmentDetail($pm_id,$rater_id){
        $query = DB::table('assessments')
            ->select('assessments.*','assessments.group_rate','assessments.final_rate','group_raters.name AS group','group_raters.weight AS group_weight','sys_ttrs_employee.nama','performances.year','performances.id AS pm_id','performances.period_start','performances.period_end',
            'employee.firstname AS emp_name','employee.nip AS  emp_nik',
            'group.name AS emp_company', 
            'occupation.alias AS emp_occupation', 
            'level.name AS level', 
            'level.id AS emp_level_id', 
            'department.name AS emp_department', 
            'unit.name AS emp_workarea')
            ->leftjoin('performances', 'assessments.performance_id', '=', 'performances.id')
            ->leftjoin('group_raters', 'group_raters.code', '=', 'assessments.group_rate')
            ->leftjoin('db_klola_hes.sys_ttrs_employee AS employee', 'employee.nip', '=', 'performances.emp_id')
            ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'performances.department_id')
            ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'performances.occupation_id')
            ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'performances.unit_id')
            ->leftjoin('db_klola_hes.sys_tmst_level AS level', 'level.id', '=', 'performances.level_id')
            ->leftjoin('db_klola_hes.sys_ttrs_employee_group AS group', 'group.id', '=', 'performances.company_id')
            ->where('assessments.rater_id', $rater_id)
            ->where('assessments.performance_id', $pm_id)
            ->first();
        return $query;
    }

    public static function getAssessmentByPmId($pm_id){
        $query = DB::table('assessments')
            ->select('assessments.*',
            'employee.firstname AS emp_name','employee.nip AS emp_nik',
            'group.name AS emp_company', 
            'occupation.alias AS emp_occupation', 
            'level.name AS level', 
            'department.name AS emp_department', 
            'level.id AS emp_level_id', 
            'unit.name AS emp_workarea')
            ->leftjoin('performances', 'assessments.performance_id', '=', 'performances.id')
            ->leftjoin('db_klola_hes.sys_ttrs_employee AS employee', 'employee.nip', '=', 'assessments.rater_id')
            ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'performances.department_id')
            ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'performances.occupation_id')
            ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'performances.unit_id')
            ->leftjoin('db_klola_hes.sys_tmst_level AS level', 'level.id', '=', 'performances.level_id')
            ->leftjoin('db_klola_hes.sys_ttrs_employee_group AS group', 'group.id', '=', 'performances.company_id')
            ->where('assessments.performance_id', $pm_id)
            ->orderBy('assessments.id','ASC')
            ->get();
        return $query;
    }


    public static function getAssessmentHistory($id){
        $query = DB::table('performances')
            ->select('assessments.*','group_raters.name AS group','performances.id AS pm_id','performances.year','performances.period_start','performances.period_end',
            'employee.firstname AS emp_name','employee.nip AS  emp_nik',
            'group.name AS emp_company', 
            'occupation.alias AS emp_occupation', 
            'level.name AS level', 
            'department.name AS emp_department', 
            'unit.name AS emp_workarea')
            ->leftjoin('assessments', 'assessments.performance_id', '=', 'performances.id')
            ->leftjoin('group_raters', 'group_raters.code', '=', 'assessments.group_rate')
            ->leftjoin('db_klola_hes.sys_ttrs_employee AS employee', 'employee.nip', '=', 'performances.emp_id')
            ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'performances.department_id')
            ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'performances.occupation_id')
            ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'performances.unit_id')
            ->leftjoin('db_klola_hes.sys_tmst_level AS level', 'level.id', '=', 'performances.level_id')
            ->leftjoin('db_klola_hes.sys_ttrs_employee_group AS group', 'group.id', '=', 'performances.company_id')
            ->where('assessments.rater_id', $id)
            ->where(function($query)
            {
                $query->where('assessments.status','!=', 0);
            })
            
            ->get();
        return $query;
    }


}
