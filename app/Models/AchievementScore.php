<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AchievementScore extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'achievement_scores';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','description','score','created_by','created_at','status'];


}
