<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Vinkla\Hashids\Facades\Hashids;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use Auth;

class AnnouncementController extends Controller
{

    public function read()
    {
        $announcement = Announcement::where('status',1)->paginate(15);
        return view('personal.dashboard.announcement', compact('announcement'));
    }

    public function search(Request $request){
        $announcement = Announcement::when($request->q, function ($query) use ($request) {
                  $query->where('title', 'ILIKE', "%{$request->q}%")
                        ->orWhere('content', 'ILIKE', "%{$request->q}%");
                  })->paginate(8);
        return view('personal.dashboard.announcement', compact('announcement'));
     }

}
