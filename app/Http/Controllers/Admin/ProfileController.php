<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Hash;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Traits\UploadTrait;
use Vinkla\Hashids\Facades\Hashids;

class ProfileController extends Controller
{
    use UploadTrait;
  
    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {   
        if ($request->isMethod('GET')) {
            return view('admin.profile.changePassword');
        } else {
            if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
                // The passwords matches
                return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
            }
            if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
                //Current password and new password are same
                return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
            }
            $validatedData = $request->validate([
                'current-password' => 'required',
                'new-password' => 'required|string|min:6|confirmed',
            ]);
            //Change Password
            $user = Auth::user();
            $user->password = $request->get('new-password');
            $user->save();
            return redirect()->back()->with("success","Password changed successfully !");
    
        }
    }

    public function changeProfile(Request $request)
    {   
        if ($request->isMethod('GET')) {

            $users = DB::table('users')
            ->select('users.photo','users.email','users.name as username')
            ->where('users.id', Auth::user()->id)
            ->first();
    
            return view('admin.profile.changeProfile', compact('users'));
        } else {
            $validatedData = $request->validate([
                'email' => 'required'
            ]);
            //Change Password
            $user = Auth::user();
            $user->name     = $request->get('name');
            $user->email    = $request->get('email');
            if ($request->has('photo')) {
                $image = $request->file('photo');
                $name = Str::slug($request->input('name')).'_'.time();
                $folder = '/uploads/images/';
                $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
                $this->uploadOne($image, $folder, 'public', $name);
                $user->photo = $filePath;
            }
            // Persist user record to database
            $user->save();
            return redirect()->back()->with("success","Profile changed successfully !");
    
        }
    }


}
