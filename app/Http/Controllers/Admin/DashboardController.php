<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Announcement;
use App\Models\Assessment;
use App\Models\Performance;
use App\Models\Department;
use App\Models\Dashboard;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index($year = null)
    {
        if($year == null){
            $year = date('Y');
        }else{
            $year = $year;
        }
        
        $emp_count = Employee::count();
        $topPA     = Dashboard::getTopPA();
        $announcement   = Announcement::latest()->first();
        $score_dept = Dashboard::getAvgPaByDepartment($year);
        $assessment = Assessment::getMonitoring($year);
        $performance = Performance::getMonitoring($year,'null','null','null');
        $performanceStatus = Dashboard::getPmByStatus($year);

        $department = DB::table('db_klola_hes.sys_tmst_department')->select('name','departement_id')->where('parent', 'root')->get();

        $stats_data = [];
        foreach($score_dept  as $val){
            $stats_data[$val->department_id][]=$val;
        }

        $stats =[];
        foreach($department  as $val){
            if (array_key_exists($val->departement_id,$stats_data))
            {
                $score = $employee = 0;

                foreach ($stats_data[$val->departement_id] as $element){
                    $score +=  $element->score;
                    $employee +=  $element->employee_count;
                }

                $avg_score = $score/$employee;
                if($avg_score !=0){
                    $stats[]=[
                        'name' => $val->name,
                        'score' => $avg_score
                    ];
                }
              
            }
           
        }


        return view('admin.dashboard.index',compact('emp_count','topPA','announcement','stats','assessment','year','performance','performanceStatus'));
    }
}
