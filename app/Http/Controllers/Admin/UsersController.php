<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Employee;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Vinkla\Hashids\Facades\Hashids;

class UsersController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('users_management')) {
            return abort(401);
        }

        $users = DB::table('users')
            ->paginate(30);;

        return view('admin.users.index', compact('users'));
    }


    public function datatables()
    {
        if (! Gate::allows('users_management')) {
            return abort(401);
        }
        $result = DB::table('users');

       return  DataTables::of($result)
        ->addColumn('action', function ($result) {
            $url_edit = "<a href='".route('admin.users.edit', Hashids::encode($result->id))."' title='".trans('app.edit_title')."' data-toggle='tooltip' class='btn btn-outline'><span class='ti-pencil icon-lg'></span> </a>";  
            $url_delete = "<form class='delete' action='".route('admin.users.delete', ['id' => Hashids::encode($result->id)])."' method='POST'>
                                ".csrf_field()."
                                <button class='btn btn-outline text-danger' title='".trans('app.delete_title')."' data-toggle='tooltip'><i class='ti-trash icon-lg'></i></button>
                            </form>";
            return
                '<div class="btn-group">'
                 .$url_edit .$url_delete.
                '</div>';
        }) 
        ->editColumn('updated_at', function ($result) {
            return $result->updated_at ? with(new Carbon($result->updated_at))->format('m/d/Y') : '';
        })
        ->make(true);

    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('users_management')) {
            return abort(401);
        }
        $roles = Role::get()->pluck('name', 'name');

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsersRequest $request)
    {
        if (! Gate::allows('users_management')) {
            return abort(401);
        }

        $data = $request->all();

        if ($request->has('photo')) {
            $image = $request->file('photo');
            $name = str_slug($request->get('name')).'_'.time();
            $folder = '/uploads/images/';
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            $this->uploadOne($image, $folder, 'public', $name);
            $data['photo'] = $filePath;
        }
        $user = User::create($data);

        if($request->get('type')==4){
            $employeeData = [
                'user_id'       => $user->id,
                'name'          => $request->get('name'),
            ];

            $employeeID = Employee::insertGetId($employeeData);

            $employeeDetails = [
                'employee_id' => $employeeID
            ];
            DB::table('employee_details')->insert($employeeDetails);
        }
        
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);

        return redirect()->route('admin.users.index');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('users_management')) {
            return abort(401);
        }
        $id     = Hashids::decode($id);
        $roles  = Role::get()->pluck('name', 'id');
        $user   = User::findOrFail($id['0']);
        
        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateUsersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsersRequest $request, $id)
    {
        if (! Gate::allows('users_management')) {
            return abort(401);
        }
        $user = User::findOrFail($id);
        $user->update($request->all());
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->syncRoles($roles);

        return redirect()->route('admin.users.index');
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('users_management')) {
            return abort(401);
        }
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('admin.users.index');
    }

    public function delete(Request $request)
    {

        if (! Gate::allows('master_manage')) {
            return abort(401);
        }

        $id     = Hashids::decode($request->id);

        $user  = User::findOrFail($id['0']);
        $user->delete();

        DB::table('model_has_roles')->where('model_id', $id['0'])->delete();

      
        return redirect()->route('admin.users.index')->with(['success' => 'Delete was successful!']);

    }

    public function AuthRouteAPI(Request $request){
        return $request->user();
     }
}
