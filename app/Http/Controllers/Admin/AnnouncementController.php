<?php

namespace App\Http\Controllers\Admin;

use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Vinkla\Hashids\Facades\Hashids;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use Auth;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of Items.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('announcement')) {
            return abort(401);
        }
        return view('admin.announcement.index');
    }

    public function datatables()
    {
        if (! Gate::allows('announcement')) {
            return abort(401);
        }
        $result = DB::table('announcements');

       return  DataTables::of($result)
        ->addColumn('action', function ($result) {
            $url_edit = "<a href='".route('admin.announcement.edit', Hashids::encode($result->id))."' title='".trans('app.edit_title')."' data-toggle='tooltip' class='btn btn-outline'><span class='ti-pencil icon-lg'></span> </a>";  
            $url_delete = "<form class='delete' action='".route('admin.announcement.delete', ['id' => $result->id])."' method='POST'>
                                 ".csrf_field()."
                                <button type='submit' class='btn btn-outline text-danger' title='".trans('app.delete_title')."' data-toggle='tooltip'><i class='ti-trash icon-lg'></i></button>
                            </form>";
            return
                '<div class="btn-group">'
                 .$url_edit .$url_delete.
                '</div>';
        })->addColumn('status', function ($result){
            if($result->status==1){
                return "<span class='badge badge-success'>Aktif</span>";
            }else{
                return "<span class='badge badge-danger'>Non Aktif</span>";
            }
        })
        ->editColumn('updated_at', function ($result) {
            return $result->updated_at ? with(new Carbon($result->updated_at))->format('m/d/Y') : '';
        })
        ->rawColumns(['action', 'status'])
        ->make(true);

    }

    /**
     * Show the form for creating new Items.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('announcement')) {
            return abort(401);
        }

        return view('admin.announcement.create');
    }

    /**
     * Store a newly created Items in storage.
     *
     * @param  \App\Http\Requests\StoreItemssRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('announcement')) {
            return abort(401);
        }
        $data = $request->all();
        $data['expired']    = dateNumID2MySQL($request->get('expired'));
        $data['created_by'] = Auth::user()->id;

        $items = Announcement::create($data);

        return redirect()->route('admin.announcement.index')->with(['success' => 'Add was successful!']);
    }


    /**
     * Show the form for editing Items.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('announcement')) {
            return abort(401);
        }
        $id     = Hashids::decode($id);
        $announcement = Announcement::findOrFail($id['0']);

        return view('admin.announcement.edit', compact('announcement'));
    }
    

    /**
     * Update Items in storage.
     *
     * @param  \App\Http\Requests\  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('announcement')) {
            return abort(401);
        }
        
        $data = $request->all();
        if($request->get('status')){
            $data['status'] = 1;
        }else{
            $data['status'] = 0;
        }
        $data['expired']    = dateNumID2MySQL($request->get('expired'));
        $data['updated_by'] = Auth::user()->id;
        $items = Announcement::findOrFail($id);
        $items->update($data);

        return redirect()->route('admin.announcement.index')->with(['success' => 'Edit was successful!']);
        
    }

    /**
     * Remove Items from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function delete(Request $request)
    {

        if (! Gate::allows('announcement')) {
            return abort(401);
        }
        $items  = Announcement::findOrFail($request->id);
        $items->delete();
        return redirect()->route('admin.announcement.index')->with(['success' => 'Delete was successful!']);

    }

    public function read()
    {
        $announcement = Announcement::where('status',1)->paginate(15);
        return view('admin.announcement.read', compact('announcement'));
    }

    public function search(Request $request){
        $announcement = Announcement::when($request->q, function ($query) use ($request) {
                  $query->where('title', 'ILIKE', "%{$request->q}%")
                        ->orWhere('content', 'ILIKE', "%{$request->q}%");
                  })->paginate(8);
        return view('admin.announcement.read', compact('announcement'));
     }

}
