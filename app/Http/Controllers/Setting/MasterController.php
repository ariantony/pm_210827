<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Models\Division;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Workarea;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MasterController extends Controller
{
   
    
    public function getEmployee(Request $request)
    {
        if ($request->has('q')) {
            $data = 
            DB::table('db_klola_hes.sys_ttrs_employee AS employee')
            ->select('employee.*', 'department.name AS department','unit.name AS workarea')
            ->where('employee.firstname', 'like', '%' .$request->q . '%')
            ->orWhere('employee.nip', 'like', '%' .$request->q . '%')
            ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'employee.sys_tmst_department_id')
            ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'employee.sys_tmst_unit_id')
            ->get();
            $result = array();    
            foreach ($data as $val) {
                $result[] = array('id' => $val->id,  'nik' => $val->nip, 'name' => $val->firstname, 'department' => $val->department,'workarea' => $val->workarea);
            }
            return response()->json($result);
        }
    }

    public function getDepartment($company)
    {
        return Department::pluck('name', 'id');
    }

    public function getWorkarea($company)
    {
        return Workarea::pluck('name', 'id');
    }


}
