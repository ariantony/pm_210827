<?php

namespace App\Http\Controllers\Setting;

use App\Models\Rater;
use App\Models\Employee;
use App\Models\Company;
use App\Models\Department;
use App\Models\Workarea;
use App\Models\GroupRater;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Vinkla\Hashids\Facades\Hashids;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use App\Imports\RaterImport;
use App\Exports\RaterExport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use File;

class RaterController extends Controller
{
    /**
     * Display a listing of Group.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('setting')) {
            return abort(401);
        }
        $department = DB::table('db_klola_hes.sys_tmst_department')->where('parent', 'root')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $workarea   = DB::table('db_klola_hes.sys_tmst_unit')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $company    = DB::table('db_klola_hes.sys_ttrs_employee_group')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $level      = DB::table('db_klola_hes.sys_tmst_level')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');

        return view('setting.raters.index', compact('company','department','workarea','level'));
    }

    public function datatables()
    {
        if (! Gate::allows('setting')) {
            return abort(401);
        }

        $result = DB::table('raters')
        ->select('raters.*',
        'employee.firstname AS nama',
        'employee.nip AS nip',
        'occupation.alias AS jabatan', 
        'department.name AS department', 
        'unit.name AS workarea')
        ->leftjoin('db_klola_hes.sys_ttrs_employee AS employee', 'employee.nip', '=', 'raters.employee_id')
        ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.departement_id', '=', 'employee.sys_tmst_department_id')
        ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'employee.sys_tmst_occupation_id')
        ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'employee.sys_tmst_unit_id');

       return  DataTables::of($result)
        ->addColumn('action', function ($result) {
            $url_config = "<a href='".route('setting.rater.config', $result->employee_id )."' title='".trans('app.edit_title')."' data-toggle='tooltip' class='btn btn-outline'><span class='ti-settings icon-lg'></span> </a>";  
            return
                '<div class="btn-group">'
                 .$url_config.
                '</div>';
        }) 
        ->rawColumns(['action'])
        ->make(true);

    }


     /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function config($id)
    {
        if (! Gate::allows('setting')) {
            return abort(401);
        }

        
        $employee    = Employee::getById($id);
        if($employee){
            $group_rater = GroupRater::get();

            $rater = DB::table('raters')
            ->select('raters.*')
            ->where('raters.employee_id', $id)
            ->first();
            return view('setting.raters.config', compact('group_rater','employee','rater'));
        }else{
            return redirect()->route('setting.rater.index')->with(['success' => trans('app.data_not_found')]);
        }
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('setting')) {
            return abort(401);
        }

        $id = $request->get('employee_id');
        
        $employeeRater = Rater::where('employee_id',  $id)->first();

        $raters = [];
        $rater = $request->get('rater_id');

        for($i=0;$i < count($rater);$i++) {
            $raters[] = [
                'key'    => $request->get('group')[$i],
                'value'  => $request->get('rater_id')[$i],
            ];
        }

        $data['rate'] = json_encode($raters);
        $employeeRater->update($data);

        return redirect()->route('setting.rater.index')->with(['success' => 'Config was successful!']);
    }

    public function destroy(Request $request)
    {

        if (! Gate::allows('setting')) {
            return abort(401);
        }
        Rater::truncate();
      
        return redirect()->route('setting.rater.import')->with(['success' => 'Delete was successful!']);

    }

    public function import()
    {
        if (! Gate::allows('setting')) {
            return abort(401);
        }

        return view('setting.raters.import');
    }

    public function importStore(Request $request){
        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required'
        ));
    
        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
                
                $file = $request->file('file'); 
                Excel::import(new RaterImport, $file);

                // try{
                //     Excel::import(new RaterImport, $file);
                // } catch (\Exception $e) {
                //     return Redirect::back()
                //     ->withInput($request->input())  
                //     ->withErrors(['Terdapat Template yang belum sesuai']);
                // }
                return redirect()->route('setting.rater.index')->with(['success' => trans('app.message_success_input')]);
    
            } else {
                return redirect()->route('setting.rater.index')->with(['error' => 'Error inserting the data, please upload a valid xls/csv file !']);
            }
        }
    }

    public function getItem()
    {
        return Rater::get()->pluck('name', 'id');
    }


    public function search(Request $request)
    { 
        if (! Gate::allows('setting')) {
            return abort(401);
        }
        $data = $request->all();
        $search = trans('global.app_search_by').": " ;

        if($request->input('company_id')){
            $search .= " <strong>".trans('pm.company')."</strong>: ".getDataByParam('db_klola_hes.sys_ttrs_employee_group','id',$request->input('company_id'))->name;
            $company = $request->input('company_id');
        }else{
            $company = "";
        }
      
        if($request->input('department_id')){
            $search .= " <strong>".trans('pm.department')."</strong>: ".getDataByParam('db_klola_hes.sys_tmst_department','id',$request->input('department_id'))->name;
            $department = $request->input('department_id');
        }else{
            $department = "";
        }
        if($request->input('workarea_id')){
            $search .= " <strong>".trans('pm.workarea')."</strong>: ".getDataByParam('db_klola_hes.sys_tmst_unit','id',$request->input('workarea_id'))->name;
            $workarea = $request->input('workarea_id');
        }else{
            $workarea = "";
        }

        if($request->input('level_id')){
            $search .= " <strong>".trans('pm.level')."</strong>: ".getDataByParam('db_klola_hes.sys_tmst_level','id',$request->input('level_id'))->name;
            $level = $request->input('level_id');
        }else{
            $level = "";
        }
        
        $query = DB::table('raters')
        ->select('raters.*',
        'employee.firstname AS nama',
        'employee.nip AS nip',
        'occupation.alias AS jabatan', 
        'department.name AS department', 
        'unit.name AS workarea')
        ->leftjoin('db_klola_hes.sys_ttrs_employee AS employee', 'employee.nip', '=', 'raters.employee_id')
        ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.departement_id', '=', 'employee.sys_tmst_department_id')
        ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'employee.sys_tmst_occupation_id')
        ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'employee.sys_tmst_unit_id')
       
        ->when(!empty($data['company_id']), function ($query) use ($data) {
            return $query->where('employee.sys_ttrs_employee_group_id',$data['company_id']);
        })
        ->when(!empty($data['department_id']), function ($query) use ($data) {
            return $query->where('employee.sys_tmst_department_id',$data['department_id']);
        })
        ->when(!empty($data['workarea_id']), function ($query) use ($data) {
            return $query->where('employee.sys_tmst_unit_id',$data['workarea_id']);
        })
        ->when(!empty($data['level_id']), function ($query) use ($data) {
            return $query->where('employee.sys_tmst_level_id',$data['level_id']);
        })
        ->paginate(15);

        $rater = $query->appends(array(
            'company_id'    => $company,
            'workarea_id'   => $workarea,
            'department_id' => $department,
            'level_id'      => $level,
        ));

        $department = DB::table('db_klola_hes.sys_tmst_department')->where('parent', 'root')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $workarea   = DB::table('db_klola_hes.sys_tmst_unit')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $company    = DB::table('db_klola_hes.sys_ttrs_employee_group')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $level      = DB::table('db_klola_hes.sys_tmst_level')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');

        return view('setting.raters.search', compact('rater','department', 'workarea','company','search','level'));
    }

}
