<?php

namespace App\Http\Controllers\Setting;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Vinkla\Hashids\Facades\Hashids;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use Auth;

class GeneralController extends Controller
{
    /**
     * Display a listing of Group.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('setting')) {
            return abort(401);
        }
        return view('setting.general.index');
    }


    /**
     * Update Group in storage.
     *
     * @param  \App\Http\Requests\  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('setting')) {
            return abort(401);
        }
        
        $delete = Setting::truncate();

        $setting    = [];
        $key        = $request->get('key');

        for($i=0;$i < count($key);$i++) {
            $setting[] = [
                'key'    => $request->get('key')[$i],
                'value'  => $request->get('val')[$i],
            ];
        }

        $setting = Setting::insert($setting);


        return redirect()->route('setting.general.index')->with(['success' => 'Edit was successful!']);
        
    }


}
