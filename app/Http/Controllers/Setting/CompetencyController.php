<?php

namespace App\Http\Controllers\Setting;

use App\Models\Competency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Vinkla\Hashids\Facades\Hashids;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use Auth;

class CompetencyController extends Controller
{
    /**
     * Display a listing of Group.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('setting')) {
            return abort(401);
        }
        return view('setting.competency.index');
    }

    public function datatables()
    {
        if (! Gate::allows('setting')) {
            return abort(401);
        }
        $result = DB::table('competencies');

       return  DataTables::of($result)
        ->addColumn('action', function ($result) {
            $url_edit = "<a href='".route('setting.competency.edit', Hashids::encode($result->id))."' title='".trans('app.edit_title')."' data-toggle='tooltip' class='btn btn-outline'><span class='ti-pencil icon-lg'></span> </a>";  
            $url_delete = "<form class='delete' action='".route('setting.competency.delete', ['id' => $result->id])."' method='POST'>
                            ".csrf_field()."
                            <button class='btn btn-outline text-danger' title='".trans('app.delete_title')."' data-toggle='tooltip'><i class='ti-trash icon-lg'></i></button>
                          </form>";
            return
                '<div class="btn-group">'
                 .$url_edit .$url_delete.
                '</div>';
        }) 
        ->editColumn('status', function ($result) {
            return getStatusData($result->status);
        })
        ->editColumn('updated_at', function ($result) {
            return $result->updated_at ? with(new Carbon($result->updated_at))->format('m/d/Y') : '';
        })
        ->rawColumns(['action', 'status'])
        ->make(true);

    }

    /**
     * Show the form for creating new Group.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('setting')) {
            return abort(401);
        }

        return view('setting.competency.create');
    }

    /**
     * Store a newly created Group in storage.
     *
     * @param  \App\Http\Requests\StoreGroupsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('setting')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;

        $competency = Competency::create($data);

        return redirect()->route('setting.competency.index')->with(['success' => 'Add was successful!']);
    }


    /**
     * Show the form for editing Group.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('setting')) {
            return abort(401);
        }
        $id = Hashids::decode($id);
        $competency = Competency::findOrFail($id['0']);

        return view('setting.competency.edit', compact('competency'));
    }

    /**
     * Update Group in storage.
     *
     * @param  \App\Http\Requests\  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('setting')) {
            return abort(401);
        }
        $data = $request->all();
        if($request->get('status')){
            $data['status'] = 1;
        }else{
            $data['status'] = 0;
        }
        $data['updated_by'] = Auth::user()->id;

        $competency = Competency::findOrFail($id);
        $competency->update($data);

        return redirect()->route('setting.competency.index')->with(['success' => 'Edit was successful!']);
        
    }

    /**
     * Remove Group from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        print_r($request->id).exit();
        if (! Gate::allows('setting')) {
            return abort(401);
        }

        $competency  = Competency::findOrFail($id);
        $competency->delete();
      
        return redirect()->route('setting.competency.index')->with(['success' => 'Delete was successful!']);

    }

    public function delete(Request $request)
    {

        if (! Gate::allows('setting')) {
            return abort(401);
        }

        $competency  = Competency::findOrFail($request->id);
        $competency->delete();
      
        return redirect()->route('setting.competency.index')->with(['success' => 'Delete was successful!']);

    }

    public function getItem()
    {
        return Competency::get()->pluck('name', 'id');
    }

}
