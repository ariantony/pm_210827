<?php

namespace App\Http\Controllers;


use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Employee;
use DateTime;

class SigninController extends Controller

{
  
    public function attempt(Request $request)
    {
        $this->validate($request, [
            'nik' => 'required',
            'password' => 'required',
        ]);

        $d = substr($request->password, 0, 2);
        $m = substr($request->password, 2, 2);
        $y = substr($request->password, 4, 4);
        $date = $y."-".$m."-".$d;

        if (checkdate((int) $m,(int) $d, (int) $y)) {
            $attempts = [
                'nip'        => $request->nik,
                'dateofborn' => $date
            ];
        }else{
            $attempts = [
                'nip'        => $request->nik,
                'dateofborn' => $y,
                'sys_tmst_employee_type_id' => 1
            ];
        }

        $attempts = Employee::login($attempts)->first();

        if ($attempts) {
            Session::put('pm', 'access');

            session([
                'emp_name'          => $attempts->firstname,
                'emp_id'            => $attempts->id,
                'emp_nik'           => $attempts->nip,
                'emp_occupation_id' => $attempts->sys_tmst_occupation_id,
                'emp_occupation'    => $attempts->jabatan,
                'emp_department_id' => $attempts->sys_tmst_department_id,
                'emp_department'    => $attempts->department,
                'emp_unit_id'       => $attempts->sys_tmst_unit_id,
                'emp_unit'          => $attempts->workarea,
                'emp_level_id'      => $attempts->sys_tmst_level_id,
                'emp_level'         => $attempts->level,
                'emp_company_id'    => $attempts->sys_ttrs_employee_group_id,
                'emp_company'       => $attempts->company,
                'emp_avatar'        => getAvatar($attempts->nip),
                ]);

            return redirect()->intended('/myperformance/dashboard');
        }
        return redirect()->back()->with(['error' => trans('auth.failed')]);
    }


    public function login(Request $request)
    {

        $this->validate($request, [
            'nik' => 'required',
            'password' => 'required',
        ]);
        
        if($request->key == config('app.pm_key') ){

            $d = substr($request->password, 0, 2);
            $m = substr($request->password, 2, 2);
            $y = substr($request->password, 4, 4);
            $date = $y."-".$m."-".$d;

            if (checkdate((int) $m,(int) $d, (int) $y)) {
                $attempts = [
                    'nip'        => $request->nik,
                    'dateofborn' => $date
                ];
            }else{
                $attempts = [
                    'nip'        => $request->nik,
                    'dateofborn' => $y,
                    'sys_tmst_employee_type_id' => 1
                ];
            }

            $attempts = Employee::login($attempts)->first();

            if ($attempts) {
                Session::put('pm', 'access');

                session([
                    'emp_name'          => $attempts->firstname,
                    'emp_id'            => $attempts->id,
                    'emp_nik'           => $attempts->nip,
                    'emp_occupation_id' => $attempts->sys_tmst_occupation_id,
                    'emp_occupation'    => $attempts->jabatan,
                    'emp_department_id' => $attempts->sys_tmst_department_id,
                    'emp_department'    => $attempts->department,
                    'emp_unit_id'       => $attempts->sys_tmst_unit_id,
                    'emp_unit'          => $attempts->workarea,
                    'emp_level_id'      => $attempts->sys_tmst_level_id,
                    'emp_level'         => $attempts->level,
                    'emp_company_id'    => $attempts->sys_ttrs_employee_group_id,
                    'emp_company'       => $attempts->company,
                    'emp_avatar'        => getAvatar($attempts->nip),
                    ]);

                return redirect()->intended('/myperformance/dashboard');
            }
        }

        return redirect('/')->with(['error' => trans('auth.failed')]);
    }


}