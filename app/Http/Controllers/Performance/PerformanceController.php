<?php

namespace App\Http\Controllers\Performance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use App\Models\PerformanceAppraisal;
use App\Models\Performance;
use App\Models\Assessment;
use App\Models\Competency;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Assesment;
use App\Models\AchievementScore;
use App\Models\GroupRater;
use App\Models\Workarea;
use App\Models\Rater;
use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;
use App\Imports\AdjustedImport;
use App\Imports\PerformanceImport;
use App\Exports\PerformanceExport;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Carbon\Carbon;
use File;
use PDF;


class PerformanceController extends Controller
{   
    
    public function index($year = null)
    {
        if (! Gate::allows('performance')) {
            return abort(401);
        }
        if($year == null){
            $year = date('Y');
        }else{
            $year = $year;
        }
        $department = DB::table('db_klola_hes.sys_tmst_department')->where('parent', 'root')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $workarea   = DB::table('db_klola_hes.sys_tmst_unit')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $company    = DB::table('db_klola_hes.sys_ttrs_employee_group')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $level      = DB::table('db_klola_hes.sys_tmst_level')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        return view('performance.pm.index',compact('department','workarea','company','level','year'));
    }


    public function datatables($year = null)
    {
        if (! Gate::allows('performance')) {
            return abort(401);
        }

        $result = Performance::getData($year);


       return  DataTables::of($result)
        ->addColumn('action', function ($result) {
            
            $url_show    = "<a href='".route('performance.detail', Hashids::encode($result->id))."' class='dropdown-item'><i class='ti-eye'></i> ".trans('global.app_show')." ".trans('pm.kpi')."</a>";  
            $url_delete  = "<form class='delete' action='".route('performance.delete', ['id' => $result->id])."' method='POST'>
                ".csrf_field()."
                <button class='dropdown-item' title='".trans('global.app_delete')."' data-toggle='tooltip'><i class='ti-trash'></i> ".trans('global.app_delete')."</button>
            </form>";


            $button = $url_show;
            $url_midyear = "<a href='".route('performance.show', ['type' => 'midyear' , 'id' => Hashids::encode($result->id)])."' class='dropdown-item'><span class='ti-file icon-lg'></span> ".trans('global.app_result')." MID-YEAR</a>";  
            $url_yearend = "<a href='".route('performance.show', ['type' => 'yearend' , 'id' => Hashids::encode($result->id)])."' class='dropdown-item'><span class='ti-file icon-lg'></span> ".trans('global.app_result')."  YEAR-END</a>";  

            if($result->is_score == 1) {
                $button .= $url_midyear;
            }
            if($result->is_score == 2){
                $button .= $url_midyear.$url_yearend;
            }
            if($result->status == 3){
                $url_score  = "<a href='".route('performance.adjust', ['id' => Hashids::encode($result->id)])."' title='".trans('pm.adjustment_score')."' class='dropdown-item'><i class='ti-pencil'></i> ".trans('pm.adjustment')." </a>";  
                $button .= $url_score;
            }
            return
            '<div class="dropdown">
                <a class="btn btn-md btn-default dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.trans("app.action").'</a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">'
                    .$button
                    .$url_delete.
                '</div>
            </div>';
        }) 
        ->addColumn('emp_data', function ($result){
            return "
            <span class='font-weight-bold text-uppercase'>".$result->nama."</span><br>
            <span class='text-muted font-12'>
                ".$result->company." | ".$result->workarea." | ".$result->department." | ".$result->level."
            </span>";
        })
        ->addColumn('period', function ($result) {
            return $result->period_start.'-'.$result->period_end;
        })
        ->editColumn('created_at', function ($result) {
            return $result->created_at ? with(new Carbon($result->created_at))->format('d/m/Y H:i:s') : '';
        })
        ->editColumn('status', function ($result) {
            return getStatusPM($result->status);
        })
        ->rawColumns(['action','emp_data','status'])
        ->make(true);

    }


    public function detail($id)
    {
        $id = Hashids::decode($id);
        $pm              = Performance::getPerformanceById($id['0']);
        $pm_items        = Performance::getPerformanceItemById($id['0'],1);
        return view('performance.pm.detail',compact('pm','pm_items'));
    }


    public function show($type,$id)
    {
        $id     = Hashids::decode($id);
        $type   = $type;
        $pm              = Performance::getPerformanceById($id['0']);
        $pm_items        = Performance::getPerformanceItemById($pm->id,1);
        $assessment      = Performance::getAppraisalItemById($pm->id);
        $appraisal       = Assessment:: getAssessmentByPmId($pm->id);
        $group_raters    = GroupRater::get();
        $count_mid = 0;
        $count_end = 0;
        foreach( $assessment as $item){
            if( $item->rate != NULL){
                $count_mid++;
            }
            if( $item->rate_yearend != NULL){
                $count_end++;
            }
        }
        if($type == 'midyear' && $count_mid > 0){
            return view('performance.pm.show',compact('pm','pm_items','appraisal','group_raters','type','assessment'));
        }elseif($type == 'yearend' && $count_end > 0){
            return view('performance.pm.show',compact('pm','pm_items','appraisal','group_raters','type','assessment'));
        }else{
            return Redirect::back()
            ->withErrors(['error' => trans('pm.message_detail_failed')]);
        }

    }

    public function adjust($id)
    {   
        $id = Hashids::decode($id);
        $pm         = Performance::getPerformanceById($id['0']);
        return view('performance.pm.adjustment',compact ('pm'));
    }

    public function adjustStore(Request $request)
    {   
        
        $pm = Performance::findOrFail($request->get('pm_id'));

        $data['score_adjustment_1'] = $request->get('score_adjustment_1');
        $data['score_adjustment_2'] = $request->get('score_adjustment_2');
        $data['score_adjustment_1_date'] = dateNumID2MySQL($request->get('score_adjustment_1_date'));
        $data['score_adjustment_2_date'] = dateNumID2MySQL($request->get('score_adjustment_2_date'));
        $pm->update($data);

        return redirect()->route('performance.data',$request->get('pm_year'))->with(['success' => trans('app.message_success_input')]);

    }

    public function adjust_upload()
    {   
        return view('performance.pm.adjustment_upload');
    }

    public function adjust_upload_store(Request $request){
        $this->validate($request, array(
            'file'      => 'required'
        ));
        
        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {   

                $file = $request->file('file'); 
                try{
                    $import = new AdjustedImport($request->get('year'));
                    Excel::import($import, $file);

                } catch (\Exception $e) {
                    return Redirect::back()
                    ->withInput($request->input())  
                    ->withErrors(['Terdapat Template yang belum sesuai']);
                }
                return redirect()->route('performance.data', [$request->get('year')])->with(['success' => trans('app.message_success_input')]);
                
            } else {
                return redirect()->route('performance.data')->with(['error' => 'Error inserting the data, please upload a valid xls/csv file !']);
            }
        }
           
    }

    public function upload()
    {
        if (! Gate::allows('performance')) {
            return abort(401);
        }

        return view('performance.pm.upload');
    }


    public function uploadStore(Request $request){
        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required'
        ));
        
        $period_start  = $request->get('period_1');
        $period_end    = $request->get('period_2');

        $cek_pm     = Performance::cekData($period_start, $period_end, $request->get('year'),$request->get('employee_id'));
        $cek_rater  = Rater::where('employee_id', $request->get('employee_id'))->first();

        if($cek_pm){
            return Redirect::back()
            ->withInput($request->input())  
            ->withErrors([trans('pm.message_failed_input')]);
        }else{
            if($cek_rater){

                if($request->hasFile('file')){
                    $extension = File::extension($request->file->getClientOriginalName());
                    if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {   

                        $employee  = Employee::getById($request->get('employee_id'));

                        $data['period_start']   = $request->get('period_1');
                        $data['period_end']     = $request->get('period_2');
                        $data['year']           = $request->get('year');
                        $data['status']         = 1;
                        $data['emp_id']         = $request->get('employee_id');
                        $data['company_id']     = $employee->sys_ttrs_employee_group_id;
                        $data['unit_id']        = $employee->sys_tmst_unit_id;
                        $data['department_id']  = $employee->sys_tmst_department_id;
                        $data['level_id']       = $employee->sys_tmst_level_id;
                        $data['occupation_id']  = $employee->sys_tmst_occupation_id;

                        $performance = Performance::create($data);
                        
                        $file = $request->file('file'); 
                        try{
                            $import = new PerformanceImport($performance->id, $request->get('employee_id'),$employee->sys_tmst_level_id, $request->get('year'), $request->get('model'));
                            Excel::import($import, $file);
                            if($request->get('model') == 'kpi'){
                                if( $request->get('year') <= 2019){
                                    $status = 1;
                                }else{
                                    $status = 0;
                                }
    
                                $rater   =  Rater::where('employee_id',$request->get('employee_id'))->first();
                                $raterID = json_decode($rater->rate,true);
                        
                                $dataRate = [];
                        
                                foreach ($raterID as $val ){
                                    $key = explode('_',$val['key']);
                                    if($val['value'] != NULL){
                                        $dataRate[] = [
                                            'performance_id'=> $performance->id,
                                            'group_rate'    => $key['0'],
                                            'rater_id'      => $val['value'],
                                            'status'        => $status,
                                            'created_at'    => Carbon::now()->format('Y/m/d H:i:s'),
                                        ];
                                    }
                                }
                        
                                $competency = Competency::where('status', 1)->get();
                                $dataAppraisal = [];
                                foreach ($competency as $val ){
                                    $dataAppraisal[] = [
                                        'performance_id'=> $performance->id,
                                        'competency_id' => $val->id,
                                    ];
                                }
                                Assessment::insert($dataRate);
                                PerformanceAppraisal::insert($dataAppraisal);
                            }
                        } catch (\Exception $e) {
                            return Redirect::back()
                            ->withInput($request->input())  
                            ->withErrors(['Terdapat Template yang belum sesuai']);
                        }
                        return redirect()->route('performance.preview', [Hashids::encode($performance->id), $request->get('model')])->with(['success' => trans('app.message_success_input')]);
                        
                    } else {
                        return redirect()->route('performance.data')->with(['error' => 'Error inserting the data, please upload a valid xls/csv file !']);
                    }
                }
            }else{
                return redirect()->route('performance.data')->with(['error' => trans('pm.message_failed_2_input')]);
            }
            
        }
    }


    public function preview($id, $model = null)
    {
        if (! Gate::allows('performance')) {
            return abort(401);
        }

        $id = Hashids::decode($id);

        $pm              = Performance::getPerformanceById($id['0']);
        $kpi_items       = Performance::getPerformanceItemById($id['0'],0);
        $assessment      = Performance::getAppraisalItemById($pm->id);
        $appraisal       = Assessment:: getAssessmentByPmId($id['0']);
        $rater           = Rater::where('employee_id',$pm->nik)->first();
        $group_raters    = GroupRater::get();
        $model           = $model;

        return view('performance.pm.preview',compact('pm','kpi_items','assessment','appraisal','group_raters','rater','model'));
    }


    public function previewStore(Request $request)
    {
        if (! Gate::allows('performance')) {
            return abort(401);
        }

        $performance_id  = $request->performance_id;

        if ($request->get('status')==1) {
            $data['status'] = 2;
            $pm  = Performance::findOrFail($performance_id);
            $pm->update($data);
            return redirect()->route('performance.data')->with(['success' => trans('app.message_success_input')]);
        }else{
            $pm  = Performance::findOrFail($performance_id);
            $pm->delete();
            return redirect()->route('performance.data')->with(['error' => trans('app.message_failed_input')]);
        }

    }

    public function delete(Request $request)
    {
        if (! Gate::allows('performance')) {
            return abort(401);
        }

        $pm  = Performance::findOrFail($request->id);
        $pm->delete();
        return redirect()->route('performance.data')->with(['success' => trans('app.message_success_delete')]);

    }


    public function destroy(Request $request)
    {
        if (! Gate::allows('performance')) {
            return abort(401);
        }
        $pm  = Performance::destroy($request->id);
        return redirect()->route('performance.data')->with(['success' => trans('app.message_success_delete')]);

    }

    public function search(Request $request)
    { 
        if (! Gate::allows('performance')) {
            return abort(401);
        }
        $data = $request->all();
        $search = trans('global.app_search_by').": " ;
        if($request->input('periode')){
            $search .= " <strong>".trans('pm.periode')."</strong>: ".$request->input('periode');
            $periode = $request->input('periode');
        }else{
            $periode = "";
        }
        if($request->input('company_id')){
            $search .= " <strong>".trans('pm.company')."</strong>: ".getDataByParam('db_klola_hes.sys_ttrs_employee_group','id',$request->input('company_id'))->name;
            $company = $request->input('company_id');
        }else{
            $company = "";
        }
        if($request->input('workarea_id')){
            $search .= " <strong>".trans('pm.workarea')."</strong>: ".getDataByParam('db_klola_hes.sys_tmst_unit','id',$request->input('workarea_id'))->name;
            $workarea = $request->input('workarea_id');
        }else{
            $workarea = "";
        }
        if($request->input('department_id')){
            $search .= " <strong>".trans('pm.department')."</strong>: ".getDataByParam('db_klola_hes.sys_tmst_department','id',$request->input('department_id'))->name;
            $department = $request->input('department_id');
        }else{
            $department = "";
        }
        if($request->input('level_id')){
            $search .= " <strong>".trans('pm.level')."</strong>: ".getDataByParam('db_klola_hes.sys_tmst_level','id',$request->input('level_id'))->name;
            $level = $request->input('level_id');
        }else{
            $level = "";
        }
        
        $query = DB::table('performances')
        ->select('performances.*',
        'employee.firstname AS emp_name','employee.nip AS emp_nik',
        'occupation.alias AS emp_occupation', 
        'department.name AS emp_department', 
        'group.name AS emp_company',
        'level.name AS emp_level',
        'unit.name AS emp_workarea')
        ->leftjoin('db_klola_hes.sys_ttrs_employee AS employee', 'employee.nip', '=', 'performances.emp_id')
        ->leftjoin('db_klola_hes.sys_tmst_department AS department', 'department.id', '=', 'performances.department_id')
        ->leftjoin('db_klola_hes.sys_tmst_occupation AS occupation', 'occupation.id', '=', 'performances.occupation_id')
        ->leftjoin('db_klola_hes.sys_tmst_unit AS unit', 'unit.id', '=', 'performances.unit_id')
        ->leftjoin('db_klola_hes.sys_tmst_level AS level', 'level.id', '=', 'performances.level_id')
        ->leftjoin('db_klola_hes.sys_ttrs_employee_group AS group', 'group.id', '=', 'performances.company_id')
        ->when(!empty($data['periode']), function ($query) use ($data) {
            return $query->where('performances.year',$data['periode']);
        })
        ->when(!empty($data['company_id']), function ($query) use ($data) {
            return $query->where('employee.sys_ttrs_employee_group_id',$data['company_id']);
        })
        ->when(!empty($data['department_id']), function ($query) use ($data) {
            return $query->where('employee.sys_tmst_department_id',$data['department_id']);
        })
        ->when(!empty($data['workarea_id']), function ($query) use ($data) {
            return $query->where('employee.sys_tmst_unit_id',$data['workarea_id']);
        })
        ->when(!empty($data['level_id']), function ($query) use ($data) {
            return $query->where('employee.sys_tmst_level_id',$data['level_id']);
        })
        ->where('performances.status','!=',0)
        ->paginate(25);

        $pm = $query->appends(array(
            'periode'    => $periode,
            'workarea'   => $workarea,
            'department' => $department,
            'level'      => $level,
            'company'    => $company,
        ));

        $department = DB::table('db_klola_hes.sys_tmst_department')->where('parent', 'root')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $workarea   = DB::table('db_klola_hes.sys_tmst_unit')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $company    = DB::table('db_klola_hes.sys_ttrs_employee_group')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $level      = DB::table('db_klola_hes.sys_tmst_level')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');

        return view('performance.pm.search', compact('pm','department', 'workarea','search','company','level'));
    }


    public function print($id,$score)
    { 
        
        $id              = Hashids::decode($id);
        $type            = $score;
        $pm              = Performance::getPerformanceById($id['0']);
        $pm_items        = Performance::getPerformanceItemById($pm->id,1);
        $assessment      = Performance::getAppraisalItemById($pm->id);
        $appraisal       = Assessment:: getAssessmentByPmId($pm->id);
        $group_raters    = GroupRater::get();

        $pdf = PDF::loadView('personal.performance.pdf', compact('pm', 'pm_items','type','appraisal','group_raters','assessment'))->setPaper('a4', 'landscape');;
        return $pdf->download('PMS-'.$pm->nama.'-'.$pm->year.'.pdf');
    }

    public function printKPI($id, $type)
    { 
        
        $id = Hashids::decode($id);
        $pm         = Performance::getPerformanceById($id['0']);
        $pm_items   = Performance::getPerformanceItemById($id['0'],1);

        if($type =="pdf"){
           $pdf = PDF::loadView('personal.performance.print_kpi_pdf', compact('pm', 'pm_items'))->setPaper('a4', 'landscape');;
           return $pdf->download('PMS-'.$pm->nama.'-'.$pm->year.'.pdf');
        }else{
            return view('personal.performance.print_kpi', compact('pm', 'pm_items'));
        }
       
    }


    public function monitoring($year = null)
    {
        if (! Gate::allows('performance')) {
            return abort(401);
        }

        if($year == null){
            $year = date('Y');
        }else{
            $year = $year;
        }
        
        $monitoring = Assessment::getMonitoring($year);

        $department = DB::table('db_klola_hes.sys_tmst_department')->where('parent', 'root')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $workarea   = DB::table('db_klola_hes.sys_tmst_unit')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $company    = DB::table('db_klola_hes.sys_ttrs_employee_group')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $level      = DB::table('db_klola_hes.sys_tmst_level')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $search     = '';
        
        return view('performance.pm.monitoring',compact('department','workarea','company','level','monitoring','search','year'));
    }


    public function monitoring_search(Request $request)
    {
        if (! Gate::allows('performance')) {
            return abort(401);
        }

        $data = $request->all();
        $search = trans('global.app_search_by').": " ;
        $search .= " <strong>".trans('pm.periode')."</strong>: ".$request->input('periode');
        $periode = $request->input('periode');
      
        if($request->input('department_id')){
            $search .= " <strong>".trans('pm.department')."</strong>: ".getDataByParam('db_klola_hes.sys_tmst_department','id',$request->input('department_id'))->name;
            $department = $request->input('department_id');
        }else{
            $department = "";
        }
        if($request->input('company_id')){
            $search .= " <strong>".trans('pm.company')."</strong>: ".getDataByParam('db_klola_hes.sys_ttrs_employee_group','id',$request->input('company_id'))->name;
            $company = $request->input('company_id');
        }else{
            $company = "";
        }
        if($request->input('workarea_id')){
            $search .= " <strong>".trans('pm.workarea')."</strong>: ".getDataByParam('db_klola_hes.sys_tmst_unit','id',$request->input('workarea_id'))->name;
            $workarea = $request->input('workarea_id');
        }else{
            $workarea = "";
        }

        if($request->input('rater_id')){
            $search .= " <strong>".trans('pm.rater')."</strong>: ".getDataByParam('db_klola_hes.sys_ttrs_employee','nip',$request->input('rater_id'))->firstname;
            $rater = $request->input('rater_id');
        }else{
            $rater = "";
        }

        $monitoring = Assessment::getMonitoring($periode, $company, $department, $workarea, $rater);

        $department = DB::table('db_klola_hes.sys_tmst_department')->where('parent', 'root')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $workarea   = DB::table('db_klola_hes.sys_tmst_unit')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $company    = DB::table('db_klola_hes.sys_ttrs_employee_group')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $level      = DB::table('db_klola_hes.sys_tmst_level')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $year       = $request->input('periode');
       
        return view('performance.pm.monitoring',compact('department','workarea','company','level','monitoring','search','year'));
    }


    public function monitoringKPI($year = null,$company_id = null, $workarea_id = null, $department_id = null)
    {
        if (! Gate::allows('performance')) {
            return abort(401);
        }
        if($year == null){
            $year = date('Y');
        }else{
            $year = $year;
        }
        $company_id = $workarea_id = $department_id= 'null';
        $department = DB::table('db_klola_hes.sys_tmst_department')->where('parent', 'root')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $workarea   = DB::table('db_klola_hes.sys_tmst_unit')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $company    = DB::table('db_klola_hes.sys_ttrs_employee_group')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $search     = '';
        
        return view('performance.pm.monitoring_kpi',compact('department','workarea','company','search','year','company_id','workarea_id','department_id'));
    }


    public function monitoringKPI_datatables($year = null,$company_id = null, $workarea_id = null, $department_id = null )
    {
        if (! Gate::allows('performance')) {
            return abort(401);
        }
        if($year == null){
            $year = date('Y');
        }else{
            $year = $year;
        }

        $result = Performance::getMonitoring($year, $company_id, $workarea_id, $department_id);

        return  DataTables::of($result)
        ->addColumn('emp_data', function ($result){
            return "
            <span class='font-weight-bold text-uppercase'>".$result->emp_name."</span><br>
            <span class='text-muted font-12'>
            ".$result->company." | ".$result->workarea." | ".$result->department." | ".$result->level."
            </span>";
        })
        ->rawColumns(['emp_data'])
        ->make(true);
        
    }


    public function monitoringKPI_search(Request $request)
    {
        if (! Gate::allows('performance')) {
            return abort(401);
        }

        $data = $request->all();
        $search = trans('global.app_search_by').": " ;
        $search .= " <strong>".trans('pm.periode')."</strong>: ".$request->input('periode');
        $periode = $request->input('periode');
       
        if($request->input('department_id')){
            $search .= " <strong>".trans('pm.department')."</strong>: ".getDataByParam('db_klola_hes.sys_tmst_department','id',$request->input('department_id'))->name;
            $department_id = $request->input('department_id');
        }else{
            $department_id = "null";
        }
        if($request->input('company_id')){
            $search .= " <strong>".trans('pm.company')."</strong>: ".getDataByParam('db_klola_hes.sys_ttrs_employee_group','id',$request->input('company_id'))->name;
            $company_id = $request->input('company_id');
        }else{
            $company_id = "null";
        }
        if($request->input('workarea_id')){
            $search .= " <strong>".trans('pm.workarea')."</strong>: ".getDataByParam('db_klola_hes.sys_tmst_unit','id',$request->input('workarea_id'))->name;
            $workarea_id = $request->input('workarea_id');
        }else{
            $workarea_id = "null";
        }

        $department = DB::table('db_klola_hes.sys_tmst_department')->where('parent', 'root')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $workarea   = DB::table('db_klola_hes.sys_tmst_unit')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $company    = DB::table('db_klola_hes.sys_ttrs_employee_group')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        $year       = $request->input('periode');

        return view('performance.pm.monitoring_kpi',compact('department','workarea','company','search','year','company_id','workarea_id','department_id'));
    }

   
    public function export(Request $request)
    {
        $date = date('d-m-Y');
        return Excel::download(new PerformanceExport($request->get('periode'), $request->get('company_id'), $request->get('department_id'),  $request->get('workarea_id'), $request->get('level_id'), $request->get('tipe')), 'PerformanceAppraisal-'.$request->get('tipe').'-'.$date.'.xlsx');
    }

}
