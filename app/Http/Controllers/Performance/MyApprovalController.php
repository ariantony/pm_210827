<?php

namespace App\Http\Controllers\Performance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Models\Performance;
use App\Models\PerformanceItem;
use Vinkla\Hashids\Facades\Hashids;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Mail\SendMailable;

class MyApprovalController extends Controller
{   


    public function index()
    {
        $department = DB::table('db_klola_hes.sys_tmst_department')->get()->pluck('name', 'departement_id')->prepend('Silahkan Pilih');
        $workarea   = DB::table('db_klola_hes.sys_tmst_unit')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        return view('personal.approval.index',compact('department','workarea'));
    }

    public function datatables()
    {
        $result = Performance::getApprovalList(Session::get('emp_nik'));

        return  DataTables::of($result)
        ->addColumn('action', function ($result) {

            $action = "<a href='".route('myapproval.approve', ['id' => Hashids::encode($result->id)])."' title='".trans('pm.approval')."' data-toggle='tooltip' class='btn btn-warning btn-sm text-white mr-1'> Approve</a>";  
            return $action;

        }) 
        ->addColumn('emp_data', function ($result){
            return "
                <span class='font-weight-bold text-uppercase'>".$result->emp_name."</span><br>
                <span class='text-muted' style='font-size:12px'>
                <i class='ti-card'></i> ".$result->emp_nik." -".$result->emp_workarea." | 
                    ".$result->emp_department."/".$result->emp_occupation."
                 </span>";
        })
            ->rawColumns(['action','action','emp_data','group'])
        ->make(true);

    }


    public function approve($id)
    {   
        $id = Hashids::decode($id);
        $pm = Performance::getPerformanceById($id['0']);
        $pm_items  = Performance::getPerformanceItemById($pm->id,0);
        return view('personal.approval.set',compact ('pm','pm_items'));
       
    }

    public function store(Request $request)
    {   
        
        $pm = Performance::findOrFail($request->get('pm_id'));

        
        $pm_item = $request->get('pm_item');
        $dataPM  = [];


        for($i=0;$i < count($pm_item);$i++) {
            $ids[]      = $request->get('pm_item')[$i];
            $status[]   = "WHEN id = {$request->get('pm_item')[$i]} THEN 1";
            if($request->get('target_revision')[$i] == ""){
                $target[]   = "WHEN id = {$request->get('pm_item')[$i]} THEN NULL";
            }else{
                $target[]   = "WHEN id = {$request->get('pm_item')[$i]} THEN '".$request->get('target_revision')[$i]."'";
            }
        }

        $ids        = implode(',', $ids);
        $status     = implode(' ', $status);
        $target     = implode(' ', $target);

        \DB::update("UPDATE performance_items SET status = CASE {$status} END, target_revision = CASE {$target} END  WHERE id in ({$ids})");

        $data['status'] = 2;
        $pm->update($data);

        return redirect()->route('myapproval.index')->with(['success' => trans('app.message_success_input')]);

    }


}
