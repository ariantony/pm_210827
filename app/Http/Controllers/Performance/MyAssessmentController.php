<?php

namespace App\Http\Controllers\Performance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Models\Performance;
use App\Models\Assessment;
use App\Models\Rater;
use App\Models\Score;
use App\Models\PerformanceItem;
use App\Models\PerformanceAppraisal;
use App\Models\AchievementScore;
use App\Models\Competency;
use App\Models\Workarea;
use App\Models\GroupRater;
use App\Models\Employee;
use App\Models\Department;
use Vinkla\Hashids\Facades\Hashids;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Mail\SendMailable;

class MyAssessmentController extends Controller
{   


    public function index()
    {
        $department = DB::table('db_klola_hes.sys_tmst_department')->get()->pluck('name', 'departement_id')->prepend('Silahkan Pilih');
        $workarea   = DB::table('db_klola_hes.sys_tmst_unit')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        return view('personal.assessment.index',compact('department','workarea'));
    }

    public function datatables()
    {
        $result = Assessment::getAssessmentList(Session::get('emp_nik'));

        return  DataTables::of($result)
        ->addColumn('achievement', function ($result) {

            $achievement = '';
            if($result->status == 0){
                $url_midyear  = "<a href='".route('myassessment.assess', ['type' => 'midyear' , 'id' => Hashids::encode($result->id)])."' title='".trans('pm.assessment')."' data-toggle='tooltip' class='btn btn-warning btn-sm text-white mr-1'> MID YEAR</a>";  
                $achievement .= $url_midyear;
            }
            if($result->status == 1){
                $url_yearend  = "<a href='".route('myassessment.assess', ['type' => 'yearend' , 'id' => Hashids::encode($result->id)])."' title='".trans('pm.assessment')."' data-toggle='tooltip' class='btn btn-danger btn-sm text-white'> YEAR END</a>";  
                $achievement .= $url_yearend;
            }
            return $achievement;

        }) 
        ->addColumn('emp_data', function ($result){
            return "
                <span class='font-weight-bold text-uppercase'>".$result->emp_name."</span><br>
                <span class='text-muted' style='font-size:12px'>
                <i class='ti-card'></i> ".$result->emp_nik." -".$result->emp_workarea." | 
                    ".$result->emp_department."/".$result->emp_occupation."
                 </span>";
        })
        ->addColumn('group', function ($result){
            return "<span class='text-uppercase'>".$result->group."</span>";
        })
        ->rawColumns(['action','achievement','emp_data','group'])
        ->make(true);

    }


    public function assess($type, $id)
    {   
        $id = Hashids::decode($id);
        $type                   = $type;
        $pm                     = Assessment::getPerformance($id['0']);
        $kpi_items              = Performance::getPerformanceItemById($pm->pm_id,1);
        $achievement_score      = AchievementScore::get();

        $competency = Competency::get();
        $score      = Score::get();
        
        if($pm->group_rate == 'superior' && count($kpi_items) > 0){
            if($type == 'midyear'){
                return view('personal.assessment.assess_kpi_midyear',compact ('pm','kpi_items','achievement_score','type','competency','score'));
            }else{
                return view('personal.assessment.assess_kpi_yearend',compact ('pm','kpi_items','achievement_score','type','competency','score'));
            }
        }else{
            return view('personal.assessment.assess',compact ('pm','kpi_items','achievement_score','type','competency','score'));
        }
       
    }

    public function storeAssess(Request $request)
    {   
        

        $pm = Performance::findOrFail($request->get('pm_id'));
        $assessement = Assessment::where('performance_id',$request->get('pm_id'));

        $pm_items = $request->get('pm_item');
         
        if($request->get('group_rater') == "superior"){
            if($pm_items){

                if($request->get('tipe')=='midyear'){
                    $score  = [];
                    $ids = [];
                    $achievement = [];
                    for($i=0;$i < count($pm_items);$i++) {
                        $ids[]    = $request->get('pm_item')[$i];
                        $score[]        = "WHEN id = {$request->get('pm_item')[$i]} THEN {$request->get('score')[$i]}";
                        if($request->get('achievement')[$i]){
                            $achievement[]  = "WHEN id = {$request->get('pm_item')[$i]} THEN '{$request->get('achievement')[$i]}'";
                        }else{
                            $achievement[]  = "WHEN id = {$request->get('pm_item')[$i]} THEN NULL";
                        }
                    }
                    $ids            = implode(',', $ids);
                    $score          = implode(' ', $score);
                    $achievement    = implode(' ', $achievement);
                    \DB::update("UPDATE performance_items SET midyear_score = CASE {$score} END, midyear_achievement_revision = CASE {$achievement} END WHERE id in ({$ids})");
                }else{
                    $score  = [];
                    $ids = [];
                    $kpi_score = 0;
                    for($i=0;$i < count($pm_items);$i++) {
                        $ids[]    = $request->get('pm_item')[$i];
                        $score[]  = "WHEN id = {$request->get('pm_item')[$i]} THEN {$request->get('score')[$i]}";
                        if($request->get('achievement')[$i]){
                            $achievement[]  = "WHEN id = {$request->get('pm_item')[$i]} THEN '{$request->get('achievement')[$i]}'";
                        }else{
                            $achievement[]  = "WHEN id = {$request->get('pm_item')[$i]} THEN NULL";
                        }
                    }
                    $ids            = implode(',', $ids);
                    $score          = implode(' ', $score);
                    $achievement    = implode(' ', $achievement);
                    \DB::update("UPDATE performance_items SET yearend_score = CASE {$score} END, yearend_achievement_revision = CASE {$achievement} END WHERE id in ({$ids})");
                }
            }
        }

        $competency = $request->get('competency');
        $totalrate = 0;

        for ($i=0;$i < count($competency);$i++) {
            
            $rateArray = array();
            $commentArray = array();

            $rates = array(
                "key"   => $request->get('group_rater'),
                "nik"   => Session::get('emp_nik'),
                "value" => $request->get('rate')[$i]
            );
            $ratesJson = json_encode($rates);

            $com = $i+1;
            $comments = array(
                "key"   => $request->get('group_rater'),
                "nik"   => Session::get('emp_nik'),
                "value" => $request->get('comment_'.$com)
            );
            $commentsJson = json_encode($comments);

            $appraisal = PerformanceAppraisal::where('performance_id',$request->get('pm_id'))
            ->where('competency_id',$request->get('competency_id')[$i])
            ->first();

          
            if($request->get('tipe')=='midyear'){
                if($appraisal->rate != null){
                    $rateArray = json_decode($appraisal->rate,true);
                    $rateArray[] = json_decode($ratesJson,true);
                }else{
                    $rateArray[] = json_decode($ratesJson,true);
                }
    
                if($appraisal->comment_midyear != null){
                    $commentArray = json_decode($appraisal->comment_midyear,true);
                    $commentArray[] = json_decode($commentsJson,true);
                }else{
                    $commentArray[] = json_decode($commentsJson,true);
                }
                
                $dataAsses = array(
                    'rate'              => json_encode($rateArray),
                    'comment_midyear'   => json_encode($commentArray),
                );
            }else{
                if($appraisal->rate_yearend != null){
                    $rateArray = json_decode($appraisal->rate_yearend,true);
                    $rateArray[] = json_decode($ratesJson,true);
                }else{
                    $rateArray[] = json_decode($ratesJson,true);
                }
    
                if($appraisal->comment_yearend != null){
                    $commentArray = json_decode($appraisal->comment_yearend,true);
                    $commentArray[] = json_decode($commentsJson,true);
                }else{
                    $commentArray[] = json_decode($commentsJson,true);
                }
                
                $dataAsses = array(
                    'rate_yearend'      => json_encode($rateArray),
                    'comment_yearend'   => json_encode($commentArray),
                );
            }

            $appraisal->update($dataAsses);
            $totalrate   += $request->get('rate')[$i];
        }


        $assessment = Assessment::where('performance_id',$request->get('pm_id'))
        ->where('group_rate',$request->get('group_rater'))
        ->where('rater_id',Session::get('emp_nik'))
        ->first();

        if ($request->get('tipe')=='midyear') {
            $dataAssesment['status']     = 1;
            $dataAssesment['date_midyear'] = date('Y-m-d H:i:s');
            

            $staff = explode(",",config('app.staff_id'));
            if (in_array($pm->level_id, $staff)) {
                if($request->get('group_rater') =='superior'){
                    $dataPM['is_score']  = 2;
                    $pm->update($dataPM);
                }
            }
            
        }

        if ($request->get('tipe')=='yearend') {
            $dataAssesment['status']     = 2;
            $dataAssesment['date_yearend'] = date('Y-m-d H:i:s');
        }

        $assessment->update($dataAssesment);

        return redirect()->route('myassessment.index')->with(['success' => trans('app.message_success_input')]);

    }


    public function history()
    {
        $department = DB::table('db_klola_hes.sys_tmst_department')->get()->pluck('name', 'departement_id')->prepend('Silahkan Pilih');
        $workarea   = DB::table('db_klola_hes.sys_tmst_unit')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        return view('personal.assessment.history',compact('department','workarea'));
    }

    public function datatablesHistory()
    {
        $result = Assessment::getAssessmentHistory(Session::get('emp_nik'));
        return  DataTables::of($result)
        ->addColumn('action', function ($result) {
            
            $btn = '';
            if($result->date_midyear != NULL){
                $url_midyear = "<a href='".route('myassessment.detail', ['type' => 'midyear' , 'id' => Hashids::encode($result->id)])."' class='dropdown-item'><span class='ti-file icon-lg'></span> MID-YEAR</a>";  
                $btn .= $url_midyear;
            }
            if($result->date_yearend != NULL){
                $url_yearend = "<a href='".route('myassessment.detail', ['type' => 'yearend' , 'id' => Hashids::encode($result->id)])."' class='dropdown-item'><span class='ti-file icon-lg'></span> YEAR-END</a>";  
                $btn .= $url_yearend;
            }
            
            return 
            '<div class="dropdown">
                <a class="btn btn-md btn-default dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.trans('pm.assessment').'</a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">'
                    .$btn.
                '</div>
            </div>';
        }) 
        ->addColumn('emp_data', function ($result){
            return "
                <span class='font-weight-bold text-uppercase'>".$result->emp_name."</span><br>
                <span class='text-muted' style='font-size:12px'>
                <i class='ti-card'></i> ".$result->emp_nik." -".$result->emp_workarea." | 
                    ".$result->emp_department."/".$result->emp_occupation."
                 </span>";
        })
        ->addColumn('group', function ($result){
            return "<span class='text-uppercase'>".$result->group."</span>";
        })
        ->rawColumns(['action','emp_data','group'])
        ->make(true);

    }

    public function detail($type, $id)
    {
        $id = Hashids::decode($id);
        $type           = $type;
        $pm              = Assessment::getPerformance($id['0']);
        $pm_items        = Performance::getPerformanceItemById($pm->pm_id,1);
        $assessment      = Performance::getAppraisalItemById($pm->pm_id);
        $appraisal       = Assessment:: getAssessmentByPmId($pm->pm_id);
        $group_raters    = GroupRater::get();
        $rater           = Rater::where('employee_id',$pm->emp_nik)->first();
       
        return view('personal.assessment.detail',compact('pm','type','pm_items','assessment','group_raters','rater','appraisal'));
    }


}
