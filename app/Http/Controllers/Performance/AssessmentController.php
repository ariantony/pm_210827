<?php

namespace App\Http\Controllers\Performance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Models\Performance;
use App\Models\Division;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Assesment;
use App\Models\Workarea;
use Vinkla\Hashids\Facades\Hashids;
use Yajra\DataTables\Facades\DataTables;

class AssessmentController extends Controller
{   
    
    public function index()
    {
        if (! Gate::allows('performance')) {
            return abort(401);
        }
        $department = DB::table('db_klola_hes.sys_tmst_department')->get()->pluck('name', 'departement_id')->prepend('Silahkan Pilih');
        $workarea   = DB::table('db_klola_hes.sys_tmst_unit')->get()->pluck('name', 'id')->prepend('Silahkan Pilih');
        return view('performance.pm.index',compact('department','workarea'));
    }


    public function input($nik,$id)
    {   
        $nik = Hashids::decode($nik);

        $employee = Employee::where('nik',$nik['0'])->first();
        $assesment = Assesment::where('id',$id['0'])->first();

        return view('performance.pm.assesment',compact ('employee','assesment'));
    }

}
