<?php

namespace App\Http\Controllers\Performance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Models\Performance;
use App\Models\PerformanceHistory;
use App\Models\PerformanceItem;
use App\Models\PerformanceAppraisal;
use App\Models\Competency;
use App\Models\GroupRater;
use App\Models\Employee;
use App\Models\ObjectiveCategory;
use App\Models\Dashboard;
use App\Models\Measure;
use App\Models\Announcement;
use App\Models\Rater;
use App\Models\Assessment;
use Vinkla\Hashids\Facades\Hashids;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PerformanceResultExport;
use App\Imports\MyPerformanceImport;
use App\Mail\SendMailable;
use PDF;
use File;


class MyPerformanceController extends Controller
{   
    
  
    public function index()
    {
        return view('personal.performance.index');
    }


    public function datatables()
    {
        $result = Performance::where('emp_id', Session::get('emp_nik'))->whereIn('status',[0,1,2,3,4])->get();

        return  DataTables::of($result)
        ->addColumn('action', function ($result) {
            
            $url_edit    = "<a href='".route('myperformance.personal.edit', Hashids::encode($result->id))."' title='".trans('global.app_edit')."' class='dropdown-item'><span class='ti-pencil icon-lg'></span> ".trans('global.app_edit')." </a>";  
            $url_show    = "<a href='".route('myperformance.show', ['id' => Hashids::encode($result->id)])."' class='dropdown-item'><span class='ti-eye icon-lg'></span> ".trans('global.app_show').' '.trans('pm.kpi')."  </a>";  
            $show        = $url_show;

            if($result->is_score == 1){
                $show .= "<a href='".route('myperformance.detail', ['type' => 'midyear' , 'id' => Hashids::encode($result->id)])."' class='dropdown-item'><span class='ti-file icon-lg'></span> ".trans('global.app_result')." MID-YEAR</a>";
            }
            if($result->is_score == 2){
                $show .= "<a href='".route('myperformance.detail', ['type' => 'midyear' , 'id' => Hashids::encode($result->id)])."' class='dropdown-item'><span class='ti-file icon-lg'></span> ".trans('global.app_result')." MID-YEAR</a>
                        <a href='".route('myperformance.detail', ['type' => 'yearend' , 'id' => Hashids::encode($result->id)])."' class='dropdown-item'><span class='ti-file icon-lg'></span> ".trans('global.app_result')."  YEAR-END</a>";
            }
            
            $url_delete = "<form class='delete' action='".route('myperformance.personal.delete', ['id' => $result->id])."' method='POST'>
                            ".csrf_field()."
                            <button class='text-danger dropdown-item' data-toggle='tooltip'> <i class='ti-trash icon-lg'></i> ".trans('global.app_delete')."</button>
                        </form>";

            $set_midyear = '';
            $set_yearend = '';
            
            if ($result->is_score == 0 && $result->is_achievement_midyear == 0) {
                $set_midyear  = "<a href='".route('myperformance.achievement', ['type' => 'midyear' , 'id' => Hashids::encode($result->id)])."' title='".trans('pm.achievement')."' data-toggle='tooltip' class='btn btn-warning btn-sm text-white'> MID-YEAR</a>";
            }

            if (($result->is_score == 1 && $result->is_achievement_yearend == 0)  && getAssessment($result->id) ) {
                $set_yearend  = "<a href='".route('myperformance.achievement', ['type' => 'yearend' , 'id' => Hashids::encode($result->id)])."' title='".trans('pm.achievement')."' data-toggle='tooltip' class='btn btn-info btn-sm text-white'> YEAR-END</a>";  
            }


            $achievement = '';
            if($result->status == 2 && !isStaff( Session::get('emp_level_id'))){
                $achievement .= $set_midyear;
                $achievement .= $set_yearend;
            }

            if($result->status==0 || $result->status== 1 ){
                return '<div class="dropdown">
                        <a class="btn btn-sm btn-default dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.trans("app.action").'</a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">'
                            .$url_edit 
                            .$url_show
                            .$url_delete.
                        '</div>
                    </div>';
            }else{
                return
                '<div class="btn-group">'.
                 $achievement.
                '<div class="dropdown">
                    <a class="btn btn-sm btn-default dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.trans("app.action").'</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">'
                        .$show.
                    '</div>
                </div></div>';
            }
        }) 
        ->addColumn('period', function ($result) {
            return $result->period_start."-".$result->period_end;
        })
        ->editColumn('status', function ($result) {
            return getStatusPM($result->status);
        })
        ->editColumn('created_at', function ($result) {
            return $result->created_at ? with(new Carbon($result->created_at))->format('d/m/Y H:i:s') : '';
        })
        ->rawColumns(['action','status','achievement'])
        ->make(true);

    }

    public function create()
    {
        $groupraters = GroupRater::get();

        $pm_aspect = ObjectiveCategory::where('status',1)->get()->pluck('name', 'name');
        $measure = Measure::where('status',1)->get()->pluck('title', 'id');

        return view('personal.performance.create',compact('pm_aspect','measure'));
    }

    
    public function store(Request $request)
    {
        $period_start  = $request->get('period_1');
        $period_end    = $request->get('period_2');
        $cek_pm = Performance::cekData($period_start, $period_end, $request->get('year'), Session::get('emp_nik'));

        if($cek_pm){
            return Redirect::back()
            ->withInput($request->input())  
            ->withErrors(['error' => trans('pm.message_failed_input')]);
        }else{

            $raters      = Rater::where('employee_id',Session::get('emp_nik'))->first();
         
            if($raters){

                $staff = explode(",",config('app.staff_id'));
                $groupraters = GroupRater::get();

                if(in_array(Session::get('emp_level_id'),$staff)){
                    $rater_weight = [
                        'self' => 5,
                        'superior' => 95
                    ];
                }else{
                    $rater_weight = [];
                    foreach($groupraters as $val){
                        $rater_weight[$val->code] =  $val->weight;
                    }
                }
                
                if ($request->get('status')==1) {
                    if (in_array(Session::get('emp_level_id'), $staff)) {
                        $data['status']    = 2;
                        if( $request->get('year') <= 2019){
                            $data['is_score']  = 2;
                        }else{
                            $data['is_score']  = 1;
                        }
                        $statusItemKPI     = 1;
                    }else{
                        if( $request->get('year') <= 2019){
                            $data['status']    = 2;
                            $data['is_score']  = 2;
                            $statusItemKPI     = 1;
                        }else{
                            $data['status']    = 1;
                            $statusItemKPI     = 0;
                        }
                    }
                    $dataHistory['message']   = 'insert';
                }else{
                    $statusItemKPI     = 0;
                    $data['status']    = 0;
                    $dataHistory['message']  = 'draft';
                }
                
                $data['period_start']   = $request->get('period_1');
                $data['period_end']     = $request->get('period_2');
                $data['year']           = $request->get('year');
                $data['emp_id']         = Session::get('emp_nik');
                $data['company_id']     = Session::get('emp_company_id');
                $data['unit_id']        = Session::get('emp_unit_id');
                $data['department_id']  = Session::get('emp_department_id');
                $data['level_id']       = Session::get('emp_level_id');
                $data['occupation_id']  = Session::get('emp_occupation_id');
                $data['kpi_weight']     = getSetting('weight_kpi');
                $data['competency_weight']= getSetting('weight_competency');
                $data['rater_weight']     = json_encode($rater_weight);
    
                $pm = Performance::create($data);
    
                $dataPM = [];
                $pm_aspect = $request->get('pm_aspect');
                
                for($i=0;$i < count($pm_aspect);$i++) {
                    $dataPM[] = [
                        'performance_id'=> $pm->id,
                        'aspect'        => $request->get('pm_aspect')[$i],
                        'area'          => $request->get('area')[$i],
                        'kpi'           => $request->get('kpi')[$i],
                        'weight'        => $request->get('weight')[$i],
                        'target'        => $request->get('target')[$i],
                        'measure_id'    => $request->get('measure')[$i],
                        'status'        => $statusItemKPI,
                        'created_at'    => Carbon::now()->format('Y/m/d H:i:s'),
                    ];
                }
                $pm_items = PerformanceItem::insert($dataPM);
                
                $rater = json_decode($raters['rate'], true);

                if( $request->get('year') <= 2019){
                    $status = 1;
                }else{
                    $status = 0;
                }

                $dataRate = [];
                foreach ($rater as $val ){
                    $key = explode('_',$val['key']);
                    if($val['value'] != NULL){
                        $dataRate[] = [
                            'performance_id'=> $pm->id,
                            'group_rate'    => $key['0'],
                            'rater_id'      => $val['value'],
                            'status'        => $status,
                            'created_at'    => Carbon::now()->format('Y/m/d H:i:s'),
                        ];
                    }
                }

                $competency = Competency::where('status', 1)->get();

                $dataAppraisal = [];

                foreach ($competency as $val ){
                    $dataAppraisal[] = [
                        'performance_id'=> $pm->id,
                        'competency_id' => $val->id,
                    ];
                }

                $dataHistory['performance_id'] = $pm->id;

                Assessment::insert($dataRate);
                PerformanceAppraisal::insert($dataAppraisal);
                PerformanceHistory::create($dataHistory);
            
                return redirect()->route('myperformance.personal.index')->with(['success' => trans('app.message_success_input')]);

            }else{
                return Redirect::back()
                ->withInput($request->input())  
                ->withErrors(['error' => trans('pm.message_failed_2_input')]);
            }
            
        }
    }


    public function detail($type,$id)
    {
        $id     = Hashids::decode($id);
        $type   = $type;
        $pm              = Performance::getPerformanceById($id['0']);
        $pm_items        = Performance::getPerformanceItemById($pm->id,1);
        $assessment      = Performance::getAppraisalItemById($pm->id);
        $appraisal       = Assessment:: getAssessmentByPmId($pm->id);
        $group_raters    = GroupRater::get();
        $count_mid = 0;
        $count_end = 0;
        foreach( $assessment as $item){
            if( $item->rate != NULL){
                $count_mid++;
            }
            if( $item->rate_yearend != NULL){
                $count_end++;
            }
        }
        if($type == 'midyear' && $count_mid > 0){
            return view('personal.performance.detail',compact('pm','pm_items','appraisal','group_raters','type','assessment'));
        } elseif($type == 'yearend' && $count_end > 0){
            return view('personal.performance.detail',compact('pm','pm_items','appraisal','group_raters','type','assessment'));
        }else{
            return Redirect::back()
            ->withErrors(['error' => trans('pm.message_detail_failed')]);
        }

    }

    public function show($id)
    {
        $id         = Hashids::decode($id);
        $pm         = Performance::getPerformanceById($id['0']);
        $pm_items   = Performance::getPerformanceItemById($id['0']);

        return view('personal.performance.show',compact('pm','pm_items'));
    }

    public function edit($id)
    {
        $id = Hashids::decode($id);
        $pm  = Performance::findOrFail($id['0']);
        $pm_items  = PerformanceItem::getPerformanceItem($id['0']);
        $category = ObjectiveCategory::get()->pluck('name', 'name');
        $measure = Measure::get()->pluck('title', 'id');
        return view('personal.performance.edit',compact('pm','pm_items','category','measure'));
    }

    public function update(Request $request, $id)
    {
       
        $pm = Performance::findOrFail($id);

        $period_start  = $request->get('period_1');
        $period_end    = $request->get('period_2');
        $cek_pm        = Performance::cekData($period_start, $period_end, $request->get('year'), Session::get('emp_nik'), $id);

        if($cek_pm){
            return redirect()->route('myperformance.personal.index')->with(['error' => trans('pm.message_failed_input')]);
        }else{
            
            $staff = explode(",",config('app.staff_id'));
            
            if ($request->get('status')==1) {
                if (in_array(Session::get('emp_level_id'), $staff)) {
                    $data['status']    = 2;
                    if( $request->get('year') <= 2019){
                        $data['is_score']  = 2;
                    }else{
                        $data['is_score']  = 1;
                    }
                    $statusItemKPI     = 1;
                }else{
                    if( $request->get('year') <= 2019){
                        $data['status']    = 2;
                        $data['is_score']  = 2;
                        $statusItemKPI     = 1;
                    }else{
                        $data['status']    = 1;
                        $statusItemKPI     = 0;
                    }
                }
                $dataHistory['message']   = 'update';
            }else{
                $statusItemKPI     = 0;
                $data['status']    = 0;
                $dataHistory['message']  = 'draft';
            }

            $data['period_start']   = $request->get('period_1');
            $data['period_end']     = $request->get('period_2');
            $data['year']           = $request->get('year');
            $data['emp_nik']        = Session::get('emp_nik');

            $pm->update($data);

            $pm_item_delete = PerformanceItem::where('performance_id', $id)->delete();

            $dataPM = [];
            $pm_aspect = $request->get('pm_aspect');
            for ($i=0;$i < count($pm_aspect);$i++) {
                $dataPM[] = [
                'performance_id'=> $pm->id,
                'aspect'        => $request->get('pm_aspect')[$i],
                'area'          => $request->get('area')[$i],
                'kpi'           => $request->get('kpi')[$i],
                'weight'        => $request->get('weight')[$i],
                'target'        => $request->get('target')[$i],
                'measure_id'    => $request->get('measure')[$i],
                'status'        => $statusItemKPI,
                'created_at'    => Carbon::now()->format('Y/m/d H:i:s'),
            ];
            }
            $pm_items = PerformanceItem::insert($dataPM);

            $dataHistory['message'] = 'update';
            $dataHistory['performance_id'] = $pm->id;
            $pm_history = PerformanceHistory::create($dataHistory);
       
            return redirect()->route('myperformance.personal.index')->with(['success' => trans('app.message_success_input')]);
        }
    }


    public function delete(Request $request)
    {
        $pm  = Performance::findOrFail($request->id);
        $pm->delete();
        return redirect()->route('myperformance.personal.index')->with(['success' => 'Delete was successful!']);
    }

    public function achievement($type, $id)
    {   
        $id = Hashids::decode($id);
        $type       = $type;
        $pm         = Performance::getPerformanceById($id['0']);
        $kpi_items  = Performance::getPerformanceItemById($id['0'],1);
        if($type == 'midyear' || $type == 'yearend'){
            return view('personal.performance.achievement',compact ('pm','kpi_items','type'));
        }else{
            return redirect()->route('myperformance.personal.index')->with(['error' => trans('app.message_failed_input')]);
        }
    }

    public function achievementStore(Request $request)
    {   
        
        $pm = Performance::findOrFail($request->get('pm_id'));

        $pm_items = $request->get('pm_item');
        
        $achievement  = [];
        $ids = [];
        for($i=0;$i < count($pm_items);$i++) {
            $ids[]          = $request->get('pm_item')[$i];
            $achievement[]  = "WHEN id = {$request->get('pm_item')[$i]} THEN '{$request->get('achievement')[$i]}' ";
        }
        $ids            = implode(',', $ids);
        $achievement    = implode(' ', $achievement);

        if($request->get('type')=='midyear'){
            $data['is_score'] = 1;
            \DB::update("UPDATE performance_items SET midyear_achievement = CASE {$achievement} END WHERE id in ({$ids})");
        }else{
            $data['is_score'] = 2;
            \DB::update("UPDATE performance_items SET yearend_achievement = CASE {$achievement} END WHERE id in ({$ids})");
        }

        if ($request->get('status')==1) {
            if ($request->get('type')=='midyear') {
                $data['is_achievement_midyear'] = 1;
            }else{
                $data['is_achievement_yearend'] = 1;
            }
        }
        $pm->update($data);
        return redirect()->route('myperformance.personal.index')->with(['success' => trans('app.message_success_input')]);

    }

    public function print($id, $score)
    { 
        $id              = Hashids::decode($id);
        $type            = $score;
        $pm              = Performance::getPerformanceById($id['0']);
        $pm_items        = Performance::getPerformanceItemById($pm->id,1);
        $assessment      = Performance::getAppraisalItemById($pm->id);
        $appraisal       = Assessment:: getAssessmentByPmId($pm->id);
        $group_raters    = GroupRater::get();
        $pdf = PDF::loadView('personal.performance.pdf', compact('pm', 'pm_items','type','appraisal','group_raters','assessment'))->setPaper('a4', 'landscape');;
        return $pdf->download('PMS-'.Session::get('emp_name').'-'.$pm->year.'.pdf');
    }

    public function printKPI($id, $type)
    { 
        
        $id = Hashids::decode($id);
        $pm         = Performance::getPerformanceById($id['0']);
        $pm_items   = Performance::getPerformanceItemById($id['0'],1);

        if($type =="pdf"){
           $pdf = PDF::loadView('personal.performance.print_kpi_pdf', compact('pm', 'pm_items'))->setPaper('a4', 'landscape');;
           return $pdf->download('PMS-'.Session::get('emp_name').'-'.$pm->year.'.pdf');
        }else{
            return view('personal.performance.print_kpi', compact('pm', 'pm_items'));
        }
       
    }

    public function dashboard()
    {
        $emp_appraisal  =  getRaterByEmployeeID(Session::get('emp_nik'));

        $empID = [];
        foreach ($emp_appraisal as $val){
            if(Session::get('emp_nik') != $val->employee_id){
                $empID []= $val->employee_id;
            }
        }
        $matrik_raters  = getEmployee($empID);

        $assessment     = '';
        $announcement   = Announcement::latest()->first();
        $assessment     = Assessment::getAssessmentList(Session::get('emp_nik'));

        $pm = Performance::where('emp_id', Session::get('emp_nik'))->orderBy('year', 'DESC')->limit('1')->first();
    
        if($pm){
            $pm_items        = Performance::getPerformanceItemById($pm->id,1);

            $yearend_score = $midyear_score = 0;
            foreach ($pm_items as $item){
                $yearendscore  = ($item->weight/100) * $item->yearend_score;
                $yearend_score  += $yearendscore ;
                $midyearscore= ($item->weight/100) * $item->midyear_score;
                $midyear_score += $midyearscore;
            }
            $competency_midyear = getPAScore($pm->rater_weight,$pm->competency_weight, $pm->id, 'midyear');
            $competency_yearend = getPAScore($pm->rater_weight,$pm->competency_weight, $pm->id, 'yearend');

            $pa_score_midyear = ($competency_midyear * ($pm->competency_weight/100)) + $midyear_score * ($pm->kpi_weight/100);
            $pa_score_yearend = ($competency_yearend * ($pm->competency_weight/100)) + $yearend_score * ($pm->kpi_weight/100);


            if($pm->is_score == 1){
                $pm_score = (object) array(
                    'kpi_score'  => $midyear_score,
                    'competency_score' => $competency_midyear,
                    'pa_score' => $pa_score_midyear,
                    'is_score' => 'Midyear',
                    'year'     => $pm->year,
                    'id'       => $pm->id
                );
            }else{
                $pm_score = (object) array(
                    'kpi_score'  => $yearend_score,
                    'competency_score' => $competency_yearend,
                    'pa_score' => $pa_score_yearend,
                    'is_score' => 'Year-End',
                    'year'     => $pm->year,
                    'id'       => $pm->id
                );
            }

        }else{
            $pm_score =(object) array(
                'kpi_score'  => 0,
                'competency_score' => 0,
                'pa_score' => 0,
                'is_score' => '-',
                'year'     => date('Y'),
                'id'       => ''
            );
        }

        return view('personal.dashboard.index',compact('announcement','pm_score','assessment','matrik_raters'));
    }

    public function getCategory()
    {
        return ObjectiveCategory::where('status',1)->get()->pluck('name', 'name');
    }

    public function getMeasure()
    {
        return Measure::where('status',1)->get()->pluck('title', 'id');
    }


    public function publicView($id, $year)
    { 
        $pm   = Performance::getPerformanceByNikYear($id,$year);
        $type = 'midyear';
        if($pm){
            $id     = Hashids::decode($id);
            $type   = $type;
            $pm_items        = Performance::getPerformanceItemById($pm->id,1);
            $assessment      = Performance::getAppraisalItemById($pm->id);
            $appraisal       = Assessment:: getAssessmentByPmId($pm->id);
            $group_raters    = GroupRater::get();
            $rater           = Rater::where('employee_id',$pm->nik)->first();
            return view('personal.performance.detail_public', compact('pm', 'nik','type','pm_items','competency','assessment','group_raters','rater','year'));
        }else{
            return view('personal.performance.detail_public_404');
        }
    }


    public function publicPrint($id,$year, $type)
    { 
        $pm              = Performance::getPerformanceByNikYear($id,$year);
        $pm_items        = PerformanceItem::getPerformanceItem($pm->id);
        $competency      = Competency::get();
        $appraisal_items = Performance::getAppraisalItemById($pm->id);
        $group_raters    = GroupRater::get();
        $rater           = Rater::where('employee_id',$pm->nik)->first();
        $nik             = $id;
        $year            = $year;
        $group_raters    = GroupRater::get();

        if($type =="pdf"){
           $pdf = PDF::loadView('performance.pm.export_result', compact('pm','group_raters', 'pm_items','competency','appraisal_items','group_raters'))->setPaper('a4', 'landscape');;
           return $pdf->download('PMS-'.$pm->nama.'-'.$year.'.pdf');
        }else{
            return view('personal.performance.print', compact('pm', 'pm_items','competency','appraisal_items','group_raters'));
        }
    }


    public function upload()
    {
        return view('personal.performance.upload');
    }


    public function uploadStore(Request $request){
        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required'
        ));
        
        $period_start  = $request->get('period_1');
        $period_end    = $request->get('period_2');

        $cek_pm     = Performance::cekData($period_start, $period_end, $request->get('year'),Session::get('emp_nik'));
        $cek_rater  = Rater::where('employee_id', Session::get('emp_nik'))->first();

        if($cek_pm){
            return Redirect::back()
            ->withInput($request->input())  
            ->withErrors([trans('pm.message_failed_input')]);
        }else{
            if($cek_rater){

                if($request->hasFile('file')){
                    $extension = File::extension($request->file->getClientOriginalName());
                    if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                        $staff = explode(",",config('app.staff_id'));
                        $groupraters = GroupRater::get();
        
                        if(in_array(Session::get('emp_level_id'),$staff)){
                            $rater_weight = [
                                'self' => 5,
                                'superior' => 95
                            ];
                        }else{
                            $rater_weight = [];
                            foreach($groupraters as $val){
                                $rater_weight[$val->code] =  $val->weight;
                            }
                        }
                        
                        $data['period_start']   = $request->get('period_1');
                        $data['period_end']     = $request->get('period_2');
                        $data['year']           = $request->get('year');
                        $data['emp_id']         = Session::get('emp_nik');
                        $data['company_id']     = Session::get('emp_company_id');
                        $data['unit_id']        = Session::get('emp_unit_id');
                        $data['department_id']  = Session::get('emp_department_id');
                        $data['level_id']       = Session::get('emp_level_id');
                        $data['occupation_id']  = Session::get('emp_occupation_id');
                        $data['status']         = 10;
                        $data['kpi_weight']     = getSetting('weight_kpi');
                        $data['competency_weight']= getSetting('weight_competency');
                        $data['rater_weight']     = json_encode($rater_weight);
                        $emp_nik                 = Session::get('emp_nik');

                        $performance = Performance::create($data);
                        $file = $request->file('file'); 
                      
                        try{
                            $import = new MyPerformanceImport($performance->id, Session::get('emp_nik'),$request->get('year'));
                            Excel::import($import, $file);

                            if( $request->get('year') <= 2019){
                                $status = 1;
                            }else{
                                $status = 0;
                            }

                            $rater   =  Rater::where('employee_id',Session::get('emp_nik'))->first();
                            $raterID = json_decode($rater->rate,true);
                    
                            $dataRate = [];
                    
                            foreach ($raterID as $val ){
                                $key = explode('_',$val['key']);
                                if($val['value'] != NULL){
                                    $dataRate[] = [
                                        'performance_id'=> $performance->id,
                                        'group_rate'    => $key['0'],
                                        'rater_id'      => $val['value'],
                                        'status'        => $status,
                                        'created_at'    => Carbon::now()->format('Y/m/d H:i:s'),
                                    ];
                                }
                            }
                    
                            $competency = Competency::where('status', 1)->get();
                            $dataAppraisal = [];
                            foreach ($competency as $val ){
                                $dataAppraisal[] = [
                                    'performance_id'=> $performance->id,
                                    'competency_id' => $val->id,
                                ];
                            }
                            Assessment::insert($dataRate);
                            PerformanceAppraisal::insert($dataAppraisal);

                        } catch (\Exception $e) {
                            Performance::findOrFail($performance->id)->delete();
                            return Redirect::back()
                            ->withInput($request->input())  
                            ->withErrors([trans('pm.message_upload_failed')]);
                        }
                        return redirect()->route('myperformance.preview', [Hashids::encode($performance->id)])->with(['success' => trans('app.message_success_input')]);
                        
                    } else {
                        return redirect()->route('myperformance.upload')->with(['error' => 'Error inserting the data, please upload a valid xls/csv file !']);
                    }
                }
            }else{
                return redirect()->route('myperformance.upload')->with(['error' => trans('pm.message_failed_2_input')]);
            }
            
        }
    }


    public function preview($id)
    {

        $id = Hashids::decode($id);

        $pm              = Performance::getPerformanceById($id['0']);
        $kpi_items       = Performance::getPerformanceItemById($id['0']);
        return view('personal.performance.preview',compact('pm','kpi_items'));
    }


    public function previewStore(Request $request)
    {

        $performance_id  = $request->performance_id;
        $pm  = Performance::findOrFail($performance_id);

        if ($request->get('status')==1) {
            $staff = explode(",",config('app.staff_id'));
            if (in_array(Session::get('emp_level_id'), $staff)) {
                $data['status']    = 2;
                if($pm->year <= 2019){
                    $data['is_score']  = 2;
                }else{
                    $data['is_score']  = 1;
                }
            }else{
                if($pm->year <= 2019){
                    $data['status']    = 2;
                    $data['is_score']  = 2;
                }else{
                    $data['status']    = 1;
                    $data['is_score']  = 0;
                }
            }
            $pm->update($data);
            return redirect()->route('myperformance.personal.index')->with(['success' => trans('app.message_success_input')]);
        }else{
            $pm->delete();
            return redirect()->route('myperformance.personal.index')->with(['error' => trans('app.message_failed_input')]);
        }

    }


}
