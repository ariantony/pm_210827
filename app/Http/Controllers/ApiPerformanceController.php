<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiPerformanceController extends Controller
{
    
    public function get_assessment($type = null,$year = null)
    {   
        if ($year == null || $type == null){
            return abort(404);
        }
        $sql = "
        select
        performances.score_adjustment_1,performances.score_adjustment_2,
        performances.kpi_weight,performances.rater_weight,performances.competency_weight,performances.id, employee.firstname as emp_name, employee.nip as emp_nik,level.id as emp_level_id,
        (SELECT SUM(t.yearend_score*t.weight/100) FROM performance_items AS t WHERE t.performance_id = performances.id) AS kpi_yearend,
        (SELECT SUM(t.midyear_score*t.weight/100) FROM performance_items AS t WHERE t.performance_id = performances.id) AS kpi_midyear
        from performances 
        left join db_klola_hes.sys_ttrs_employee as employee on employee.nip = performances.emp_id 
        left join db_klola_hes.sys_tmst_level as level on level.id = employee.sys_tmst_level_id 
        where performances.year = $year and performances.status != 0
        ";


        $sql .= "order by emp_nik ASC";
        
        $performance = DB::select( DB::raw($sql));

        $performances = [];

        foreach ($performance as $val){
             
            $competency = getPAScore($val->rater_weight, $val->competency_weight, $val->id,$type);
                
            $staff = explode(",",config('app.staff_id'));
            if (in_array($val->emp_level_id, $staff)) {
                $kpi        = 0;
                $scorePA =  $competency + $kpi;
            }else{
                if($type == 'midyear'){
                    $kpi        = $val->kpi_midyear * ($val->kpi_weight/100);
                }else{
                    $kpi        = $val->kpi_yearend * ($val->kpi_weight/100);
                }
                $scorePA =  ($competency * ($val->competency_weight/100)) + $kpi;
            }

             $performances[]= array(
                'nik'   => $val->emp_nik,
                'name'  => $val->emp_name,
                'score' => round($scorePA,2),
                'adjusted_1'  => $val->score_adjustment_1,
                'adjusted_2'  => $val->score_adjustment_2

             );
        }

        return response()->json($performances);
    }

}
