<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class PerformanceExport implements FromView
{

    public function __construct($periode = null,  $company = null, $department = null, $workarea = null, $level = null, $type = null)
    {
        $this->workarea     = $workarea;
        $this->department   = $department;
        $this->periode      = $periode;
        $this->company      = $company;
        $this->level        = $level;
        $this->type         = $type ;
    }

    public function view(): View
    {   

        $sql = "
        select
        performances.*, employee.firstname as emp_name, employee.nip as emp_nik, occupation.alias as emp_occupation, department.name as emp_department, department.name as emp_company, unit.name as emp_workarea,
        company.name as emp_company, level.name as emp_level,level.id as emp_level_id,
        (SELECT SUM(t.yearend_score*t.weight/100) FROM performance_items AS t WHERE t.performance_id = performances.id) AS kpi_yearend,
        (SELECT SUM(t.midyear_score*t.weight/100) FROM performance_items AS t WHERE t.performance_id = performances.id) AS kpi_midyear

        from performances 

        left join db_klola_hes.sys_ttrs_employee as employee on employee.nip = performances.emp_id 
        left join db_klola_hes.sys_tmst_department as department on department.id = employee.sys_tmst_department_id 
        left join db_klola_hes.sys_tmst_occupation as occupation on occupation.id = employee.sys_tmst_occupation_id 
        left join db_klola_hes.sys_tmst_unit as unit on unit.id = employee.sys_tmst_unit_id 
        left join db_klola_hes.sys_tmst_level as level on level.id = employee.sys_tmst_level_id 
        left join db_klola_hes.sys_ttrs_employee_group as company on company.id = employee.sys_ttrs_employee_group_id
        where performances.year = $this->periode and performances.status != 0
        ";

        if(!empty($this->company)){
            $sql .= " and employee.sys_ttrs_employee_group_id = $this->company ";
        }
        if(!empty($this->workarea)){
            $sql .= " and employee.sys_tmst_unit_id = $this->workarea ";
        }
        
        if(!empty($this->department)){
            $sql .= " and employee.sys_tmst_department_id = $this->department ";
        }
        if(!empty($this->level)){
            $sql .= " and employee.sys_tmst_level_id = $this->level ";
        }

        $sql .= "order by emp_nik ASC";
        
        $performance = DB::select( DB::raw($sql));

        $performances = [];

        foreach ($performance as $val){
             
            $competency = getPAScore($val->rater_weight, $val->competency_weight, $val->id,$this->type);
                
            $staff = explode(",",config('app.staff_id'));
            if (in_array($val->emp_level_id, $staff)) {
                $kpi        = 0;
                $scorePA =  $competency + $kpi;
            }else{
                if($this->type == 'midyear'){
                    $kpi        = $val->kpi_midyear * ($val->kpi_weight/100);
                }else{
                    $kpi        = $val->kpi_yearend * ($val->kpi_weight/100);
                }
                $scorePA =  ($competency * ($val->competency_weight/100)) + $kpi;
            }

             $performances[]= array(
                'emp_name'  => $val->emp_name,
                'emp_nik'   => $val->emp_nik,
                'emp_occupation' => $val->emp_occupation,
                'emp_level'      => $val->emp_level,
                'emp_department' => $val->emp_department,
                'emp_workarea'   => $val->emp_workarea,
                'emp_company'    => $val->emp_company,
                'year'           => $val->year,
                'kpi'               => round($kpi,2),
                'competency'        => round($competency,2),
                'score_pa'             => round($scorePA,2),
                'score_adjustment_1_date'   => $val->score_adjustment_1_date,
                'score_adjustment_1'   => $val->score_adjustment_1,
                'score_adjustment_2_date'   => $val->score_adjustment_2_date,
                'score_adjustment_2'   => $val->score_adjustment_2,
             );
        }
        return view('performance.pm.export', [
            'pm' => $performances
        ]);
    }

  
}
