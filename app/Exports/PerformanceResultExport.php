<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\Performance;
use App\Models\PerformanceItem;
use App\Models\Competency;
use App\Models\GroupRater;

class PerformanceResultExport implements FromView
{

    public function __construct($id = null)
    {
        $this->id  = $id;
    }

    public function view(): View
    {

        $pm                 = Performance::findOrFail($this->id);
        $pm_items           = PerformanceItem::getPerformanceItem($this->id);
        $appraisal_items    = Performance::getAppraisalItemById($this->id);
        $competency         = Competency::get();
        $group_raters       = GroupRater::get();

        return view('performance.pm.export_result', [
            'pm' => $pm,
            'pm_items' => $pm_items,
            'competency' => $competency,
            'group_raters' => $group_raters,
            'appraisal_items' => $appraisal_items
        ]);
    }

  
}
