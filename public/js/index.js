$(document).ready(function() {
    $('.select2').select2({
        placeholder: "Silahkan pilih...",
    });

    var container = $('form').length > 0 ? $('form').parent() : "body";

    $('.datepicker').datepicker({
        container: container,
        todayHighlight: 'TRUE',
        format: "dd/mm/yyyy",
        autoclose: true,
        orientation: 'bottom'
    });
    $('.monthpicker').datepicker({
        container: container,
        todayHighlight: 'TRUE',
        autoclose: true,
        format: "MM",
        startView: "months",
        minViewMode: "months",
        orientation: 'bottom'
    });

    $('.yearpicker').datepicker({
        container: container,
        todayHighlight: 'TRUE',
        autoclose: true,
        format: " yyyy", // Notice the Extra space at the beginning
        viewMode: "years",
        minViewMode: "years",
        orientation: 'bottom'
    });


    $(document).on('click', "#btn-cancel", function(e) {
        var _this = $(this);
        e.preventDefault();
        Swal.fire({
            title: 'Konfirmasi', // Opération Dangereuse
            text: 'Apakah anda yakin melanjutkan ini?', // Êtes-vous sûr de continuer ?
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: 'null',
            cancelButtonColor: 'null',
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-primary',
            confirmButtonText: 'Ya, lanjut', // Oui, sûr
            cancelButtonText: 'Batal', // Annuler
        }).then(res => {
            if (res.value) {
                _this.closest("form").submit();
            }
        });
    });

});