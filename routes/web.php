<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


Route::get('locale/{locale}', function ($locale) {
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::get('assesment-input', ['uses' => 'Performance\AssessmentController@input','as' => 'assesment.input']);

Route::get('performance/get_assessment/{type?}/{year?}', ['uses' => 'ApiPerformanceController@get_assessment','as' => 'performance.get']);


Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware'=>['auth']], function () {
    Route::get('dashboard/{id?}', ['uses' => 'Admin\DashboardController@index', 'as' => 'dashboard'] );
   
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
   
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
   
    Route::resource('users', 'Admin\UsersController')->except(['show']);
    Route::post('users_delete', ['uses' => 'Admin\UsersController@delete', 'as' => 'users.delete']);
    Route::get('users_datatables', ['uses' => 'Admin\UsersController@datatables', 'as' => 'users.datatables'] );
    
    Route::resource('announcement', 'Admin\AnnouncementController');
    Route::post('announcement_delete', ['uses' => 'Admin\AnnouncementController@delete', 'as' => 'announcement.delete']);
    Route::get('announcement_datatables', ['uses' => 'Admin\AnnouncementController@datatables', 'as' => 'announcement.datatables'] );
});

Route::group(['prefix' => 'setting', 'as' => 'setting.', 'middleware'=>['auth']], function () {
    Route::resource('objective_category', 'Setting\ObjectiveCategoryController');
    Route::post('objective_category_delete', ['uses' => 'Setting\ObjectiveCategoryController@delete', 'as' => 'objective_category.delete']);
    Route::get('objective_category_datatables', ['uses' => 'Setting\ObjectiveCategoryController@datatables', 'as' => 'objective_category.datatables'] );
   
    Route::resource('measure', 'Setting\MeasureController');
    Route::post('measure_delete', ['uses' => 'Setting\MeasureController@delete', 'as' => 'measure.delete']);
    Route::get('measure_datatables', ['uses' => 'Setting\MeasureController@datatables', 'as' => 'measure.datatables'] );

    Route::resource('competency', 'Setting\CompetencyController');
    Route::post('competency_delete', ['uses' => 'Setting\CompetencyController@delete', 'as' => 'competency.delete']);
    Route::get('competency_datatables', ['uses' => 'Setting\CompetencyController@datatables', 'as' => 'competency.datatables'] );
   
    Route::resource('group_rater', 'Setting\GroupRaterController');
    Route::post('group_rater_delete', ['uses' => 'Setting\GroupRaterController@delete', 'as' => 'group_rater.delete']);
    Route::get('group_rater_datatables', ['uses' => 'Setting\GroupRaterController@datatables', 'as' => 'group_rater.datatables'] );

    Route::get('rater', ['uses' => 'Setting\RaterController@index', 'as' => 'rater.index'] );
    Route::get('rater_datatables', ['uses' => 'Setting\RaterController@datatables', 'as' => 'rater.datatables'] );
    Route::get('rater/{id}', ['uses' => 'Setting\RaterController@config', 'as' => 'rater.config'] );
    Route::post('rater_store', ['uses' => 'Setting\RaterController@store', 'as' => 'rater.store']);
    Route::post('rater_destroy', ['uses' => 'Setting\RaterController@destroy', 'as' => 'rater.destroy']);
    Route::get('rater_search', ['uses' => 'Setting\RaterController@search', 'as' => 'rater.search'] );
    
    Route::get('rater_import', ['uses' => 'Setting\RaterController@import', 'as' => 'rater.import'] );
    Route::post('rater_import_store', ['uses' => 'Setting\RaterController@importStore', 'as' => 'rater.importstore']);

    Route::get('general', ['uses' => 'Setting\GeneralController@index', 'as' => 'general.index'] );
    Route::post('general', ['uses' => 'Setting\GeneralController@store', 'as' => 'general.store'] );

    
});

Route::group(['prefix' => 'performance', 'as' => 'performance.', 'middleware'=>['auth']], function () {
   
    Route::get('datatables/{id?}', ['uses' => 'Performance\PerformanceController@datatables', 'as' => 'datatables'] );
    Route::get('data/{id?}', ['uses' => 'Performance\PerformanceController@index','as' => 'data']);
    Route::get('search', ['uses' => 'Performance\PerformanceController@search','as' => 'search']);
    Route::get('export', ['uses' => 'Performance\PerformanceController@export','as' => 'export']);
    Route::get('detail/{id}', ['uses' => 'Performance\PerformanceController@detail','as' => 'detail']);
    Route::get('print/{id}/{score}', ['uses' => 'Performance\PerformanceController@print','as' => 'print']);
    Route::get('show/{type}/{id}', ['uses' => 'Performance\PerformanceController@show','as' => 'show']);
    Route::get('print_kpi/{id}/{type}', ['uses' => 'Performance\MyPerformanceController@printKPI', 'as' => 'print.kpi'] );
    Route::post('destroy', ['uses' => 'Performance\PerformanceController@destroy', 'as' => 'destroy']);
    Route::get('upload', ['uses' => 'Performance\PerformanceController@upload', 'as' => 'upload'] );
    Route::post('upload_store', ['uses' => 'Performance\PerformanceController@uploadStore', 'as' => 'upload.store']);
    Route::get('preview/{id}/{model?}', ['uses' => 'Performance\PerformanceController@preview', 'as' => 'preview'] );
    Route::post('preview_store', ['uses' => 'Performance\PerformanceController@previewStore', 'as' => 'preview.store']);
    Route::post('delete', ['uses' => 'Performance\PerformanceController@delete', 'as' => 'delete']);


    Route::get('history', ['uses' => 'Performance\PerformanceController@history','as' => 'history']);
    Route::get('history_datatables', ['uses' => 'Performance\PerformanceController@historyDatatables', 'as' => 'history.datatables'] );
    Route::get('monitoring/{year?}', ['uses' => 'Performance\PerformanceController@monitoring','as' => 'monitoring']);
    Route::get('monitoring_search', ['uses' => 'Performance\PerformanceController@monitoring_search','as' => 'monitoring.search']);

    Route::get('monitoring_kpi/{year?}/{company?}/{workarea?}/{department?}', ['uses' => 'Performance\PerformanceController@monitoringKPI','as' => 'monitoring.kpi']);
    Route::get('monitoring_kpi_datatables/{year?}/{company?}/{workarea?}/{department?}', ['uses' => 'Performance\PerformanceController@monitoringKPI_datatables','as' => 'monitoring.kpi.datatables']);
    Route::get('monitoring_kpi_search', ['uses' => 'Performance\PerformanceController@monitoringKPI_search','as' => 'monitoring.kpi.search']);

    Route::get('adjust/{id}', ['uses' => 'Performance\PerformanceController@adjust','as' => 'adjust']);
    Route::post('adjust_store', ['uses' => 'Performance\PerformanceController@adjustStore','as' => 'adjust.store']);
    Route::get('adjust_upload', ['uses' => 'Performance\PerformanceController@adjust_upload','as' => 'adjust.upload']);
    Route::post('adjust_upload_store', ['uses' => 'Performance\PerformanceController@adjust_upload_store','as' => 'adjust.upload.store']);


});

Route::group(['middleware'=>['auth']], function () {
    Route::match(['get', 'post'], 'profile/change_profile', ['uses' => 'Admin\ProfileController@changeProfile', 'as' => 'profile.change_profile']);
    Route::match(['get', 'post'], 'profile/change_password', ['uses' => 'Admin\ProfileController@changePassword', 'as' => 'profile.change_password']);
    
    Route::get('announcement_read', ['uses' => 'Admin\AnnouncementController@read', 'as' => 'announcement.read'] );
    Route::post('announcement_read', ['uses' => 'Admin\AnnouncementController@search', 'as' => 'announcement.search'] );
   
    Route::get('notifications', ['uses' => 'Admin\NotificationController@index', 'as' => 'notifications'] );
    Route::get('notifications/{id}', ['uses' => 'Admin\NotificationController@show', 'as' => 'notifications.show'] );
   
    Route::get('get_employee', ['uses' => 'Setting\MasterController@getEmployee'] );
    Route::get('get_department/{id}', ['uses' => 'Setting\MasterController@getDepartment'] );
    Route::get('get_workarea/{id}', ['uses' => 'Setting\MasterController@getWorkarea'] );

});

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Auth::routes();



// EMPLOYEEE
Route::post('signin', ['uses' => 'SigninController@attempt','as' => 'signin']);
Route::post('sign', ['uses' => 'SigninController@login','as' => 'sign']);
Route::get('personal_detail_public/{id}/{year}/', ['uses' => 'Performance\MyPerformanceController@publicView', 'as' => 'personal.public.view'] );
Route::get('personal_detail_print/{id}/{year}/{type}', ['uses' => 'Performance\MyPerformanceController@publicPrint', 'as' => 'personal.public.print'] );

Route::group(['middleware'=>['checksigned']], function () {
    Route::get('announcement_read', ['uses' => 'Dashboard\AnnouncementController@read', 'as' => 'announcement.read'] );
});

Route::group(['prefix' => 'myperformance', 'as' => 'myperformance.', 'middleware'=>['checksigned']], function () {
    
    Route::get('detail/{type}/{id}', ['uses' => 'Performance\MyPerformanceController@detail','as' => 'detail']);
    Route::get('print/{id}/{score}', ['uses' => 'Performance\MyPerformanceController@print', 'as' => 'print'] );
    Route::get('show/{id}', ['uses' => 'Performance\MyPerformanceController@show','as' => 'show']);
    Route::get('print_kpi/{id}/{type}', ['uses' => 'Performance\MyPerformanceController@printKPI', 'as' => 'print.kpi'] );

    Route::get('upload', ['uses' => 'Performance\MyPerformanceController@upload', 'as' => 'upload'] );
    Route::post('upload_store', ['uses' => 'Performance\MyPerformanceController@uploadStore', 'as' => 'upload.store']);
    Route::get('preview/{id}', ['uses' => 'Performance\MyPerformanceController@preview', 'as' => 'preview'] );
    Route::post('preview_store', ['uses' => 'Performance\MyPerformanceController@previewStore', 'as' => 'preview.store']);

    Route::get('dashboard', ['uses' => 'Performance\MyPerformanceController@dashboard','as' => 'dashboard']);
    Route::resource('personal', 'Performance\MyPerformanceController');
    
    Route::get('personal_datatables', ['uses' => 'Performance\MyPerformanceController@datatables','as' => 'personal.datatables']);
    Route::post('personal_delete', ['uses' => 'Performance\MyPerformanceController@delete', 'as' => 'personal.delete']);
    Route::get('personal_search', ['uses' => 'Performance\MyPerformanceController@search','as' => 'personal.search']);
    Route::get('personal_print_pm/{id}/{type}', ['uses' => 'Performance\MyPerformanceController@printPM', 'as' => 'personal.print_pm'] );

    Route::get('achievement/{type}/{id}', ['uses' => 'Performance\MyPerformanceController@achievement','as' => 'achievement']);
    Route::post('achievementStore', ['uses' => 'Performance\MyPerformanceController@achievementStore', 'as' => 'achievement.store']);

    Route::get('assesment', ['uses' => 'Performance\MyAssessmentController@index','as' => 'assesment']);

    Route::get('get_objective_category', ['uses' => 'Performance\MyPerformanceController@getCategory','as' => 'get_performance_category']);
    Route::get('get_measure', ['uses' => 'Performance\MyPerformanceController@getMeasure','as' => 'get_measure']);
});



Route::group(['prefix' => 'myassessment', 'as' => 'myassessment.', 'middleware'=>['checksigned']], function () {

    Route::get('index', ['uses' => 'Performance\MyAssessmentController@index','as' => 'index']);
    Route::get('datatables', ['uses' => 'Performance\MyAssessmentController@datatables','as' => 'datatables']);
    Route::get('assess/{type}/{id}', ['uses' => 'Performance\MyAssessmentController@assess','as' => 'assess']);
    Route::post('store', ['uses' => 'Performance\MyAssessmentController@storeAssess','as' => 'store']);

    Route::get('history', ['uses' => 'Performance\MyAssessmentController@history','as' => 'history']);
    Route::get('datatables_history/{type}', ['uses' => 'Performance\MyAssessmentController@datatablesHistory','as' => 'history.datatables']);
    Route::get('detail/{type}/{id}', ['uses' => 'Performance\MyAssessmentController@detail','as' => 'detail']);
    
});


Route::group(['prefix' => 'myapproval', 'as' => 'myapproval.', 'middleware'=>['checksigned']], function () {

    Route::get('index', ['uses' => 'Performance\MyApprovalController@index','as' => 'index']);
    Route::get('datatables', ['uses' => 'Performance\MyApprovalController@datatables','as' => 'datatables']);
    Route::get('approve/{id}', ['uses' => 'Performance\MyApprovalController@approve','as' => 'approve']);
    Route::post('store', ['uses' => 'Performance\MyApprovalController@store','as' => 'store']);
    
});



